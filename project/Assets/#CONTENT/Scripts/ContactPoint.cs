﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContactPoint : MonoBehaviour
{
    private GameObject _detachedHook = null;

    public void SetDetachedHook(GameObject hook)
    {
        _detachedHook = hook;
    }

    private void OnDestroy()
    {
        if (_detachedHook != null)
            Destroy(_detachedHook);
    }
}
