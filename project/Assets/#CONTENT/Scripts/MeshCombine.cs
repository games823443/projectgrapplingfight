using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Platformtype
{
    boss,
    transition,
    normal
}

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
public class MeshCombine : MonoBehaviour
{
    public int PlatformID = -1;
        
    [SerializeField] private Platformtype _type=Platformtype.normal;
    public Platformtype Type => _type;

    void Start()
    {
        if (_type == Platformtype.transition)
            PlatformID = MapGenerator.s_instance.MaxPlatformID;

        MeshFilter[] meshFilters = GetComponentsInChildren<MeshFilter>();
        MeshFilter meshFilter = GetComponent<MeshFilter>();

        if (MapGenerator.s_instance.PlatformMeshes[PlatformID] == null)
        {
            Vector3 originalPos = transform.position;
            Quaternion originalRot = transform.rotation;

            transform.position = Vector3.zero;
            transform.rotation = Quaternion.identity;

            CombineInstance[] combine = new CombineInstance[meshFilters.Length];

            int i = 0;

            int offset = 0;

            while (i + offset < meshFilters.Length)
            {
                if (meshFilters[i + offset].sharedMesh != null)
                {
                    combine[i].mesh = meshFilters[i + offset].sharedMesh;
                    combine[i].transform = meshFilters[i + offset].transform.localToWorldMatrix;

                    i++;
                }
                else
                {
                    offset++;
                }
            }

            CombineInstance[] reducedCombine = new CombineInstance[meshFilters.Length - offset];

            Array.Copy(combine, reducedCombine, meshFilters.Length - offset);


            meshFilter.gameObject.SetActive(true);
            meshFilter.mesh.CombineMeshes(reducedCombine);

            MapGenerator.s_instance.SetPlatformMesh(PlatformID, Instantiate(meshFilter.mesh));

            transform.position = originalPos;
            transform.rotation = originalRot;
        }
        else
        {
            meshFilter.mesh = Instantiate(MapGenerator.s_instance.PlatformMeshes[PlatformID]);
        }

        int k = 1;

        while (k < meshFilters.Length)
        {
            meshFilters[k].gameObject.SetActive(false);

            k++;
        }

        MeshCollider collider = GetComponent<MeshCollider>();
        collider.sharedMesh = meshFilter.mesh;
        collider.convex = true;
        transform.gameObject.SetActive(true);

        if (_type == Platformtype.transition)
        {
            transform.localScale = new Vector3(10f, 10f, 10f);
            transform.localRotation = Quaternion.identity;
            transform.localPosition = Vector3.zero;
        }
        else if (_type == Platformtype.boss)
        {
            transform.localScale = new Vector3(0.666f, 0.666f, 0.666f);
            transform.localRotation = Quaternion.identity;
            transform.localPosition = Vector3.zero;
        }
        else if (_type == Platformtype.normal)
        {
            transform.localScale = Vector3.one;
            transform.localRotation = Quaternion.identity;
            transform.localPosition = Vector3.zero;
        }
    }      
}