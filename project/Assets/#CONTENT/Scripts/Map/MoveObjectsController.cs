﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveObjectsController : MonoBehaviour
{
    private Rigidbody _rb;
    private int _currentTargetID = 0;
    
    Vector3 m_EulerAngleVelocity;
    
    [Header("Movement")]
    [SerializeField] private bool _move = true;
    [SerializeField] private bool _rotate = true;
    [SerializeField] private float _movementSpeed = 20.0f;
    [SerializeField] private Transform[] _path = null;
    // Start is called before the first frame update
    void Start()
    {    m_EulerAngleVelocity = new Vector3(0, 100, 0);
        _rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        if(_move)
            MoveAlongPath();
        
    }
    
    private void MoveAlongPath()
    {
        Vector3 direction = _path[_currentTargetID].position - transform.position;

        _rb.MovePosition(transform.position + direction.normalized * _movementSpeed * Time.deltaTime);
        Quaternion deltaRotation = Quaternion.Euler(m_EulerAngleVelocity * Time.deltaTime);
        if (_rotate)
        {
            _rb.MoveRotation(_rb.rotation * deltaRotation);
        }
        
        if(direction.magnitude < 4.0f)
        {
            _currentTargetID++;
            if (_currentTargetID == _path.Length)
                _currentTargetID = 0;
        }
    }
}
