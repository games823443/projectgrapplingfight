﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Layer : MonoBehaviour
{
    private float _angle = 0.0f;

    private List<GameObject> _platforms = new List<GameObject>();
    private Dictionary<int, List<GameObject>> _platformsDevidedInRings = new Dictionary<int, List<GameObject>>();

    private GameObject _lastPlatform;

    private GameObject _centerPlatform;
    private Vector3 _offset;
    private GameObject transitionToNextLayer;
    public GameObject GenerateLayer(GameObject centerPlatform, int platformCountMin, int platformCountMax, float outDistance, float outDistanceIncrease, int angleMin, int angleMax, Vector3 offset, int layerNumber, bool lastLayer = false)
    {
        _centerPlatform = centerPlatform;
        _offset = offset;

        int numPlatforms = MapGenerator.s_instance.Rnd.Next(platformCountMin, platformCountMax);
        int counter = 0;

        int ringIdx = 0;

        while (counter < numPlatforms)
        {
            List<GameObject> ring = new List<GameObject>();

            for (int i = 0; i < numPlatforms; i++)
            {
                GameObject go = Instantiate(MapGenerator.s_instance.GetRandomPlatformPrefab());

                go.transform.position = centerPlatform.transform.position + centerPlatform.transform.forward * outDistance;

                go.transform.RotateAround(centerPlatform.transform.position, centerPlatform.transform.up, _angle);
                AddRandomOffset(go);

                _platforms.Add(go);
                ring.Add(go);

                _angle += MapGenerator.s_instance.Rnd.Next(angleMin, angleMax);

                counter++;

                _lastPlatform = go;
                
                if (_angle > 360)
                    break;
            }

            _platformsDevidedInRings.Add(ringIdx, ring);
            ringIdx++;

            outDistance += outDistanceIncrease;

            _angle = MapGenerator.s_instance.Rnd.Next(angleMin, angleMax);

            angleMin -= 10;
            angleMax -= 10;
        }

        GameObject bossPlatform = Instantiate(MapGenerator.s_instance.GetRandomBossPrefab());
        bossPlatform.transform.position = _lastPlatform.transform.position;
        bossPlatform.transform.rotation = _lastPlatform.transform.rotation;


        MapGenerator.s_instance.ActiveBossPlaforms.Add(layerNumber,bossPlatform);

        Destroy(_lastPlatform);

        _lastPlatform = bossPlatform;

        RemoveOverlappingPlatforms();
        FindInterRingTransitions();

        //add other bossplatforms
        if (_lastPlatform.transform.name.Contains("SwarmPlatform"))
        {
            _lastPlatform.GetComponentInChildren<SwarmManager>().SwarmCoordinator.Layer = layerNumber;
        }
        else
            _lastPlatform.GetComponentInChildren<Enemy>()?.SetLayer(layerNumber);

        if (!lastLayer)
        {
            transitionToNextLayer = CreateTransitionToNextLayer();
        }
        else
            MapGenerator.s_instance.SetLastPlatform(_lastPlatform);
        //CreateWinPlatform();

        Debug.DrawLine(_lastPlatform.transform.position, _lastPlatform.transform.position + Vector3.up * 200.0f, Color.cyan, 10.0f);
        return transitionToNextLayer;
    }

    private void RemoveOverlappingPlatforms()
    {
        List<int> indexesToRemove = new List<int>();

        for (int i = 0; i < _platforms.Count; i++)
        {
            for (int j = 0; j < _platforms.Count; j++)
            {
                if (i == j)
                    continue;

                if (Vector3.Distance(_platforms[i].transform.position, _platforms[j].transform.position) < 250.0f)
                {
                    if (!indexesToRemove.Contains(i) && !indexesToRemove.Contains(j))
                        indexesToRemove.Add(i);
                }
            }
        }

        foreach (int i in indexesToRemove)
        {
            GameObject platformToDelete = _platforms[i];
            _platforms.RemoveAt(i);
            Destroy(platformToDelete);
        }
    }

    private void FindInterRingTransitions()
    {
        GameObject currentPlatform = _centerPlatform;

        if (_platformsDevidedInRings.Count == 0)
            return;

        for (int i = 0; i < _platformsDevidedInRings[0].Count; i++) // find first transitions from center platform to first ring
        {
            Debug.DrawLine(_platformsDevidedInRings[0][i].transform.position, _centerPlatform.transform.position, Color.green, 10.0f);
            
            CreateRandomTransition(_platformsDevidedInRings[0][i], _centerPlatform, false);

            if (i > 0)
                CreateRandomTransition(_platformsDevidedInRings[0][i], _platformsDevidedInRings[0][i - 1], true);
        }

        for (int ring = 1; ring < _platformsDevidedInRings.Count; ring++) //go through all rings
        {
            for (int i = 0; i < _platformsDevidedInRings[ring].Count; i++) //go through all platforms in a ring
            {
                if (i > 0)
                    CreateRandomTransition(_platformsDevidedInRings[ring][i], _platformsDevidedInRings[ring][i - 1], true);

                currentPlatform = _platformsDevidedInRings[ring][i];

                GameObject closestPlatform = _platformsDevidedInRings[ring - 1][0];

                for (int j = 0; j < _platformsDevidedInRings[ring - 1].Count; j++) //go through all platforms on the inner ring to find the closest platform
                {
                    if (Vector3.Distance(currentPlatform.transform.position, _platformsDevidedInRings[ring - 1][j].transform.position) < Vector3.Distance(currentPlatform.transform.position, closestPlatform.transform.position))
                    {
                        closestPlatform = _platformsDevidedInRings[ring - 1][j];
                    }
                }

                CreateRandomTransition(closestPlatform, currentPlatform, false);
                Debug.DrawLine(closestPlatform.transform.position, currentPlatform.transform.position, Color.green, 10.0f);
            }
        }
    }

    private void CreateRandomTransition(GameObject platform1, GameObject platform2, bool inbetweenRings)
    {
        float distance = Vector3.Distance(platform1.transform.position, platform2.transform.position);// - platform1.transform.localScale.x;

        if (inbetweenRings && (MapGenerator.s_instance.Rnd.Next(0, 100) > 65 || distance > 550)) //65% chance to create transition if not too far away
            return;

        int amountOfTransitions = (int)(distance / MapGenerator.s_instance.DistanceToSpawnTransition);

        float distanceBetweenTransitions = distance / (amountOfTransitions + 1);

        for(int i = 0; i < amountOfTransitions; i++)
        {
            GameObject transition = Instantiate(MapGenerator.s_instance.GetRandomTransitionPrefab());
            transition.transform.position = platform1.transform.position;
            transition.transform.LookAt(platform2.transform);
            transition.transform.position += transition.transform.forward * (distanceBetweenTransitions * (i + 1));
        }
        Debug.DrawLine(platform1.transform.position, platform2.transform.position, Color.green, 10.0f);
    }

    private GameObject CreateTransitionToNextLayer()
    {
        int amoutOfPlatforms = (int)(MapGenerator.s_instance.DistanceBetweenLayer / 20.0f) + 1;

        float yRotation = 0.0f;
        float yPos = 0.0f;
        GameObject transitionToNextLayer=new GameObject("TransitionToNextLayer");
        for(int i = 0; i < amoutOfPlatforms; i++)
        {
            GameObject platform = Instantiate(MapGenerator.s_instance.TransitionToNextLayer);
            platform.transform.position = _lastPlatform.transform.position + _lastPlatform.transform.forward * 80.0f + new Vector3(0.0f, yPos, 0.0f);
            platform.transform.parent = transitionToNextLayer.transform;
            platform.transform.RotateAround(_lastPlatform.transform.position, Vector3.up, yRotation);
            yRotation += 35.0f;
            yPos += 20.0f;
        }
        return transitionToNextLayer;
    }

    private void AddRandomOffset(GameObject platform)
    {
        float xOffset = MapGenerator.s_instance.Rnd.Next((int)_offset.x * -1, (int)_offset.x) / 10.0f;
        float yOffset = MapGenerator.s_instance.Rnd.Next((int)_offset.y * -1, (int)_offset.y) / 10.0f;
        float zOffset = MapGenerator.s_instance.Rnd.Next((int)_offset.z * -1, (int)_offset.z) / 10.0f;

        Vector3 posOffset = new Vector3(xOffset, yOffset, zOffset);

        platform.transform.position += posOffset;

        if (platform.transform.position.y < MapGenerator.s_instance.YMin)
            platform.transform.position = new Vector3(platform.transform.position.x, MapGenerator.s_instance.YMin, platform.transform.position.z);

        platform.transform.Rotate(Vector3.up, (float)MapGenerator.s_instance.Rnd.Next(-30, 30));
    }

    public GameObject GetLastPlatform()
    {
        return _lastPlatform;
    }

    private void CreateWinPlatform()
    {
        GameObject lastPlatform = _lastPlatform;

        _lastPlatform = Instantiate(MapGenerator.s_instance.FirstPlatform);
        _lastPlatform.transform.position = lastPlatform.transform.position;
        _lastPlatform.transform.rotation = lastPlatform.transform.rotation;

        Destroy(lastPlatform);

        GameObject winObject = Instantiate(MapGenerator.s_instance.WinObject, _lastPlatform.transform);
    }

    
}
