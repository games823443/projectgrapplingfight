﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class MapGenerator : MonoBehaviour
{
    public static MapGenerator s_instance;

    [SerializeField] private int _firstLayerPlatformCountMin = 15;
    [SerializeField] private int _firstLayerPlatformCountMax = 25;

    [SerializeField] private int _layerCount = 3;
    [SerializeField] private int _platformDecreasePerLayer = 3;
    [SerializeField] private float _distanceBetweenLayer = 200.0f;
    [SerializeField] private float _initialHeight = 100.0f;

    [Header("Platforms")]
    [SerializeField] private GameObject[] _platformPrefabs = null;
    [SerializeField] private GameObject _startPlatform = null;
    [SerializeField] private GameObject[] _transitionPrefabs = null;
    [SerializeField] private float _distanceToSpawnTransition = 60.0f;
    [SerializeField] private GameObject[] _bossPlatforms = null;
    [SerializeField] private GameObject _transitionToNextLayer = null;
    
    [Header("Circular Distribution")]
    [SerializeField] private float _outDistance = 200f;
    [SerializeField] private float _outDistanceIncrease = 200f;
    [SerializeField] private int _angleMin = 60;
    [SerializeField] private int _angleMax = 100;

    [Header("Platform Offset")]
    [SerializeField] private int _xOffset = 100;
    [SerializeField] private int _yOffset = 100;
    [SerializeField] private int _zOffset = 100;
    [SerializeField] private float _yMin = 0.0f;



    [Header("TransitionEffect")] [SerializeField]
    private ParticleSystem _effect = null;
    
    [Header("")]
    [SerializeField] private GameObject _winObject = null;

    private GameObject _lastPlatform = null;
    
    private Dictionary<int,GameObject> _layerTransitonObjects = new Dictionary<int, GameObject>();
    
    public Dictionary<int,GameObject> LayerTransitionObjects => _layerTransitonObjects;
    
    public float DistanceToSpawnTransition => _distanceToSpawnTransition;

    public Vector3 Offset => new Vector3(_xOffset, _yOffset, _zOffset);
    public float DistanceBetweenLayer => _distanceBetweenLayer;

    private List<Layer> _layer = new List<Layer>();
    private System.Random _rnd = new System.Random();

    public System.Random Rnd => _rnd;

    public GameObject WinObject => _winObject;
    public GameObject FirstPlatform => _startPlatform;

    public GameObject TransitionToNextLayer => _transitionToNextLayer;

    private bool _mapGenerationDone = false;
    public bool MapGenerationDone => _mapGenerationDone;

    public float YMin => _yMin;

    private Dictionary<int,GameObject> _activeBossPlatforms=new Dictionary<int,GameObject>();

    public Dictionary<int, GameObject> ActiveBossPlaforms => _activeBossPlatforms;

    private float[] _layerBoundaries;

    public float[] LayerBoundaries => _layerBoundaries;

    private Mesh[] _platformMeshes;
    public Mesh[] PlatformMeshes => _platformMeshes;

    private int _maxPlatformID;
    public int MaxPlatformID => _maxPlatformID;

    private int _lastTransition = -1;
    private int _lastBossPlatform = -1;

    // Start is called before the first frame update
    void Awake()
    {
        if (s_instance == null)
            s_instance = this;
        else
            Debug.LogWarning("MapGenerator already exists!");

        

        GenerateMap();
        
    }

    public void GenerateMap()
    {
        float y = _initialHeight;
        int platformMin = _firstLayerPlatformCountMin;
        int platformMax = _firstLayerPlatformCountMax;

        float yRotation = 0.0f;

        _layerBoundaries = new float[_layerCount];

        for (int i = 0; i < _layerCount; i++)
        {
            _layerBoundaries[i] = y - (_yOffset/10f);

            GameObject centerPlatform;

            if (i == 0)
                centerPlatform = Instantiate(_startPlatform);
            else
                centerPlatform = Instantiate(GetRandomPlatformPrefab());            

            centerPlatform.transform.rotation = Quaternion.Euler(0.0f, yRotation, 0.0f);


            if (yRotation == 0.0f)
                yRotation = 180.0f;
            else
                yRotation = 0.0f;

            if (i > 0)
            {
                Vector3 pos = _layer[i - 1].GetLastPlatform().transform.position;
                pos.y = y;
                centerPlatform.transform.position = pos;
                
            }
            else
                centerPlatform.transform.position = new Vector3(0.0f, y, 0.0f);

            y += _distanceBetweenLayer;
            
            GameObject layer = new GameObject("Layer" + i);
            Layer layerComp = layer.AddComponent<Layer>();

            if (i < _layerCount - 1)
            {
               GameObject transitionToNextLayerParent = layerComp.GenerateLayer(centerPlatform, platformMin, platformMax, _outDistance, _outDistanceIncrease,
                    _angleMin, _angleMax, Offset,i);
               
               Transform topObject = transitionToNextLayerParent.transform.GetChild(transitionToNextLayerParent.transform.childCount-1);
               Transform botsObject = transitionToNextLayerParent.transform.GetChild(0);
               for(int cnt = 0; cnt < transitionToNextLayerParent.transform.childCount; cnt++)
               {
                   GameObject smallCone = transitionToNextLayerParent.transform.GetChild(cnt).gameObject;
                   var renderers= smallCone.GetComponentsInChildren<Renderer>();
                   foreach (Renderer renderer in renderers)
                   {
                       renderer.enabled = false;
                   }
                   smallCone.AddComponent<MoveTransition>();
                   smallCone.GetComponent<MoveTransition>().yBot = botsObject.transform.position.y;
                   smallCone.GetComponent<MoveTransition>().yTop = topObject.transform.position.y;
               }
               
               _layerTransitonObjects.Add(i,transitionToNextLayerParent);
               
            }
            else     
                layerComp.GenerateLayer(centerPlatform, platformMin, platformMax, _outDistance, _outDistanceIncrease, _angleMin, _angleMax, Offset,i, true);
            
            _layer.Add(layerComp);

            platformMin -= _platformDecreasePerLayer;
            platformMax -= _platformDecreasePerLayer;
        }

        _mapGenerationDone = true;

    }

    public GameObject GetRandomPlatformPrefab()
    {
        return _platformPrefabs[_rnd.Next(0, _platformPrefabs.Length)];
    }

    public GameObject GetRandomTransitionPrefab()
    {
        int id = _rnd.Next(0, _transitionPrefabs.Length);

        while(id == _lastTransition)
            id = _rnd.Next(0, _transitionPrefabs.Length);

        _lastTransition = id;

        return _transitionPrefabs[id];
    }
    public GameObject GetRandomBossPrefab()
    {
        int id = _rnd.Next(0, _bossPlatforms.Length);

        while (id == _lastBossPlatform)
            id = _rnd.Next(0, _bossPlatforms.Length);

        _lastBossPlatform = id;

        return _bossPlatforms[id];
    }

    public void SpawnLayerTransition(int layerID)
    {
        if (layerID < _layerCount - 1)
        {
            GameObject stair = LayerTransitionObjects.FirstOrDefault(t => t.Key == layerID).Value;

            if (stair != null)
            {
               
                StartCoroutine(ActivateTransition(stair));
                _activeBossPlatforms.Remove(layerID);
            }
        }
        else
            Instantiate(WinObject, _lastPlatform.transform);
    }

    IEnumerator ActivateTransition(GameObject stair)
    {
        foreach (Transform child in stair.transform)
        {
            foreach (Renderer renderer in child.GetComponentsInChildren<Renderer>())
            {
                renderer.enabled = true;
            }
            var effect=Instantiate(_effect, child.transform.position, child.transform.rotation);
            effect.transform.parent = child;
            effect.Play();

            yield return new WaitForSeconds(0.5f);
            effect.Stop();
        }
        foreach (Transform child in stair.transform)
        {
            child.gameObject.GetComponent<MoveTransition>().stop = false;
        }
    }

    public void SetLastPlatform(GameObject lastPlatform)
    {
        _lastPlatform = lastPlatform;
    }

    /*private void SetPlatformIDs()
    {
        int size = _platformPrefabs.Length + _bossPlatforms.Length + 2;
        _maxPlatformID = size - 1;
        _platformMeshes = new Mesh[size];

        MeshCombine[] meshCombine = _startPlatform.GetComponentsInChildren<MeshCombine>();
        
        foreach(var mc in meshCombine)
        {
            mc.PlatformID = 0;
        }

        for (int i = 0; i < _platformPrefabs.Length; i++)
        {
            meshCombine = _platformPrefabs[i].GetComponentsInChildren<MeshCombine>();

            foreach (var mc in meshCombine) 
            {
                mc.PlatformID = i + 1;
            }
        }

        for (int j = 0; j < _bossPlatforms.Length; j++)
        {
            meshCombine = _bossPlatforms[j].GetComponentsInChildren<MeshCombine>();

            foreach (var mc in meshCombine)
            { 
                mc.PlatformID = j + _platformPrefabs.Length + 1;
            }
        }
    }*/

    public void SetPlatformMesh(int id, Mesh m)
    {
        _platformMeshes[id] = m;

        //if (id == _maxPlatformID)
        //    return;

        //MeshFilter[] meshes;
        //MeshCombine[] meshComine;

        //if (id == 0)
        //{
        //    meshComine = _startPlatform.GetComponentsInChildren<MeshCombine>();
        //}
        //else if (id > 0 && id < _platformPrefabs.Length + 1)
        //{
        //    meshComine = _platformPrefabs[id - 1].GetComponentsInChildren<MeshCombine>();
        //}
        //else
        //{
        //    meshComine = _bossPlatforms[id - _platformPrefabs.Length - 1].GetComponentsInChildren<MeshCombine>();
        //}

        //Debug.Log(meshComine.Length);

        //foreach (var mc in meshComine)
        //{
        //    meshes = mc.GetComponentsInChildren<MeshFilter>();

        //    for (int i = 0; i < meshes.Length; i++)
        //    {
        //        if (meshes[i].transform.GetComponent<MeshCombine>() == null)
        //            meshes[i].gameObject.SetActive(false);
        //    }
        //}
    }
}
