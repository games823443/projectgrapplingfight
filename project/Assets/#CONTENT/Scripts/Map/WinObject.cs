﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WinObject : MonoBehaviour
{
    [SerializeField] private float _rotationSpeed = 50f;
    [SerializeField] private float _upDownSpeed = 1.5f;
    [SerializeField] private float _upDownHeight = 3.0f;
    [SerializeField] private float _initialHeight = 7.0f;
    private Vector3 _initialPosition;
    private float sin = 0.0f;

    // Start is called before the first frame update
    void Start()
    {
        _initialPosition = transform.position;
        _initialPosition.y += _initialHeight;
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(0.0f, _rotationSpeed * Time.deltaTime, 0.0f);

        sin += Time.deltaTime * _upDownSpeed;

        transform.position = _initialPosition + new Vector3(0.0f, Mathf.Sin(sin) * _upDownHeight, 0.0f);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            UIManager.s_instance.ShowWinScreen();
            AkSoundEngine.SetState("Music_InGame", "Victory");
            GameManager.s_instance.PlayMapCompleted();

            Destroy(gameObject);
        }
    }
}
