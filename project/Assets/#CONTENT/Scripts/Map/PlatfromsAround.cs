﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using Random = UnityEngine.Random;

public class PlatfromsAround : MonoBehaviour
{
    public GameObject transitionType;
    public GameObject prefab;
    public int radius = 0;

    public int count = 0;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void OnButtonPress()
    {
        Debug.Log("GeneratedSpheres");
        CreateCones(radius,count);
    }

    void CreateCones(int radius, int count)
    {
        Vector3 center = prefab.transform.position;
        for (int i = 0; i < count; i++){
            Vector3 pos = RandomCircle(center, radius);
            Quaternion rot = Quaternion.FromToRotation(Vector3.forward, center-pos);
            Instantiate(transitionType, pos, rot);
        }
    }
    
    Vector3 RandomCircle ( Vector3 center ,   float radius  ){
        float ang = Random.value * 360;
        Vector3 pos;
        pos.x = center.x + radius * Mathf.Sin(ang * Mathf.Deg2Rad);
        pos.y = center.y + radius * Mathf.Cos(ang * Mathf.Deg2Rad);
        pos.z = center.z;
        return pos;
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
