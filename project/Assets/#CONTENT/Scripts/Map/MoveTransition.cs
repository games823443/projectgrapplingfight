using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveTransition : MonoBehaviour
{

    public float yTop;

    public float yBot;

    private bool _whereToMove=false;

    public bool stop = true;
    // Start is called before the first frame update
    void Start()
    {
        float distanceToTop = yTop- transform.position.y;
        float distanceToBot = transform.position.y - yBot;

        if (distanceToTop < distanceToBot)
        {
            _whereToMove = true;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!stop)
        {
            if (_whereToMove)
            {
                transform.position += new Vector3(0, 20 * Time.deltaTime, 0);
            }
            else
            {
                transform.position -= new Vector3(0, 20 * Time.deltaTime, 0);
            }

            if (transform.position.y >= yTop)
            {
                _whereToMove = false;
            }

            if (transform.position.y <= yBot)
            {
                _whereToMove = true;
            }
        }
    }
}
