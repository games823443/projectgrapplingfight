﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Rewired;
using UnityEngine.EventSystems;
using System.Collections;
using System.Numerics;
using Unity.Collections;
using Vector2 = UnityEngine.Vector2;
using Vector3 = UnityEngine.Vector3;

public class UIManager : MonoBehaviour
{
    public static UIManager s_instance;

    [SerializeField] private Image[] _hearts = null;
    [SerializeField] private GameObject _winScreen = null;
    [SerializeField] private GameObject _pauseScreen = null;
    [SerializeField] private GameObject _resumeButton = null;
    [SerializeField] private GameObject _winScreenMenuButton = null;
    [SerializeField] private Scrollbar _audioVolumeScrollbar = null;
    [SerializeField] private Scrollbar _mouseSensitivityScrollbar = null;
    [SerializeField] private RectTransform _pickupBackground = null;
    [SerializeField] private Text _pickupText = null;
    [SerializeField] private Text killCounterText = null;
    [SerializeField] private Text timeText = null;
    [SerializeField] private Text distanceText = null;
    private RectTransform _pickupTextRT;
    private int _heartsVisible = 5;

    private Player rewiredPlayer;
    private Vector3 prevPlayerPos;
    private float distance = 0;

    private bool _mouseFocus = true;

    public bool MouseFocus => _mouseFocus;

    private GameObject lastSelected;

    private bool _fadeOutPickupText = false;
    private Image _pickupBackgroundImage;

    private float _mouseSensitivity = 1f;
    private int killCount = 0;

    private float _mouseMultiplier = 10.0f;

    public float MouseSensitivity => _mouseSensitivity;


    public AK.Wwise.Event Menu_Pause = new AK.Wwise.Event();
    public uint Menu_PauseID { get { return (uint)(Menu_Pause == null ? AkSoundEngine.AK_INVALID_PLAYING_ID : Menu_Pause.Id); } }

    public AK.Wwise.Event Menu_Continue = new AK.Wwise.Event();
    public uint Menu_ContinueID { get { return (uint)(Menu_Continue == null ? AkSoundEngine.AK_INVALID_PLAYING_ID : Menu_Continue.Id); } }

    public AK.Wwise.Event Menu_ReturnMainMenu = new AK.Wwise.Event();
    public uint Menu_ReturnMainMenuID { get { return (uint)(Menu_ReturnMainMenu == null ? AkSoundEngine.AK_INVALID_PLAYING_ID : Menu_ReturnMainMenu.Id); } }

    public AK.Wwise.Event Menu_SliderRelease = new AK.Wwise.Event();
    public uint Menu_SliderReleaseID { get { return (uint)(Menu_SliderRelease == null ? AkSoundEngine.AK_INVALID_PLAYING_ID : Menu_SliderRelease.Id); } }

    //RTPCs
    [SerializeField]
    private AK.Wwise.RTPC _sfxVolume = null;

    [SerializeField]
    private AK.Wwise.RTPC _musicVolume = null;


    private void Awake()
    {
        float volume = PlayerPrefs.GetFloat("Volume", 100.0f);
        _audioVolumeScrollbar.value = volume;

        _sfxVolume.SetGlobalValue(volume * 100f);
        _musicVolume.SetGlobalValue(volume * 100f);
        
        float sesitivity=PlayerPrefs.GetFloat("Sesitivity", 1.0f);
        _mouseSensitivityScrollbar.value = sesitivity;
        _mouseSensitivity = sesitivity*_mouseMultiplier;

        rewiredPlayer = ReInput.players.GetPlayer(0);

        lastSelected = _resumeButton;

        _pickupTextRT = _pickupText.GetComponent<RectTransform>();
        _pickupBackgroundImage = _pickupBackground.GetComponent<Image>();

        _pickupBackground.gameObject.SetActive(false);
        killCounterText.text = "Kills: "+killCount;
        
    }

    // Start is called before the first frame update
    void Start()
    {
        if (s_instance == null)
            s_instance = this;
        else
            Debug.LogWarning("UIManager already exists!");

        if (_winScreen != null)
            _winScreen.SetActive(false);

        if (_pauseScreen != null)
            _pauseScreen.SetActive(false);

        prevPlayerPos = GameObject.FindObjectOfType<MainCharacterController>().transform.position;

    }

    // Update is called once per frame
    void Update()
    {
        if (_mouseFocus)
        {
            if (rewiredPlayer.GetAnyButton() && !rewiredPlayer.GetButton("ShootHook") && rewiredPlayer.GetAxis("HorizontalLook") == 0.0f && rewiredPlayer.GetAxis("VerticalLook") == 0.0f)
            {
                _mouseFocus = false;

                Cursor.visible = false;
                Cursor.lockState = CursorLockMode.Locked;

                if (_winScreen.activeSelf || _pauseScreen.activeSelf)
                {
                    EventSystem.current.SetSelectedGameObject(lastSelected);
                }
            }
        }
        else
        {
            if (rewiredPlayer.GetButton("ShootHook") || rewiredPlayer.GetAxis("HorizontalLook") != 0.0f || rewiredPlayer.GetAxis("VerticalLook") != 0.0f)
            {
                _mouseFocus = true;

                if (_winScreen.activeSelf || _pauseScreen.activeSelf)
                {
                    lastSelected = EventSystem.current.currentSelectedGameObject;
                    EventSystem.current.SetSelectedGameObject(null);

                    Cursor.visible = true;
                    Cursor.lockState = CursorLockMode.None;
                }
            }
        }
        if (!_winScreen.activeSelf && rewiredPlayer.GetButtonDown("Menu"))
        {
            TogglePause();
        }

        CheckPickUpTextFadeOut();
        UpdateTime();
        UpdateDistance();
    }

    private void FixedUpdate()
    {
        
    }

    private void UpdateTime()
    {
        int time = (int)Time.timeSinceLevelLoad;
        int minutes = time / 60;
        int seconds = time % 60;
        string secAdd = "";
        if (seconds < 10)
        {
            secAdd = "0";
        }
        else
        {
            secAdd = "";
        }
        timeText.text = "Time: " + minutes + ":"+secAdd+seconds;
    }


    private void UpdateDistance()
    {
        var playerPos = GameObject.FindObjectOfType<MainCharacterController>().transform;
        distance += Vector3.Distance(prevPlayerPos, playerPos.position);
        prevPlayerPos = playerPos.position;

        float distKm = (distance / 1000);

        distanceText.text = "Distance: " + String.Format("{0:0.0}",distKm) + " km";
        //distanceText.text = "x: "+x+" y:"+y+" z:"+z;
        
    }
    

    public void RemoveHeart()
    {
        _heartsVisible -= 1;

        if(_heartsVisible >= 0)
            _hearts[_heartsVisible].enabled = false;
    }

    public void RestoreHeart()
    {
        _hearts[_heartsVisible].enabled = true;

        if (_heartsVisible < 5)
            _heartsVisible += 1;
    }

    public void ResetHearts()
    {
        foreach (Image h in _hearts)
            h.enabled = true;

        _heartsVisible = 5;
    }

    public void ShowWinScreen()
    {
        GameManager.s_instance.PauseGame();
        _pauseScreen.SetActive(false);
        _winScreen.SetActive(true);
        lastSelected = _winScreenMenuButton;
        if (!_mouseFocus)
        {
            EventSystem.current.SetSelectedGameObject(lastSelected);
        }
    }

    public void TogglePause()
    {
        _winScreen.SetActive(false);
        if (GameManager.s_instance.Paused)
        {
            PlayContinueSound();
            _pauseScreen.SetActive(false);
            GameManager.s_instance.ResumeGame();
        }
        else
        {
            GameManager.s_instance.PlayPauseGame();
            PlayPauseSound();
            AkSoundEngine.SetState("Music_InGame", "InGame_Menu");
            _pauseScreen.SetActive(true);
            GameManager.s_instance.PauseGame();
            lastSelected = _resumeButton;
            if (!_mouseFocus)
            {
                EventSystem.current.SetSelectedGameObject(lastSelected);
            }
        }
    }

    public void ReturnToMainMenu()
    {
        GameManager.s_instance.PlayReturnMainMenu();
        //PlayReturnMainMenuSound();
        SceneManager.LoadScene("MainMenu");
    }

    public void AudioVolumeChanged()
    {
        //PlaySliderReleaseSound();
        AudioListener.volume = _audioVolumeScrollbar.value;

        _sfxVolume.SetGlobalValue(_audioVolumeScrollbar.value * 100f);
        _musicVolume.SetGlobalValue(_audioVolumeScrollbar.value * 100f);

        PlayerPrefs.SetFloat("Volume", _audioVolumeScrollbar.value);
        PlayerPrefs.Save();
        if (_mouseFocus)
        {
            EventSystem.current.SetSelectedGameObject(null);
        }
    }

    public void MouseSensitivityChanged()
    {
        //PlaySliderReleaseSound();
        _mouseSensitivity = _mouseSensitivityScrollbar.value * _mouseMultiplier;
        
        if (_mouseSensitivity < 0.1f)
            _mouseSensitivity = 0.1f*_mouseMultiplier;
        
        PlayerPrefs.SetFloat("Sesitivity", _mouseSensitivity/_mouseMultiplier);
        PlayerPrefs.Save();
    }

    public void ShowPickupText(string text)
    {
        _fadeOutPickupText = false;
        ResetPickUpTextAlpha();

        _pickupBackground.gameObject.SetActive(true);

        float width = (10f + text.Length) * 11f;

        _pickupBackground.rect.Set(_pickupBackground.rect.x, _pickupBackground.rect.y, width, _pickupBackground.rect.height);
        _pickupTextRT.rect.Set(_pickupTextRT.rect.x, _pickupTextRT.rect.y, width, _pickupTextRT.rect.height);

        _pickupBackground.sizeDelta = new Vector2(width, _pickupBackground.rect.height);
        _pickupTextRT.sizeDelta = new Vector2(width, _pickupTextRT.rect.height);

        _pickupText.text = text;

        StartCoroutine(WaitTillFadeOut());
    }

    private IEnumerator WaitTillFadeOut()
    {
        yield return new WaitForSeconds(2f);
        _fadeOutPickupText = true;
    }

    private void CheckPickUpTextFadeOut()
    {
        if(_fadeOutPickupText)
        {
            Color bc = _pickupBackgroundImage.color;
            bc.a -= Time.deltaTime;

            if (bc.a < 0)
                bc.a = 0;

            _pickupBackgroundImage.color = bc;

            Color tc = _pickupText.color;
            tc.a -= Time.deltaTime;

            if (tc.a < 0)
                tc.a = 0;

            _pickupText.color = tc;

            if (bc.a == 0 && tc.a == 0)
            {
                _fadeOutPickupText = false;
                _pickupBackground.gameObject.SetActive(false);
                ResetPickUpTextAlpha();
            }
        }
    }

    private void ResetPickUpTextAlpha()
    {
        Color bc = _pickupBackgroundImage.color;
        bc.a = 1;

        _pickupBackgroundImage.color = bc;

        Color tc = _pickupText.color;
        tc.a = 1;

        _pickupText.color = tc;
    }

    private void PlayPauseSound()
    {
        AkSoundEngine.PostEvent(Menu_PauseID, gameObject);
    }

    private void PlayContinueSound()
    {
        AkSoundEngine.PostEvent(Menu_ContinueID, gameObject);
    }

    private void PlayReturnMainMenuSound()
    {
        AkSoundEngine.PostEvent(Menu_ReturnMainMenuID, gameObject);
    }

    public void PlaySliderReleaseSound()
    {
        AkSoundEngine.PostEvent(Menu_SliderReleaseID, gameObject);
    }

    private void UpdateKillCounter()
    {
        killCounterText.text = "Kills: " + killCount;
    }

    public void AddKillToCounter()
    {
        killCount++;
        UpdateKillCounter();
    }
}
