﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class WaterRise : MonoBehaviour
{
    [SerializeField] private float _waterRiseSpeed = 1f;
    [SerializeField] private GameObject water = null;


    // Update is called once per frame
    void Update()
    {
        RiseWater();
    }

    void RiseWater()
    {
        //for (float i = 0; i < 1; i=i+0.1f)
        //{
        //    water.transform.Translate(0,i*Time.deltaTime,0);
        //}

        water.transform.position += new Vector3(0f, Time.deltaTime * _waterRiseSpeed, 0f);

        GameManager.s_instance.WaterHight = water.transform.position.y;
    }
}
