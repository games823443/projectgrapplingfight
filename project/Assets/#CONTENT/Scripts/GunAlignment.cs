﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunAlignment : MonoBehaviour
{
    [SerializeField] private GameObject _refGO = null;
     
    // Update is called once per frame
    void Update()
    {
        transform.rotation = _refGO.transform.rotation;
    }
}
