﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Rewired;
using UnityEngine.EventSystems;

public class MainMenuManager : MonoBehaviour
{
    [SerializeField] private GameObject _mainScreen = null;
    [SerializeField] private GameObject _instructionsScreen = null;
    [SerializeField] private GameObject _creditsScreen = null;
    [SerializeField] private GameObject _startButton = null;
    [SerializeField] private GameObject _backButton = null;
    [SerializeField] private Scrollbar _audioVolumeScrollbar = null;
    private Player rewiredPlayer;

    private bool mouseFocus = true;

    GameObject lastSelected;

    //---Wwise Events---//

    //menu sounds
    public AK.Wwise.Event Menu_Start = new AK.Wwise.Event();
    public uint Menu_StartID { get { return (uint)(Menu_Start == null ? AkSoundEngine.AK_INVALID_PLAYING_ID : Menu_Start.Id); } }

    public AK.Wwise.Event Menu_SliderRelease = new AK.Wwise.Event();
    public uint Menu_SliderReleaseID { get { return (uint)(Menu_SliderRelease == null ? AkSoundEngine.AK_INVALID_PLAYING_ID : Menu_SliderRelease.Id); } }

    public AK.Wwise.Event Menu_Click = new AK.Wwise.Event();
    public uint Menu_ClickID { get { return (uint)(Menu_Click == null ? AkSoundEngine.AK_INVALID_PLAYING_ID : Menu_Click.Id); } }

    //music and atmo
    public AK.Wwise.Event Ambience_Wind = new AK.Wwise.Event();
    public uint Ambience_WindID { get { return (uint)(Ambience_Wind == null ? AkSoundEngine.AK_INVALID_PLAYING_ID : Ambience_Wind.Id); } }

    public AK.Wwise.Event Ambience_Rain = new AK.Wwise.Event();
    public uint Ambience_RainID { get { return (uint)(Ambience_Rain == null ? AkSoundEngine.AK_INVALID_PLAYING_ID : Ambience_Rain.Id); } }

    public AK.Wwise.Event Music = new AK.Wwise.Event();
    public uint MusicID { get { return (uint)(Music == null ? AkSoundEngine.AK_INVALID_PLAYING_ID : Music.Id); } }

    //RTPCs
    [SerializeField]
    private AK.Wwise.RTPC _sfxVolume = null;

    [SerializeField]
    private AK.Wwise.RTPC _musicVolume = null;

    private void Awake()
    {
        float volume = PlayerPrefs.GetFloat("Volume", 100f);
        _audioVolumeScrollbar.value = volume;

        _sfxVolume.SetGlobalValue(volume * 100f);
        _musicVolume.SetGlobalValue(volume * 100f);

        rewiredPlayer = ReInput.players.GetPlayer(0);

        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;

        lastSelected = _backButton;

        EnableMainScreen();
        PlayAmbienceRain();
        PlayAmbienceWind();
        PlayMusic();

        AkSoundEngine.SetState("Music_Global", "Menu");
    }

    public void StartGame()
    {
        PlayStartSound();
        SceneManager.LoadScene("SampleScene");
    }

    private void Update()
    {
        if (mouseFocus)
        {
            if (rewiredPlayer.GetAnyButton() && !rewiredPlayer.GetButton("ShootHook") && rewiredPlayer.GetAxis("HorizontalLook") == 0.0f && rewiredPlayer.GetAxis("VerticalLook") == 0.0f)
            {
                Cursor.visible = false;
                Cursor.lockState = CursorLockMode.Locked;

                EventSystem.current.SetSelectedGameObject(lastSelected);

                mouseFocus = false;
            }
        }
        else
        {
            if (rewiredPlayer.GetAxis("HorizontalLook") != 0.0f || rewiredPlayer.GetAxis("VerticalLook") != 0.0f)
            {
                lastSelected = EventSystem.current.currentSelectedGameObject;
                EventSystem.current.SetSelectedGameObject(null);

                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.None;

                mouseFocus = true;
            }
        }

        if (rewiredPlayer.GetButton("Back"))
        {
            PlayClickSound();
            EnableMainScreen();
        }
    }

    public void Quit()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }

    public void EnableInstructions()
    {
        PlayClickSound();
        _instructionsScreen.SetActive(true);
        _creditsScreen.SetActive(false);
        _mainScreen.SetActive(false);
        lastSelected = _backButton;
        if (!mouseFocus)
        {
            EventSystem.current.SetSelectedGameObject(lastSelected);
        }
    }

    public void EnableMainScreen()
    {
        PlayClickSound();
        _instructionsScreen.SetActive(false);
        _creditsScreen.SetActive(false);
        _mainScreen.SetActive(true);
        lastSelected = _startButton;
        if (!mouseFocus)
        {
            EventSystem.current.SetSelectedGameObject(lastSelected);
        }
    }

    public void EnableCreditsScreen()
    {
        PlayClickSound();
        _instructionsScreen.SetActive(false);
        _creditsScreen.SetActive(true);
        _mainScreen.SetActive(false);
        lastSelected = _backButton;
        if (!mouseFocus)
        {
            EventSystem.current.SetSelectedGameObject(lastSelected);
        }
    }

    public void AudioVolumeChanged()
    {
        //PlaySliderReleaseSound();
        AudioListener.volume = _audioVolumeScrollbar.value;

        _sfxVolume.SetGlobalValue(_audioVolumeScrollbar.value * 100f);
        _musicVolume.SetGlobalValue(_audioVolumeScrollbar.value * 100f);

        PlayerPrefs.SetFloat("Volume", _audioVolumeScrollbar.value);
        PlayerPrefs.Save();
        if (mouseFocus)
        {
            EventSystem.current.SetSelectedGameObject(null);
        }
    }

    private void PlayStartSound()
    {
        AkSoundEngine.PostEvent(Menu_StartID, gameObject);
    }

    public void PlaySliderReleaseSound()
    {
        AkSoundEngine.PostEvent(Menu_SliderReleaseID, gameObject);
    }

    private void PlayClickSound()
    {
        AkSoundEngine.PostEvent(Menu_ClickID, gameObject);
    }

    private void PlayAmbienceWind()
    {
        AkSoundEngine.PostEvent(Ambience_WindID, gameObject);
    }

    private void PlayAmbienceRain()
    {
        AkSoundEngine.PostEvent(Ambience_RainID, gameObject);
    }

    private void PlayMusic()
    {
        AkSoundEngine.PostEvent(MusicID, gameObject);
    }
}
