using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreManager : MonoBehaviour
{
    // Start is called before the first frame update
    public static ScoreManager s_instance;

    [SerializeField] private float bossAdd = 1000.0f;
    [SerializeField] private float headShotAdd = 20.0f;
    [SerializeField] private float enemiePointsAdd = 100.0f;
    [SerializeField] private float stunnAdd = 5.0f;
    [SerializeField] private float comboAdd = 10.0f;
    [SerializeField] private float comboHeadshotAdd = 100.0f;
    
    private float _timeNeeded = 0.0f;
    private float _bossPoints = 0.0f;
    private float _headshotPoints = 0.0f;
    private float _enemiePoints = 0.0f;
    private float _stunPoints = 0.0f;
    private float _comboPoints = 0.0f;
    private float _comboHeadshotPoints = 0.0f;
    private bool _startTimer = false;
    void Start()
    {
        if (s_instance == null)
            s_instance = this;
        else
            Debug.LogWarning("ScoreManager already exists!");
    }

    // Update is called once per frame
    void Update()
    {
        if (_startTimer)
        {
            _timeNeeded = Time.timeSinceLevelLoad;
        }
    }
    
    public void AddPoints(PointType type)
    {
        switch (type)
        {
            case PointType.NONE:
                break;
            case PointType.BOSS:
                _bossPoints += 100;
                break;
            case PointType.STUN:
                break;
            case PointType.COMBO:
                break;
            case PointType.HEADSHOT:
                break;
            case PointType.COMBOHEADSHOT:
                break;
            case PointType.ENEMIE:
                break;
            default:
                break;
        }
    }

    public void getFullScore()
    {
        
    }

    public void startTimer()
    {
        
    }

    public void endTimer()
    {
        
    }




    public enum PointType { NONE, BOSS, HEADSHOT, ENEMIE, STUN,COMBO, COMBOHEADSHOT }
}
