﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PullableObject : MonoBehaviour
{
    [Range(0, 1)]
    [SerializeField] private float _damageMultiplier = 0.5f;
    private Rigidbody _rigidbody;

    private GameObject _detachedHook = null;
    private float _backVelocity = 2000.0f;

    private bool _hit = false;
    private Vector3 _hitOffset = new Vector3(0.0f, 5.0f, 0.0f);

    private Collider _collider;
    private Renderer _renderer;

    private Material _originalMaterial;
    private bool _isHooked = false;

    private float _hitMultiplier = 1.0f;

    private GameObject _fireEffect = null;
    private bool _isSleeping = false;

    private Outline _outline;
    private bool _initOutline = true;

    [SerializeField] AttachMaterials _objectMaterial = AttachMaterials.NONE;
    public AttachMaterials AttachMaterial => _objectMaterial;

    private float _soundTimer = 0f;
    [SerializeField] private float _soundCooldown = 0.5f;

    //---Wwise Events---//

    //collision sounds
    public AK.Wwise.Event Item_Collide = new AK.Wwise.Event();
    public uint Item_CollideID { get { return (uint)(Item_Collide == null ? AkSoundEngine.AK_INVALID_PLAYING_ID : Item_Collide.Id); } }

    public AK.Wwise.Event Item_ExtraDamage_Burning_Start = new AK.Wwise.Event();
    public uint Item_ExtraDamage_Burning_StartID { get { return (uint)(Item_ExtraDamage_Burning_Start == null ? AkSoundEngine.AK_INVALID_PLAYING_ID : Item_ExtraDamage_Burning_Start.Id); } }

    public AK.Wwise.Event Item_ExtraDamage_Burning_Stop = new AK.Wwise.Event();
    public uint Item_ExtraDamage_Burning_StopID { get { return (uint)(Item_ExtraDamage_Burning_Stop == null ? AkSoundEngine.AK_INVALID_PLAYING_ID : Item_ExtraDamage_Burning_Stop.Id); } }

    private void Start()
    {
        _rigidbody = GetComponent<Rigidbody>();
        _collider = GetComponent<Collider>();

        _renderer = GetComponent<Renderer>();

        if (_renderer == null)
            _renderer = GetComponentInChildren<Renderer>();

        _originalMaterial = _renderer.material;

        NavMeshObstacle navmesho = GetComponent<NavMeshObstacle>();
        if(navmesho != null)
            navmesho.enabled = false;

        _outline = gameObject.AddComponent<Outline>();
        _outline.enabled = false;
    }

    private void Update()
    {
        if(GameManager.s_instance.Sleep(transform.position))
        {
            _isSleeping = true;
            _rigidbody.Sleep();
            return;
        }

        if(_initOutline)
        {
            _outline.OutlineWidth = GameManager.s_instance.ObjectOutlineWidth;
            _outline.OutlineMode = Outline.Mode.OutlineAll;
            _outline.OutlineColor = GameManager.s_instance.ObjectOutlineColor;

            _initOutline = false;
        }
        
        if(_isSleeping && !GameManager.s_instance.Sleep(transform.position))
        {
            _isSleeping = false;
            _rigidbody.WakeUp();
        }

        if(_hit)
        {
            _rigidbody.AddForce(_hitOffset);
        }

        if(_soundTimer != 0f)
        {
            _soundTimer -= Time.deltaTime;

            if (_soundTimer <= 0f)
                _soundTimer = 0f;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform == null || _rigidbody == null)
            return;

        if (_soundTimer <= 0f)
        {
            PlayCollisionSound();
            _soundTimer = _soundCooldown;
        }

        if (collision.transform.CompareTag("enemy") && _rigidbody.velocity.magnitude > 25.0f)
        {
            if (_hit && _hitMultiplier < 5.0f)
            {
                _hitMultiplier *= 1.2f;
            }

            float velocity = _rigidbody.velocity.magnitude;

            float damage = (velocity * 0.25f * _rigidbody.mass * 0.01f) * _damageMultiplier * _hitMultiplier;

            Enemy enemyComp = collision.transform.GetComponent<Enemy>();
            enemyComp.TakeDamage(damage, transform.position.y);

            GameObject bloodEffect = GameObject.Instantiate(GameManager.s_instance.BloodSplatterEffect, collision.GetContact(0).point, Quaternion.identity);
            bloodEffect.transform.LookAt(collision.GetContact(0).point + collision.GetContact(0).normal);

            if (_detachedHook != null)
                Destroy(_detachedHook);

            _rigidbody.velocity = Vector3.zero;
            _rigidbody.angularVelocity = Vector3.zero;
            _rigidbody.AddForce(Vector3.up * _backVelocity * _rigidbody.mass);

            if (!_hit)
                PlayFireStartSound();

            _hit = true;

            _renderer.material = GameManager.s_instance.RedMaterial;
            _fireEffect = Instantiate(GameManager.s_instance.FireEffect);
            _fireEffect.transform.parent = transform;
            _fireEffect.transform.localPosition = Vector3.zero;

            _collider.enabled = false;
            StartCoroutine(WaitToReactivateCollider());

        }
        else
        {
            if (!collision.transform.CompareTag("Player") && _hit && !_isHooked)
            {
                ResetHit();
            }
        }
    }

    private void ResetHit()
    {
        _hit = false;
        _renderer.material = _originalMaterial;
        _hitMultiplier = 1.0f;
        Destroy(_fireEffect);
        PlayFireStopSound();
    }

    public void SetDetachedHook(GameObject detachedHook)
    {
        _detachedHook = detachedHook;
    }

    private IEnumerator WaitToReactivateCollider()
    {
        yield return new WaitForSeconds(0.5f);

        _collider.enabled = true;
    }

    public void SetIsHooked(bool isHooked)
    {
        _isHooked = isHooked;
    }

    private void PlayCollisionSound()
    {
        AkSoundEngine.PostEvent(Item_CollideID, gameObject);
    }

    private void PlayFireStartSound()
    {
        AkSoundEngine.PostEvent(Item_ExtraDamage_Burning_StartID, gameObject);
    }

    private void PlayFireStopSound()
    {
        AkSoundEngine.PostEvent(Item_ExtraDamage_Burning_StopID, gameObject);
    }

    public void SetObjectMaterial(AttachMaterials material)
    {
        _objectMaterial = material;
    }
}
