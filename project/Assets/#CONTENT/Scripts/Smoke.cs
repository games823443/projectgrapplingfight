using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Smoke : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        UpdateSmoke();
    }
    
    private void UpdateSmoke()
    {
        Dictionary<int,GameObject>bossPlatforms=MapGenerator.s_instance.ActiveBossPlaforms;
        GameObject currentPlatform=bossPlatforms.FirstOrDefault(x => x.Key == bossPlatforms.Keys.Min()).Value;
        gameObject.transform.LookAt(currentPlatform.transform);
        
    }
}
