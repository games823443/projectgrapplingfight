﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using Debug = UnityEngine.Debug;

public class PowerUp : MonoBehaviour
{
   public GameObject PickUpEffect;
   public float Duration = 10f;
   public float EffectStrength = 2.5f;
   private GameObject Effect;

    //---Wwise Events---//

    //spawn sounds
    public AK.Wwise.Event Item_Spawn_Health = new AK.Wwise.Event();
    public uint Item_Spawn_HealthID { get { return (uint)(Item_Spawn_Health == null ? AkSoundEngine.AK_INVALID_PLAYING_ID : Item_Spawn_Health.Id); } }

    public AK.Wwise.Event Item_Spawn_Slow_Mo = new AK.Wwise.Event();
    public uint Item_Spawn_Slow_MoID { get { return (uint)(Item_Spawn_Slow_Mo == null ? AkSoundEngine.AK_INVALID_PLAYING_ID : Item_Spawn_Slow_Mo.Id); } }

    public AK.Wwise.Event Item_Spawn_Magnet = new AK.Wwise.Event();
    public uint Item_Spawn_MagnetID { get { return (uint)(Item_Spawn_Magnet == null ? AkSoundEngine.AK_INVALID_PLAYING_ID : Item_Spawn_Magnet.Id); } }

    public AK.Wwise.Event Item_Spawn_HookLength = new AK.Wwise.Event();
    public uint Item_Spawn_HookLengthID { get { return (uint)(Item_Spawn_HookLength == null ? AkSoundEngine.AK_INVALID_PLAYING_ID : Item_Spawn_HookLength.Id); } }

    public AK.Wwise.Event Item_Spawn_HookSpeed = new AK.Wwise.Event();
    public uint Item_Spawn_HookSpeedID { get { return (uint)(Item_Spawn_HookSpeed == null ? AkSoundEngine.AK_INVALID_PLAYING_ID : Item_Spawn_HookSpeed.Id); } }

    public AK.Wwise.Event Item_Spawn_HookDamage = new AK.Wwise.Event();
    public uint Item_Spawn_HookDamageID { get { return (uint)(Item_Spawn_HookDamage == null ? AkSoundEngine.AK_INVALID_PLAYING_ID : Item_Spawn_HookDamage.Id); } }

    public AK.Wwise.Event Item_Spawn_Speed = new AK.Wwise.Event();
    public uint Item_Spawn_SpeedID { get { return (uint)(Item_Spawn_Speed == null ? AkSoundEngine.AK_INVALID_PLAYING_ID : Item_Spawn_Speed.Id); } }


    //pickup sounds
    public AK.Wwise.Event Item_PickUp_Health = new AK.Wwise.Event();
    public uint Item_PickUp_HealthID { get { return (uint)(Item_PickUp_Health == null ? AkSoundEngine.AK_INVALID_PLAYING_ID : Item_PickUp_Health.Id); } }

    public AK.Wwise.Event Item_PickUp_Slow_Mo = new AK.Wwise.Event();
    public uint Item_PickUp_Slow_MoID { get { return (uint)(Item_PickUp_Slow_Mo == null ? AkSoundEngine.AK_INVALID_PLAYING_ID : Item_PickUp_Slow_Mo.Id); } }

    public AK.Wwise.Event Item_PickUp_Magnet = new AK.Wwise.Event();
    public uint Item_PickUp_MagnetID { get { return (uint)(Item_PickUp_Magnet == null ? AkSoundEngine.AK_INVALID_PLAYING_ID : Item_PickUp_Magnet.Id); } }

    public AK.Wwise.Event Item_PickUp_HookLength = new AK.Wwise.Event();
    public uint Item_PickUp_HookLengthID { get { return (uint)(Item_PickUp_HookLength == null ? AkSoundEngine.AK_INVALID_PLAYING_ID : Item_PickUp_HookLength.Id); } }

    public AK.Wwise.Event Item_PickUp_HookSpeed = new AK.Wwise.Event();
    public uint Item_PickUp_HookSpeedID { get { return (uint)(Item_PickUp_HookSpeed == null ? AkSoundEngine.AK_INVALID_PLAYING_ID : Item_PickUp_HookSpeed.Id); } }

    public AK.Wwise.Event Item_PickUp_HookDamage = new AK.Wwise.Event();
    public uint Item_PickUp_HookDamageID { get { return (uint)(Item_PickUp_HookDamage == null ? AkSoundEngine.AK_INVALID_PLAYING_ID : Item_PickUp_HookDamage.Id); } }

    public AK.Wwise.Event Item_PickUp_Speed = new AK.Wwise.Event();
    public uint Item_PickUp_SpeedID { get { return (uint)(Item_PickUp_Speed == null ? AkSoundEngine.AK_INVALID_PLAYING_ID : Item_PickUp_Speed.Id); } }
    public enum PickUpType
   {
      SPEED = 0,
      SLOWMOTION = 1,
      HEALTH = 2,
      HOOKLENGTH=3,
      HOOKDAMAGE=4,
      HOOKSPEED=5,
      MAGNETIC=6
   }

   public PickUpType Type;

    private void Start()
    {
        PlayItemSpawnSound();
    }

    void OnTriggerEnter(Collider other)
   {
      if (other.CompareTag("Player"))
      {
         foreach (Renderer r in gameObject.GetComponentsInChildren<Renderer>())
            r.enabled = false;
         gameObject.GetComponent<Collider>().enabled = false;
         Pickup(other);
      }
   }

   private void Pickup(Collider player)
   { 
      Effect= Instantiate(PickUpEffect, transform.position, transform.rotation);

      switch (Type)
      {
         case PickUpType.HEALTH:
            Health();
            AkSoundEngine.PostEvent(Item_PickUp_HealthID, gameObject);
            UIManager.s_instance.ShowPickupText("Health +" + EffectStrength);
            break;
         case PickUpType.SPEED:
            StartCoroutine(Speed());
                AkSoundEngine.PostEvent(Item_PickUp_SpeedID, gameObject);
                UIManager.s_instance.ShowPickupText("Speed x" + EffectStrength + " for " + Duration + " seconds");
            break;
         case PickUpType.SLOWMOTION:
            StartCoroutine(SlowMotion());
                AkSoundEngine.PostEvent(Item_PickUp_Slow_MoID, gameObject);
                UIManager.s_instance.ShowPickupText("SlowMotion duration + " + EffectStrength + " for " + Duration + " seconds");
            break;
         case PickUpType.HOOKLENGTH:
            IncreaseHookLength();
                AkSoundEngine.PostEvent(Item_PickUp_HookLengthID, gameObject);
                UIManager.s_instance.ShowPickupText("Increase HookLength by " + EffectStrength);
            break;
         case PickUpType.HOOKDAMAGE:
            IncreaseHookDamage();
                AkSoundEngine.PostEvent(Item_PickUp_HookDamageID, gameObject);
                UIManager.s_instance.ShowPickupText("Increase HookDamage by " + EffectStrength);
            break;
         case PickUpType.HOOKSPEED:
            IncreaseHookSpeed();
                AkSoundEngine.PostEvent(Item_PickUp_HookSpeedID, gameObject);
                UIManager.s_instance.ShowPickupText("Increase HookSpeed by " + EffectStrength);
            break;
         case PickUpType.MAGNETIC:
            EnableMagnetic();
                AkSoundEngine.PostEvent(Item_PickUp_MagnetID, gameObject);
                UIManager.s_instance.ShowPickupText("Enable Magnetic Hook");
            break;
         default:
            Debug.Log("Error no valid pickup type");
            break;
      }
      Destroy(Effect,1);
   }

   private void IncreaseHookLength()
   {      
        GameManager.s_instance.Hook.IncreaseRange(EffectStrength);
      Destroy();
   }

   private void Health()
   {
      GameManager.s_instance.CharacterController.SetHealth(EffectStrength);
        GameManager.s_instance.CharacterController.UpdateHealthBar();
      Destroy();
   }
   private IEnumerator Speed()
   {
       GameManager.s_instance.CharacterController._speed *=
         EffectStrength;
     // Debug.Log("Speed applied");
      yield return new WaitForSeconds(Duration);
        GameManager.s_instance.CharacterController._speed /= EffectStrength;
      //Debug.Log("Speed removed");
      
      Destroy();

   }
   private IEnumerator SlowMotion()
   {
        GameManager.s_instance.CharacterController._maxSlowmotionTime +=
         EffectStrength;
      // Debug.Log("Speed applied");
      yield return new WaitForSeconds(Duration);
        GameManager.s_instance.CharacterController._maxSlowmotionTime -= EffectStrength;
      //Debug.Log("Speed removed");
      
      Destroy();
   }

    private void IncreaseHookDamage()
    {
        GameManager.s_instance.Hook.IncreaseHookDamage(EffectStrength);
        Destroy();
    }

    private void EnableMagnetic()
    {
        GameManager.s_instance.Hook.EnableMagnetic();
        Destroy();
    }

    private void IncreaseHookSpeed()
    {
        GameManager.s_instance.Hook.IncreaseHookSpeed(EffectStrength);
        Destroy();
    }

   private void Destroy()
   {
      Destroy(gameObject);
   }

    private void PlayItemSpawnSound()
    {
        switch(Type)
        {
            case PickUpType.HEALTH:
                AkSoundEngine.PostEvent(Item_Spawn_HealthID, gameObject);
                break;
            case PickUpType.SLOWMOTION:
                AkSoundEngine.PostEvent(Item_Spawn_Slow_MoID, gameObject);
                break;
            case PickUpType.MAGNETIC:
                AkSoundEngine.PostEvent(Item_Spawn_MagnetID, gameObject);
                break;
            case PickUpType.HOOKLENGTH:
                AkSoundEngine.PostEvent(Item_Spawn_HookLengthID, gameObject);
                break;
            case PickUpType.HOOKSPEED:
                AkSoundEngine.PostEvent(Item_Spawn_HookSpeedID, gameObject);
                break;
            case PickUpType.HOOKDAMAGE:
                AkSoundEngine.PostEvent(Item_Spawn_HookDamageID, gameObject);
                break;
            case PickUpType.SPEED:
                AkSoundEngine.PostEvent(Item_Spawn_SpeedID, gameObject);
                break;
        }
    }
      
}
