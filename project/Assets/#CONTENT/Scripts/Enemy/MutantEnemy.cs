using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MutantEnemy : Enemy
{
    [SerializeField] private float _attackAnimDuration = 2.0f;
    [SerializeField] private float _timeBeforeAttack = 1f;
    [SerializeField] private float _damage = 1;
    [SerializeField] private float actualAttackDistance = 5;
    //[SerializeField] private float _attackDistanceMutant = 1;
    private bool _attacking = false;
    private bool _canDoDamage = false;
    private bool _didDamage = false;

    public bool IsAttacking => _attacking;
    public bool CanDoDamage => _canDoDamage;
    public float Damage => _damage;
    public bool DidDamage => _didDamage;

    public AK.Wwise.Event Sliding_Pain = new AK.Wwise.Event();
    public AK.Wwise.Event Sliding_Death = new AK.Wwise.Event();
    public AK.Wwise.Event Sliding_Idle = new AK.Wwise.Event();

    // Start is called before the first frame update
    void Start()
    {
        SetFollowPlayer(true);
        //_attackDistance = _attackDistanceMutant;

        _enemyType = EnemyTypes.SLIDING;

        Sliding_Pain_base = Sliding_Pain;
        Sliding_Death_base = Sliding_Death;
        Sliding_Idle_base = Sliding_Idle;
    }

    // Update is called once per frame
    protected override void Update()
    {
        if(_multiplierSet && !_damageMultiplierSet)
        {
            _damage *= _damageMultiplier;
            _damageMultiplierSet = true;
        }

        if(!_dead)
        {
            if (_rigidbody.isKinematic == false)
            {
                base.Update();
                CheckAttack();
            }
        }
    }

    private void CheckAttack()
    {
        if(_characterCloseEnough)
        {
            if ((!_attacking) && Vector3.Distance(transform.position, GameManager.s_instance.GetCharacterController().transform.position) < actualAttackDistance)
                Attack();
        }
    }

    private void Attack()
    {
        _attacking = true;
        StopMovement();

        _animator.SetTrigger("attack");

        StartCoroutine(WaitToDoDamage());
        StartCoroutine(WaitForAttackAnimEnd());
    }

    private IEnumerator WaitToDoDamage()
    {
        yield return new WaitForSeconds(_timeBeforeAttack);
        _canDoDamage = true;
        GameManager.s_instance.GetCharacterController().TakeDamage(Damage);
        DoDamage();
    }

    private IEnumerator WaitForAttackAnimEnd()
    {
        yield return new WaitForSeconds(_attackAnimDuration);
        _attacking = false;
        _canDoDamage = false;
        //_didDamage = false;
        ResumeMovement();
    }

    public void DoDamage()
    {
        _didDamage = true;
    }
}
