﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrabBossClaws : MonoBehaviour
{
    [SerializeField] private CrabBoss _parent = null;

    private void OnTriggerEnter(Collider other)
    {
        //Debug.Log("Hit EnemyCheck:"+other.CompareTag("enemy"));
        if (!other.CompareTag("enemy"))
        {
            if (other.CompareTag("Player") && _parent.CanDoDamage && !_parent.DidDamage)
            {
                GameManager.s_instance.GetCharacterController().TakeDamage(_parent.Damage, EnemyTypes.CRAB);
                _parent.DoDamage();
                GameManager.s_instance.GetCharacterController().KnockBack();
            }
        }
    }

}
