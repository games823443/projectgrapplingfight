﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmallEnemyWeapon : MonoBehaviour
{
    [SerializeField] private SmallEnemy _parent = null;

    private void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag("enemy"))
        {
            if (other.CompareTag("Player") && _parent.CanDoDamage && !_parent.DidDamage)
            {
                GameManager.s_instance.GetCharacterController().TakeDamage(_parent.Damage, EnemyTypes.SKELETON);
                _parent.DoDamage();
            }
        }
    }
}
