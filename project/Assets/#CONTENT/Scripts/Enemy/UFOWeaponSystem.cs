using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using _CONTENT.Scripts.Enemy;
using UnityEngine;

enum WEAPONSTATE {
    AIM,
    SHOOT
}

public class UFOWeaponSystem : MonoBehaviour {
    private LineRenderer[] lr;
    private GameObject targeting;
    private WEAPONSTATE curState;
    [SerializeField] private float rotationSpeed = 25;
    [SerializeField] private float upDownSpeed = 25;
    [SerializeField] private GameObject plasmaBall;
    [SerializeField] private float shootingCooldown = 1.5f;
    [SerializeField] private int maxShots = 5;
    private float shootTimeAccu;
    private int shotsFired = 0;
    private float timeAccuAim;
    private float switchToShootingStateCooldownAccu;
    [SerializeField] private float switchToShootingStateCooldownValue = 5f;
    private float switchToShootingStateCooldownRequired;


    private void Start() {
        lr = GetComponentsInChildren<LineRenderer>();
        targeting = transform.Find("Targeting")?.gameObject;
        SwitchToAimState();
    }

    public void Enable() {
        gameObject.SetActive(true);
        SwitchToAimState();
    }

    public void Disable() {
        gameObject.SetActive(false);
    }

    public void MyUpdate() {
        switch (curState) {
            case WEAPONSTATE.AIM:
                AimBehavior();
                break;
            case WEAPONSTATE.SHOOT:
                ShootingLogic();
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    private void SwitchToShootState() {
        shootTimeAccu = shootingCooldown; //enable instant shooting
        shotsFired = 0;
        curState = WEAPONSTATE.SHOOT;
    }

    private void SwitchToAimState(float cooldown = 0) {
        curState = WEAPONSTATE.AIM;
        switchToShootingStateCooldownAccu = 0;
        switchToShootingStateCooldownRequired = cooldown;
    }

    private void ShootingLogic() {
        shootTimeAccu += Time.deltaTime;
        if (shootTimeAccu >= shootingCooldown) {
            Shoot();
            shootTimeAccu = 0;
            shotsFired++;
        }

        if (shotsFired >= maxShots) {
            SwitchToAimState(switchToShootingStateCooldownValue);
        }
    }

    private Vector4 ToPositionVec(Vector3 pos) {
        return new Vector4(pos.x, pos.y, pos.z, 1);
    }

    private void Shoot() {
        foreach (LineRenderer lineRenderer in lr) {
            var trans = lineRenderer.transform;
            var ltW = lineRenderer.transform.localToWorldMatrix;
            var pos = trans.position;
            var start = ltW * ToPositionVec(lineRenderer.GetPosition(0));
            var end = ltW * ToPositionVec(lineRenderer.GetPosition(1));
            var forward = (end - start).normalized;
            var rotation = Quaternion.LookRotation(forward, Vector3.up);
            var pb = Instantiate(plasmaBall, pos, rotation);
            //Debug.DrawRay(pos,forward*500);
            pb.transform.forward = forward;
        }
    }

    private void AimBehavior() {
        MainCharacterController controller = GameManager.s_instance.GetCharacterController();
        float rotation = (timeAccuAim * rotationSpeed) % 360;
        targeting.transform.rotation = Quaternion.Euler(0, rotation, 0);
        timeAccuAim += Time.deltaTime;
        switchToShootingStateCooldownAccu += Time.deltaTime;
        //var controllerPos = controller.GetComponent<CapsuleCollider>().center;

        var targetRot = CalculateTargetRotation(controller);
        controller.gameObject.layer = LayerMask.NameToLayer("Default");
        DoLaserLogic(targetRot);
        controller.gameObject.layer = LayerMask.NameToLayer("Ignore Raycast");
    }

    private Quaternion CalculateTargetRotation(MainCharacterController controller) {
        var controllerPos = controller.transform.position;
        controllerPos.y += 2; //feels better if it is a bit higher
        var dirToPlayer = (controllerPos - transform.position).normalized;
        var targetRot = Quaternion.LookRotation(dirToPlayer, Vector3.up);
        var asEuler = targetRot.eulerAngles;
        asEuler.y = 0;
        asEuler.z = 0;
        targetRot = Quaternion.Euler(asEuler);
        return targetRot;
    }

    private void DoLaserLogic(Quaternion targetRot) {
        int layerUfo = LayerMask.NameToLayer("UFO");
        foreach (var lineRenderer in lr) {
            HandleUpDownMovementOfLasers(lineRenderer, targetRot);
        }

        var data = lr.Select(lineRenderer => {
            var transform = lineRenderer.transform;
            bool hitSomething = Physics.Raycast(transform.position, transform.forward,
                out RaycastHit hit, 100, layerUfo);
            return (hitSomething, hit, lineRenderer);
        }).ToList();

        foreach (var (hitSomething, hit, lineRenderer) in data) {
            if (hitSomething) {
                LimitLaserLength(hit, lineRenderer);
            }
            else {
                lineRenderer.SetPosition(1, new Vector3(0, 0, 20)); //20 is default in prefab
            }
        }

        SetTargetingLightRange();

        bool hitPlayer = data.Any(tuple => tuple.hitSomething && tuple.hit.collider.CompareTag("Player"));
        if (hitPlayer && switchToShootingStateCooldownAccu >= switchToShootingStateCooldownRequired) {
            SwitchToShootState();
        }
    }

    private void SetTargetingLightRange() {
        foreach (var lineRenderer in lr) {
            var lightSource = lineRenderer.gameObject.GetComponent<Light>();
            var start = lineRenderer.GetPosition(0);
            var end = lineRenderer.GetPosition(1);
            var length = (start - end).magnitude;
            lightSource.range = (length * 5) + 5;
            //5 is the combined scale, it also needs to be a bit longer then the actual distance, otherwise it does not reach the geometry
        }
    }

    private static void LimitLaserLength(RaycastHit hit, LineRenderer lineRenderer) {
        var hitPos = new Vector4(hit.point.x, hit.point.y, hit.point.z, 1f);
        var localEndPos = lineRenderer.transform.worldToLocalMatrix * hitPos;
        lineRenderer.SetPosition(1, new Vector3(localEndPos.x, localEndPos.y, localEndPos.z));
    }

    private void HandleUpDownMovementOfLasers(LineRenderer lineRenderer, Quaternion targetRot) {
        var curRotation = lineRenderer.transform.rotation;
        var oldEuler = curRotation.eulerAngles;
        curRotation = Quaternion.Euler(oldEuler.x, 0, 0);
        var rotateTowards = Quaternion.RotateTowards(curRotation, targetRot, upDownSpeed * Time.deltaTime);
        var angles = rotateTowards.eulerAngles;
        angles.y = oldEuler.y;
        angles.z = oldEuler.z;
        lineRenderer.transform.rotation = Quaternion.Euler(angles);
    }
}