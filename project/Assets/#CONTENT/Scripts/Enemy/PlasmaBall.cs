using System;
using System.Collections;
using System.Collections.Generic;
using _CONTENT.Scripts.Enemy;
using UnityEngine;

public class PlasmaBall : MonoBehaviour {
    [SerializeField] private float timeBetweenDamageTicks = 2f;
    [SerializeField] private float lifeTime = 10f;
    [SerializeField] private int damage = 1;
    [SerializeField] private float speed = 15f;

    private float lifeTimeAccu;
    private float timeSinceLastDamageTick;

    // Update is called once per frame
    void Update() {
        lifeTimeAccu += Time.deltaTime;
        MoveForward();
        if (lifeTimeAccu > lifeTime) {
            Despawn();
        }
    }

    private void MoveForward() {
        transform.position += transform.forward * speed * Time.deltaTime;
    }

    private void Despawn() {
        //maybe do last big explosion?
        enabled = false;
        Destroy(gameObject);
    }

    private bool CollisionWithPlayer(Collider other) {
        return !other.CompareTag("enemy") && other.CompareTag("Player");
    }

    private void OnTriggerEnter(Collider other) {
        if (CollisionWithPlayer(other)) {
            timeSinceLastDamageTick = 0;
            GameManager.s_instance.GetCharacterController().TakeDamage(damage);
        }
        else if (other.CompareTag("forceField"))
        {
            Physics.IgnoreCollision(GetComponent<Collider>(), other);
        }
        else
        {
            Despawn();
        }
    }
    
    private void OnCollisionStay(Collision other) {
        if (!CollisionWithPlayer(other.collider)) return;
        timeSinceLastDamageTick += Time.deltaTime;
        if (timeSinceLastDamageTick >= timeBetweenDamageTicks) {
            GameManager.s_instance.GetCharacterController().TakeDamage(damage);
            timeSinceLastDamageTick = 0;
        }
    }

    private void OnCollisionExit(Collision other) {
        if (!CollisionWithPlayer(other.collider)) return;
        timeSinceLastDamageTick = 0;
    }
}