﻿namespace _CONTENT.Scripts.Enemy {
    public interface DamageValueProvider {
        public int GetDamage();
    }
}