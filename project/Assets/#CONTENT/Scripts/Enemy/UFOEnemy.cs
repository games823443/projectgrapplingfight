﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

namespace _CONTENT.Scripts.Enemy {
    enum UFOSTATE {
        IDLE,
        FIGHT
    }

    public class UFOEnemy : global::Enemy {
        private const int Y_OFFSET = 25;
        private UFOSTATE curStat;
        private float timeAccu;
        private float spawnHeight;
        private bool shouldMoveTowardsSpawnHeight = false;
        [SerializeField] private float amplitude = 0.15f;
        [SerializeField] private float frequenzy = 0.001f;
        [SerializeField] private float speed = 2f;
        private GameObject forceField;
        [SerializeField] private float forceFieldDuration = 5f;
        [SerializeField] private float forceFieldCooldown = 2f;
        private float forceFieldDurationAccu = 0;
        private float forceFieldCooldownAccu = 0;
        private UFOWeaponSystem wSystem;

        public AK.Wwise.Event UFO_Pain = new AK.Wwise.Event();
        public AK.Wwise.Event UFO_Death = new AK.Wwise.Event();
        public AK.Wwise.Event UFO_Idle = new AK.Wwise.Event();

        protected override void Start() {
            base.Start();

            _enemyType = EnemyTypes.UFO;

            UFO_Pain_base = UFO_Pain;
            UFO_Death_base = UFO_Death;
            UFO_Idle_base = UFO_Idle;

            wSystem = GetComponentInChildren<UFOWeaponSystem>();
            forceFieldCooldownAccu = forceFieldCooldown + 1f; //enable instant activation 
            forceField = transform.Find("ForceField").gameObject;
            int layerUfo = LayerMask.NameToLayer("UFO");
            //int layerDefault = LayerMask.NameToLayer("Default");
            //Physics.IgnoreLayerCollision(layerUfo,layerDefault);
            var spawnPos = transform.position;
            var rayStart = spawnPos;
            rayStart.y += Y_OFFSET + 3;
            bool hitSomething = Physics.Raycast(rayStart, Vector3.down, out RaycastHit hit, 100, layerUfo);
            if (hitSomething) {
                var hitPoint = hit.point;
                //print("hit at height: " + hitPoint.y);
                //print("hit: " + hit.collider.gameObject.name);
                spawnPos.y = hitPoint.y + Y_OFFSET;
                //print("spawn at: " + spawnPos.y);
            }
            else {
                spawnPos.y += Y_OFFSET;
                //print("no hit, height: "+spawnPos.y);
            }

            transform.position = spawnPos;
            spawnHeight = spawnPos.y;
            _rigidbody.useGravity = false;
            SwitchToIdleState();
//            _navMeshAgent.isStopped = true;
        }

/*
        private void OnCollisionEnter(Collision other) {
            print("ufo collided with: "+other.gameObject.name);
        }
*/
        //UFO is stationary
        private protected override bool IsStationary() {
            return true;
        }

        protected override void ResumeMovement() {
            base.ResumeMovement();
            _rigidbody.useGravity = false;
//            _navMeshAgent.isStopped = true;
        }

        protected override void Update() {
            base.Update();

            timeAccu += Time.deltaTime * 1000;
            switch (curStat) {
                case UFOSTATE.IDLE:
                    HandleIDLEState();
                    break;
                case UFOSTATE.FIGHT:
                    HandleFIGHTState();
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void SwitchToIdleState() {
            curStat = UFOSTATE.IDLE;
            _rigidbody.constraints = RigidbodyConstraints.FreezePosition | RigidbodyConstraints.FreezeRotation;
            DeactivateForceField();
            wSystem.Disable();
        }

        private void SwitchToFIGHTState() {
            ActivateWeapons();
            ActivateForceField();
            wSystem.Enable();
            curStat = UFOSTATE.FIGHT;
        }

        private void ActivateForceField() {
            forceFieldCooldownAccu = 0;
            forceFieldDurationAccu = 0;
            forceField.SetActive(true);
            gameObject.tag = "Untagged";
        }

        private void DeactivateForceField() {
            forceFieldCooldownAccu = 0;
            forceFieldDurationAccu = 0;
            forceField.SetActive(false);
            gameObject.tag = "enemy";
        }

        private void HandleForceFieldLogic() {
            if (forceField.activeSelf) {
                forceFieldDurationAccu += Time.deltaTime;
                if (forceFieldDurationAccu >= forceFieldDuration) {
                    DeactivateForceField();
                    //forceField.SetActive(false);
                    //forceFieldDurationAccu = 0;
                    //forceFieldCooldownAccu = 0;
                }
            }
            else {
                forceFieldCooldownAccu += Time.deltaTime;
                if (forceFieldCooldownAccu >= forceFieldCooldown) {
                    ActivateForceField();
                    //forceField.SetActive(true);
                    //forceFieldCooldownAccu = 0;
                    //forceFieldDurationAccu = 0;
                }
            }
        }

        private void HandleIDLEState() {
            if (shouldMoveTowardsSpawnHeight) {
                //print("movetowards spawnheight");
                MoveTowardsSpawnHeight();
            }
            else {
                WobbleUpDown();
            }

            if (_characterCloseEnough) {
                //print("switch to fight");
                SwitchToFIGHTState();
                return;
            }
        }

        private void MoveTowardsSpawnHeight() {
            float dif = transform.position.y - spawnHeight;
            var curPos = transform.position;
            if (dif < speed * Time.deltaTime) {
                curPos.y = spawnHeight;
                transform.position = curPos;
                shouldMoveTowardsSpawnHeight = false;
            }
            else {
                int dir = Math.Sign(dif);
                curPos.y += dir * speed * Time.deltaTime;
            }
        }

        private void WobbleUpDown() {
            float yOffset = (float)(amplitude * Math.Cos(timeAccu * frequenzy));
            float newY = spawnHeight + yOffset;
            var pos = transform.position;
            pos.y = newY;
            transform.position = pos;

            //transform.Translate(0, yOffset, 0);
        }

        private void HandleFIGHTState() {
            if (!_characterCloseEnough) {
                DeactivateWeapons();
                //print("switch back to idle");
                shouldMoveTowardsSpawnHeight = true;
                SwitchToIdleState();
                return;
            }

            if (!IsNearGround()) {
                //print("Movedown");
                MoveDown();
            }

            HandleForceFieldLogic();
            wSystem.MyUpdate();
        }

        private bool IsNearGround() {
            int layerUfo = LayerMask.NameToLayer("UFO");

            var rayStart = transform.position;

            bool hitSomething = Physics.Raycast(rayStart, Vector3.down, out RaycastHit hit, 100, layerUfo);
            if (hitSomething) {
                return hit.distance <= Y_OFFSET;
            }

            return true;
        }

        private void MoveDown() {
            transform.Translate(0, -speed * Time.deltaTime, 0);
        }

        private void ActivateWeapons() { }

        private void DeactivateWeapons() { }
    }
}