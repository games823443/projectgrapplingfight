﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBarLookAtPlayer : MonoBehaviour
{
    private Transform _player;
    private bool _visible = false;

    private void Start()
    {
        gameObject.SetActive(_visible);
    }

    // Update is called once per frame
    void Update()
    {
        if(_player == null)
            _player = GameManager.s_instance.GetCharacterController().transform;

        if(!_visible && !GameManager.s_instance.Sleep(transform.position))
        {
            _visible = true;
            gameObject.SetActive(_visible);
        }
        else if(_visible && GameManager.s_instance.Sleep(transform.position))
        {
            _visible = false;
            gameObject.SetActive(_visible);
        }

        if(_visible)
            transform.LookAt(_player);
    }
}
