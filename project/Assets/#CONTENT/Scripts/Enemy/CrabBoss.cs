﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrabBoss : Enemy
{
    [SerializeField] private float _attackAnimDuration = 1.0f;
    [SerializeField] private float _shootAnimDuration = 4f;
    [SerializeField] private float _timeBeforeAttack = 1.0f;
    [SerializeField] private float _damage = 1;
    [SerializeField] private float _shootRange = 10f;
    private bool _attacking = false;
    private bool _canDoDamage = false;
    private bool _didDamage = false;

    public bool IsAttacking => _attacking;
    public bool CanDoDamage => _canDoDamage;
    public float Damage => _damage;
    public bool DidDamage => _didDamage;

    [Header("Projectile")] [SerializeField]
    private GameObject[] _projectiles = null;

    [SerializeField] private Transform _projectileSpawnPoint = null;
    [SerializeField] private float _timeToSpawnProjectile = 1.0f;
    [SerializeField] private float _projectileDamage = 1;
    [SerializeField] private float _projectileSpeed = 20.0f;

    private bool _isShooting = false;

    //---Wwise Events---//

    public AK.Wwise.Event Crab_Pain = new AK.Wwise.Event();
    public AK.Wwise.Event Crab_Death = new AK.Wwise.Event();

    public AK.Wwise.Event Crab_Attack = new AK.Wwise.Event();
    public uint Crab_AttackID { get { return (uint)(Crab_Attack == null ? AkSoundEngine.AK_INVALID_PLAYING_ID : Crab_Attack.Id); } }

    public AK.Wwise.Event Crab_Scream = new AK.Wwise.Event();
    public uint Crab_ScreamID { get { return (uint)(Crab_Scream == null ? AkSoundEngine.AK_INVALID_PLAYING_ID : Crab_Scream.Id); } }

    private bool _startCombatMusic = false;

    // Start is called before the first frame update
    void Start()
    {
        SetFollowPlayer(true);
        _stopWhenhit = false;

        _enemyType = EnemyTypes.CRAB;

        Crab_Pain_base = Crab_Pain;
        Crab_Death_base = Crab_Death;

        _addToAlertedEnemy = false;
    }

    // Update is called once per frame
    void Update()
    {
        
        if(_multiplierSet && !_damageMultiplierSet)
        {
            _damage *= _damageMultiplier;
            _projectileDamage *= _damageMultiplier;

            _damageMultiplierSet = true;
        }
        
        if (!_dead)
        {
            if (_rigidbody.isKinematic == false)
            {
                base.Update();
                CheckAttack();
            }
        }

        if(_characterCloseEnough && !_startCombatMusic)
        {
            _startCombatMusic = true;
            AkSoundEngine.SetState("Music_InGame", "Boss");
        }
        else if(!_characterCloseEnough && _startCombatMusic)
        {
            _startCombatMusic = false;
            AkSoundEngine.SetState("Music_InGame", "Explore");
        }
    }

    private void CheckAttack()
    {
        if (_characterCloseEnough)
        {
            if (!_attacking&&!_isShooting)
            {
                Attack();
            }
                
        }
        // else if (Vector3.Distance(GameManager.s_instance.characterRigidbody.transform.position,transform.position)<_shootRange&&!_isShooting)
        // {
        //     Shoot();
        // }
    }

    private void Attack()
    {
        transform.LookAt(GameManager.s_instance.GetCharacterController().transform.position);
        _attacking = true;
        StopMovement();
        _animator.SetTrigger("Attack_4");
        PlayAttackSound();
        StartCoroutine(WaitToDoDamage());
        StartCoroutine(WaitForAttackAnimEnd());
    }

    private void Shoot()
    {
        _rigidbody.freezeRotation = true;
        _rigidbody.isKinematic = true;
        _isShooting = true;
        StopMovement();
        _animator.SetTrigger("shoot");
        PlayScreamSound();
        gameObject.transform.LookAt(GameManager.s_instance.GetCharacterController().transform.position);
        StartCoroutine(CreateProjectile());
        StartCoroutine(WaitForShootAnimEnd());
    }

    private IEnumerator WaitToDoDamage()
    {
        yield return new WaitForSeconds(_timeBeforeAttack);
        _canDoDamage = true;
    }

    private IEnumerator WaitForAttackAnimEnd()
    {
        yield return new WaitForSeconds(_attackAnimDuration);
        _canDoDamage = false;
        _didDamage = false;
        if (!_isShooting)
        {
            Shoot();     
        }
    }
    
    private IEnumerator WaitForShootAnimEnd()
    {
        yield return new WaitForSeconds(_shootAnimDuration);
        StartCoroutine(WaitForAttack());
        _canDoDamage = true;
        ResumeMovement();
        _rigidbody.freezeRotation = false;
        _rigidbody.isKinematic = false;
    }

    private IEnumerator WaitForAttack()
    {
        yield return new WaitForSeconds(5.0f);
        _attacking = false;
        _isShooting = false;
    }

    public void DoDamage()
    {
        _didDamage = true;
    }

    private IEnumerator CreateProjectile()
    {
        yield return new WaitForSeconds(_timeToSpawnProjectile);

        for (int i = 0; i < 12; i+=4)
        {
            GameObject projectile = Instantiate(_projectiles[Random.Range(0, _projectiles.Length - 1)]);
            projectile.AddComponent<SphereCollider>();
            Rigidbody rb = projectile.AddComponent<Rigidbody>();
            rb.mass = 150f;
            //PullableObject pullableObject = projectile.AddComponent<PullableObject>();
            //pullableObject.SetObjectMaterial(AttachMaterials.STONE);

            BulletController bulletController = projectile.AddComponent<BulletController>();
            bulletController.SetBulletSpeed(_projectileSpeed);
            bulletController.SetDamage(_projectileDamage);
            
            projectile.transform.position = _projectileSpawnPoint.position;
            projectile.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
            Vector3 projectilePosition = GameManager.s_instance.GetCharacterController().transform.position;
            projectilePosition.y += 10;
            projectilePosition.x += i;
            projectile.transform.LookAt(projectilePosition);
            projectile.layer = 15;
            projectile.tag = "pullableObject";

        }
    }

    private void PlayAttackSound()
    {
        AkSoundEngine.PostEvent(Crab_AttackID, gameObject);
    }

    private void PlayScreamSound()
    {
        AkSoundEngine.PostEvent(Crab_ScreamID, gameObject);
    }
}
