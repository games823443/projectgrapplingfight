﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpingEnemy : Enemy
{
    [SerializeField] private float _damage = 1;
    [SerializeField] private float _jumpStartTime = 0.25f;
    [SerializeField] private float _jumpAnimationLength = 4.0f;
    [SerializeField] private float _attackCooldown = 3.0f;
    private bool _attacking = false;
    private bool _canAttack = true;
    private bool _didDamage = false;
    
    private float _attackMagnitude = 0.0f;
    private Vector3 _direction;

    public bool IsAttacking => _attacking;
    public float Damage => _damage;
    public bool DidDamage => _didDamage;

    //---Wwise Events---//

    public AK.Wwise.Event Arach_Pain = new AK.Wwise.Event();
    public AK.Wwise.Event Arach_Death = new AK.Wwise.Event();
    public AK.Wwise.Event Arach_Idle = new AK.Wwise.Event();

    public AK.Wwise.Event Arach_Attack = new AK.Wwise.Event();
    public uint Arach_AttackID { get { return (uint)(Arach_Attack == null ? AkSoundEngine.AK_INVALID_PLAYING_ID : Arach_Attack.Id); } }

    // Start is called before the first frame update
    void Start()
    {
        SetFollowPlayer(true);

        _enemyType = EnemyTypes.ARACH;

        Arach_Pain_base = Arach_Pain;
        Arach_Death_base = Arach_Death;
        Arach_Idle_base = Arach_Idle;
    }

    // Update is called once per frame
    protected override void Update()
    {
        if(_multiplierSet && !_damageMultiplierSet)
        {
            _damage *= _damageMultiplier;
            _damageMultiplierSet = true;
        }

        if (!_dead)
        {
            base.Update();
            CheckAttack();
        }
    }

    private void CheckAttack()
    {
        if (_canAttack && _characterCloseEnough)
        {
            if (!_attacking && Vector3.Distance(transform.position, GameManager.s_instance.GetCharacterController().transform.position) < 50.0f)
                TryAttack();
        }
    }

    private void TryAttack()
    {
        Vector3 direction = GameManager.s_instance.GetCharacterController().transform.position - transform.position;

        if (!Physics.Raycast(transform.position, direction, direction.magnitude))
        {
            _attacking = true;
            _canAttack = false;
            Attack();
        }
    }

    private void Attack()
    {
        StopMovement();
        _animator.SetTrigger("attack");

        PlayAttackSound();

        StartCoroutine(Jump());
    }

    private IEnumerator Jump()
    {
        yield return new WaitForSeconds(_jumpStartTime);
        transform.LookAt(new Vector3(GameManager.s_instance.GetCharacterController().transform.position.x, transform.position.y, GameManager.s_instance.GetCharacterController().transform.position.z));

        _direction = GameManager.s_instance.GetCharacterController().transform.position - transform.position;
        _attackMagnitude = _direction.magnitude;

        _rigidbody.AddForce((_direction.normalized + transform.up) * _direction.magnitude * _rigidbody.mass, ForceMode.Impulse);
        StartCoroutine(WaitForAttackAnimEnd());
    }

    private IEnumerator WaitForAttackAnimEnd()
    {
        yield return new WaitForSeconds(_jumpAnimationLength);
        _rigidbody.velocity = Vector3.zero;
        _attacking = false;
        _didDamage = false;
        ResumeMovement();
        StartCoroutine(AttackCooldown());
    }

    private IEnumerator AttackCooldown()
    {
        yield return new WaitForSeconds(_attackCooldown);
        _canAttack = true;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (_attacking && !_didDamage && collision.gameObject.CompareTag("Player"))
        {
            GameManager.s_instance.GetCharacterController().TakeDamage(_damage, EnemyTypes.ARACH);
            GameManager.s_instance.characterRigidbody.AddForce((_direction + transform.up) * _attackMagnitude * 20f, ForceMode.Impulse);
            _didDamage = true;
        }
    }

    private void PlayAttackSound()
    {
        AkSoundEngine.PostEvent(Arach_AttackID, gameObject);
    }
}
