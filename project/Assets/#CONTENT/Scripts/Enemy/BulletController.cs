﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour
{
    private float _lifeTimer = 0.0f;
    private float _speed = 200.0f;
    private float _damage = 1;
    
    private Vector3 forward;

    private void Start()
    {
        forward = transform.forward;
        gameObject.GetComponent<Rigidbody>().AddForce(transform.forward*_speed,ForceMode.Impulse);
    }

    private void Update()
    {
        
        _lifeTimer += Time.deltaTime;

        if (_lifeTimer > 10f)
        {
            if (GameManager.s_instance.Hook.ContactPoint != null&&GameManager.s_instance.Hook.ContactPoint.transform.parent!=null&&GameManager.s_instance.Hook.ContactPoint.transform.parent.gameObject!=null)
            {
                if (GameManager.s_instance.Hook.ContactPoint.transform.parent.gameObject == gameObject)
                {
                    GameManager.s_instance.Hook.ResetContactPoint();
                }
                
            }
            else
            {
                GameManager.s_instance.Hook.ResetContactPoint();
            }

            Destroy(gameObject);
        }
           
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag("enemy"))
        {
            if (other.CompareTag("Player"))
            {
                GameManager.s_instance.GetCharacterController().TakeDamage(_damage, EnemyTypes.GOLEM);
                if (GameManager.s_instance.Hook.ContactPoint != null&&GameManager.s_instance.Hook.ContactPoint.transform.parent!=null&&GameManager.s_instance.Hook.ContactPoint.transform.parent.gameObject!=null)
                {
                    if (GameManager.s_instance.Hook.ContactPoint.transform.parent.gameObject == gameObject)
                    {
                        GameManager.s_instance.Hook.ResetContactPoint();
                    }
                }
                else
                {
                    GameManager.s_instance.Hook.ResetContactPoint();
                }

                Destroy(gameObject);
            }
        }
    }

    public void SetBulletSpeed(float speed)
    {
        _speed = speed;
    }

    public void SetDamage(float damage)
    {
        _damage = damage;
    }
}
