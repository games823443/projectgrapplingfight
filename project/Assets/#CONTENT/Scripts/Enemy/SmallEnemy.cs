﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmallEnemy : Enemy
{
    [SerializeField] private float _attackAnimDuration = 2.0f;
    [SerializeField] private float _timeBeforeAttack = 1.0f;
    [SerializeField] private float _damage = 1;
    private bool _attacking = false;
    private bool _canDoDamage = false;
    private bool _didDamage = false;

    public bool IsAttacking => _attacking;
    public bool CanDoDamage => _canDoDamage;
    public float Damage => _damage;
    public bool DidDamage => _didDamage;

    //---Wwise Events---//

    public AK.Wwise.Event Skeleton_Pain = new AK.Wwise.Event();
    public AK.Wwise.Event Skeleton_Death = new AK.Wwise.Event();
    public AK.Wwise.Event Skeleton_Idle = new AK.Wwise.Event();

    public AK.Wwise.Event Skeleton_Attack = new AK.Wwise.Event();
    public uint Skeleton_AttackID { get { return (uint)(Skeleton_Attack == null ? AkSoundEngine.AK_INVALID_PLAYING_ID : Skeleton_Attack.Id); } }

    // Start is called before the first frame update
    void Start()
    {
        SetFollowPlayer(true);

        _enemyType = EnemyTypes.SKELETON;

        Skeleton_Pain_base = Skeleton_Pain;
        Skeleton_Death_base = Skeleton_Death;
        Skeleton_Idle_base = Skeleton_Idle;
    }

    // Update is called once per frame
    protected override void Update()
    {
        if(_multiplierSet && !_damageMultiplierSet)
        {
            _damage *= _damageMultiplier;
            _damageMultiplierSet = true;
        }

        if(!_dead)
        {
            if (_rigidbody.isKinematic == false)
            {
                base.Update();
                CheckAttack();
            }
        }
    }

    private void CheckAttack()
    {
        if(_characterCloseEnough)
        {
            if (!_attacking && Vector3.Distance(transform.position, GameManager.s_instance.GetCharacterController().transform.position) < 10.0f)
                Attack();
        }
    }

    private void Attack()
    {
        _attacking = true;
        StopMovement();

        _animator.SetTrigger("attack");
        PlayAttackSound();

        StartCoroutine(WaitToDoDamage());
        StartCoroutine(WaitForAttackAnimEnd());
    }

    private IEnumerator WaitToDoDamage()
    {
        yield return new WaitForSeconds(_timeBeforeAttack);
        _canDoDamage = true;
    }

    private IEnumerator WaitForAttackAnimEnd()
    {
        yield return new WaitForSeconds(_attackAnimDuration);
        _attacking = false;
        _canDoDamage = false;
        _didDamage = false;
        ResumeMovement();
    }

    public void DoDamage()
    {
        _didDamage = true;
    }

    private void PlayAttackSound()
    {
        AkSoundEngine.PostEvent(Skeleton_AttackID, gameObject);
    }
}
