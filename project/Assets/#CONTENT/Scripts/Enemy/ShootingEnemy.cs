﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootingEnemy : Enemy
{
    [Header("Shooting")]
    [SerializeField] private float _shootingRate = 1.0f;
    [SerializeField] private float _shootingAnimationDuration = 5.0f;

    [Header("Projectile")]
    [SerializeField] private GameObject[] _projectiles = null;
    [SerializeField] private Transform _projectileSpawnPointParent  = null;
    [SerializeField] private Transform[] _projectileSpawnPoints  = null;
    [SerializeField] private float _timeToSpawnProjectile = 1.0f;
    [SerializeField] private float _projectileDamage = 1;
    [SerializeField] private float _projectileSpeed = 20.0f;

    [SerializeField] private bool _isBoss = false;

    private float _shootTimer = 0.0f;
    private bool _isShooting = false;
    private bool _startCombatMusic = false;

    //---Wwise Events---//

    public AK.Wwise.Event Golem_Pain = new AK.Wwise.Event();
    public AK.Wwise.Event Golem_Death = new AK.Wwise.Event();
    public AK.Wwise.Event Golem_Idle = new AK.Wwise.Event();

    public AK.Wwise.Event Golem_Attack = new AK.Wwise.Event();
    public uint Golem_AttackID { get { return (uint)(Golem_Attack == null ? AkSoundEngine.AK_INVALID_PLAYING_ID : Golem_Attack.Id); } }

    public AK.Wwise.Event Golem_Scream = new AK.Wwise.Event();
    public uint Golem_ScreamID { get { return (uint)(Golem_Scream == null ? AkSoundEngine.AK_INVALID_PLAYING_ID : Golem_Scream.Id); } }

    private void Start()
    {
        _enemyType = EnemyTypes.GOLEM;

        Golem_Pain_base = Golem_Pain;
        Golem_Death_base = Golem_Death;
        Golem_Idle_base = Golem_Idle;

        if (_isBoss)
            _addToAlertedEnemy = false;
    }

    protected override void Update()
    {
        if(_multiplierSet && !_damageMultiplierSet)
        {
            _projectileDamage *= _damageMultiplier;
            _damageMultiplierSet = true;
        }

        if (!_dead)
        {
            if (_rigidbody.isKinematic == false)
            {
                base.Update();
                Shoot();
            }
        }

        if (_isBoss)
        {
            if (_characterCloseEnough && !_startCombatMusic)
            {
                _startCombatMusic = true;
                //AkSoundEngine.SetState("Music_InGame", "Boss");
                GameManager.s_instance.PlayBossStart();
            }
            else if (!_characterCloseEnough && _startCombatMusic)
            {
                _startCombatMusic = false;
                //AkSoundEngine.SetState("Music_InGame", "Explore");
            }
        }
    }

    private void Shoot()
    {
        if (_characterCloseEnough && !_isShooting)
        {
            _shootTimer += Time.deltaTime;

            if (_shootTimer > _shootingRate)
            {
                _shootTimer = 0.0f;
                transform.LookAt(new Vector3(GameManager.s_instance.GetCharacterController().transform.position.x, transform.position.y, GameManager.s_instance.GetCharacterController().transform.position.z));
                _animator.SetTrigger("attack");
                _isShooting = true;

                StopMovement();
                PlayScreamSound();
                StartCoroutine(CreateProjectile());
                StartCoroutine(WaitForAttackAnimation());
            }
        }
        else if(_isShooting)
        {
            transform.LookAt(new Vector3(GameManager.s_instance.GetCharacterController().transform.position.x, transform.position.y, GameManager.s_instance.GetCharacterController().transform.position.z));
            _rigidbody.velocity = Vector3.zero;
        }    
    }

    private IEnumerator WaitForAttackAnimation()
    {
        yield return new WaitForSeconds(_shootingAnimationDuration);

        ResumeMovement();
    }

    private IEnumerator CreateProjectile()
    {
        yield return new WaitForSeconds(_timeToSpawnProjectile);

        PlayAttackSound();

        _isShooting = false;

        _projectileSpawnPointParent.LookAt(GameManager.s_instance.GetCharacterController().transform.position);

        foreach (var spawnPoint in _projectileSpawnPoints)
        {
            GameObject projectile = Instantiate(_projectiles[Random.Range(0, _projectiles.Length - 1)]);
            projectile.AddComponent<SphereCollider>();
            Rigidbody rb = projectile.AddComponent<Rigidbody>();
            rb.mass = 150f;
            //PullableObject pullableObject = projectile.AddComponent<PullableObject>();
            //pullableObject.SetObjectMaterial(AttachMaterials.STONE);

            BulletController bulletController = projectile.AddComponent<BulletController>();
            bulletController.SetBulletSpeed(_projectileSpeed);
            bulletController.SetDamage(_projectileDamage);

            projectile.transform.position = spawnPoint.position;
            projectile.transform.rotation = spawnPoint.rotation;
            projectile.transform.localScale = new Vector3(3.0f, 3.0f, 3.0f);
            //Vector3 projectilePosition = GameManager.s_instance.GetCharacterController().transform.position;
            //projectile.transform.LookAt(projectilePosition);
            projectile.layer = 15;
            projectile.tag = "pullableObject";
        }
    }

    private void PlayAttackSound()
    {
        AkSoundEngine.PostEvent(Golem_AttackID, gameObject);
    }

    private void PlayScreamSound()
    {
        AkSoundEngine.PostEvent(Golem_ScreamID, gameObject);
    }
}
