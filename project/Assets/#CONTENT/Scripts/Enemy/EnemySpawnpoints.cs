﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawnpoints : MonoBehaviour
{
    [SerializeField] private Transform[] _spawnPoints = null;

    private bool _init = false;

    // Start is called before the first frame update
    void Start()
    {
        //SpawnRandomEnemies();
    }

    private void Update()
    {
        if(!_init)
        {
            SpawnRandomEnemies();
            _init = true;
        }
    }

    private void SpawnRandomEnemies()
    {
        foreach(var spawnPoint in _spawnPoints)
        {
            GameObject enemy = Instantiate(GameManager.s_instance.GetRandomEnemy(), transform);

            enemy.transform.position = spawnPoint.position;
        }
    }
}
