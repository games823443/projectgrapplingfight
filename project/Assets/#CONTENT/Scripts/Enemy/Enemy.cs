﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;

[RequireComponent(typeof(NavMeshAgent))]
[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(AudioSource))]
[RequireComponent(typeof(Rigidbody))]


public class Enemy : MonoBehaviour
{
    [SerializeField] private bool _kill = false;
    protected NavMeshAgent _navMeshAgent;
    [Header("Health")] [SerializeField] private float _health = 50.0f;
    [SerializeField] private GameObject _healthBar = null;
    [SerializeField] private ProgressBar _progressBarComp = null;
    [SerializeField] private float _headHight = 1.0f;
    private bool _lockDamage = false;
    private float _initialHealth = 100.0f;

    protected bool _stopWhenhit = true;
    protected Animator _animator;

    [Header("Movement")]
    [SerializeField] private float _movementSpeed = 20.0f;
    [SerializeField] private float _distanceToUpdateTarget = 1.0f;
    [SerializeField] private float _animationSpeed = 0.025f;


    [SerializeField] private Material _hitMaterial = null;
    [SerializeField] private Material[] _hitMaterials = null;
    private Material _mat;
    private Material[] _mats;

    [SerializeField] private float _deathClipLength = 2.0f;

    [SerializeField] private protected float _attackDistance = 120f;

    private Renderer _renderer;

    [Header("Debug")]
    [SerializeField] private bool _showTarget = false;

    private Vector3 _movementTarget;

    protected bool _dead = false;
    public bool Dead => _dead;

    private float _attachedWeight = 0.0f;

    private System.Random _rnd = new System.Random();

    [Header("Sounds")] [SerializeField] private AudioClip[] _hitSounds = null;
    private AudioSource _audioSource = null;



    private bool _followPlayer = false;
    private bool _hookAttached = false;

    private int _layer = -1;

    protected Rigidbody _rigidbody;
    private int _pathIdx = 0;

    private Vector3 _itemPos;

    protected float _damageMultiplier = 1.0f;

    protected bool _multiplierSet = false;
    protected bool _damageMultiplierSet = false;

    private Outline _outline;

    private bool _initOutline = true;

    protected EnemyTypes _enemyType = EnemyTypes.DEFAULT;
    public EnemyTypes EnemyType => _enemyType;

    protected bool _characterCloseEnough = false;
    protected bool _addToAlertedEnemy = true;

    //---Wwise Events---//

    //only base events, events are then set in the child classes

    //Pain sounds
    protected AK.Wwise.Event Arach_Pain_base = new AK.Wwise.Event();
    public uint Arach_PainID { get { return (uint)(Arach_Pain_base == null ? AkSoundEngine.AK_INVALID_PLAYING_ID : Arach_Pain_base.Id); } }

    protected AK.Wwise.Event Golem_Pain_base = new AK.Wwise.Event();
    public uint Golem_PainID { get { return (uint)(Golem_Pain_base == null ? AkSoundEngine.AK_INVALID_PLAYING_ID : Golem_Pain_base.Id); } }

    protected AK.Wwise.Event Crab_Pain_base = new AK.Wwise.Event();
    public uint Crab_PainID { get { return (uint)(Crab_Pain_base == null ? AkSoundEngine.AK_INVALID_PLAYING_ID : Crab_Pain_base.Id); } }

    protected AK.Wwise.Event Skeleton_Pain_base = new AK.Wwise.Event();
    public uint Skeleton_PainID { get { return (uint)(Skeleton_Pain_base == null ? AkSoundEngine.AK_INVALID_PLAYING_ID : Skeleton_Pain_base.Id); } }

    protected AK.Wwise.Event UFO_Pain_base = new AK.Wwise.Event();
    public uint UFO_PainID { get { return (uint)(UFO_Pain_base == null ? AkSoundEngine.AK_INVALID_PLAYING_ID : UFO_Pain_base.Id); } }

    protected AK.Wwise.Event Sliding_Pain_base = new AK.Wwise.Event();
    public uint Sliding_PainID { get { return (uint)(Sliding_Pain_base == null ? AkSoundEngine.AK_INVALID_PLAYING_ID : Sliding_Pain_base.Id); } }

    //Death sounds
    protected AK.Wwise.Event Arach_Death_base = new AK.Wwise.Event();
    public uint Arach_DeathID { get { return (uint)(Arach_Death_base == null ? AkSoundEngine.AK_INVALID_PLAYING_ID : Arach_Death_base.Id); } }

    protected AK.Wwise.Event Golem_Death_base = new AK.Wwise.Event();
    public uint Golem_DeathID { get { return (uint)(Golem_Death_base == null ? AkSoundEngine.AK_INVALID_PLAYING_ID : Golem_Death_base.Id); } }

    protected AK.Wwise.Event Crab_Death_base = new AK.Wwise.Event();
    public uint Crab_DeathID { get { return (uint)(Crab_Death_base == null ? AkSoundEngine.AK_INVALID_PLAYING_ID : Crab_Death_base.Id); } }

    protected AK.Wwise.Event Skeleton_Death_base = new AK.Wwise.Event();
    public uint Skeleton_DeathID { get { return (uint)(Skeleton_Death_base == null ? AkSoundEngine.AK_INVALID_PLAYING_ID : Skeleton_Death_base.Id); } }

    protected AK.Wwise.Event UFO_Death_base = new AK.Wwise.Event();
    public uint UFO_DeathID { get { return (uint)(UFO_Death_base == null ? AkSoundEngine.AK_INVALID_PLAYING_ID : UFO_Death_base.Id); } }

    protected AK.Wwise.Event Sliding_Death_base = new AK.Wwise.Event();
    public uint Sliding_DeathID { get { return (uint)(Sliding_Death_base == null ? AkSoundEngine.AK_INVALID_PLAYING_ID : Sliding_Death_base.Id); } }

    //Idle sounds
    protected AK.Wwise.Event Arach_Idle_base = new AK.Wwise.Event();
    public uint Arach_IdleID { get { return (uint)(Arach_Idle_base == null ? AkSoundEngine.AK_INVALID_PLAYING_ID : Arach_Idle_base.Id); } }

    protected AK.Wwise.Event Golem_Idle_base = new AK.Wwise.Event();
    public uint Golem_IdleID { get { return (uint)(Golem_Idle_base == null ? AkSoundEngine.AK_INVALID_PLAYING_ID : Golem_Idle_base.Id); } }

    protected AK.Wwise.Event Crab_Idle_base = new AK.Wwise.Event();
    public uint Crab_IdleID { get { return (uint)(Crab_Idle_base == null ? AkSoundEngine.AK_INVALID_PLAYING_ID : Crab_Idle_base.Id); } }

    protected AK.Wwise.Event Skeleton_Idle_base = new AK.Wwise.Event();
    public uint Skeleton_IdleID { get { return (uint)(Skeleton_Idle_base == null ? AkSoundEngine.AK_INVALID_PLAYING_ID : Skeleton_Idle_base.Id); } }

    protected AK.Wwise.Event UFO_Idle_base = new AK.Wwise.Event();
    public uint UFO_IdleID { get { return (uint)(UFO_Idle_base == null ? AkSoundEngine.AK_INVALID_PLAYING_ID : UFO_Idle_base.Id); } }

    protected AK.Wwise.Event Sliding_Idle_base = new AK.Wwise.Event();
    public uint Sliding_IdleID { get { return (uint)(Sliding_Idle_base == null ? AkSoundEngine.AK_INVALID_PLAYING_ID : Sliding_Idle_base.Id); } }

    // Start is called before the first frame update
    void Awake()
    {
        //gameObject.SetActive(false);
        _navMeshAgent = GetComponent<NavMeshAgent>();
        //_navMeshAgent.speed = _movementSpeed;
        _navMeshAgent.speed = 0f;

        if (MapGenerator.s_instance != null)
            _navMeshAgent.enabled = false;

        _animator = GetComponent<Animator>();

        _renderer = GetComponentInChildren<SkinnedMeshRenderer>();
        if (_renderer == null) {
            //lets try a normal MeshRenderer
            _renderer = GetComponentInChildren<MeshRenderer>();
        }
        _mat = _renderer.material;
        _mats = _renderer.materials;

        _movementTarget = transform.position;

        _audioSource = GetComponent<AudioSource>();
        _audioSource.spatialBlend = 0.5f;
        _audioSource.volume = 0.5f;

        _rigidbody = GetComponent<Rigidbody>();
        _rigidbody.isKinematic = false;

        _headHight *= transform.localScale.x;

        _outline = gameObject.AddComponent<Outline>();
        _outline.enabled = false;
    }

    protected virtual void Start() 
    {
        PlayIdleSound();
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        if(_initOutline)
        {
            _outline.OutlineColor = GameManager.s_instance.EnemyOutlineColor;
            _outline.OutlineWidth = GameManager.s_instance.EnemyOutlineWidth;
            _outline.OutlineMode = Outline.Mode.OutlineAll;
            

            _initOutline = false;
        }

        if (GameManager.s_instance.Sleep(transform.position))
        {
            _rigidbody.Sleep();
            return;
        }

        if (_kill)
            Die();

        if (!_multiplierSet && MapGenerator.s_instance.MapGenerationDone)
        {
            SetLayerMultiplier();
            _multiplierSet = true;
        }

        if (_dead)
        {
            StopMovement();
            return;
        }

        if (_rigidbody.isKinematic == false)
        {
            if (!IsStationary()) 
            {
                if (!_navMeshAgent.enabled && MapGenerator.s_instance.MapGenerationDone) 
                {
                    _navMeshAgent.enabled = true;
                    UpdateMovementTarget();
                }
                else 
                {
                    Walk();
                    UpdateAnimator();
                    CheckHealthBar();
                }
            }
            else 
            {
                CheckHealthBar(); 
            }
        }

        CheckCharacterCloseEnough();
    }

    private protected virtual bool IsStationary() {
        return false;
    }

    public void TakeDamage(float damage = 10.0f, float yPosition = 0f)
    {
        if (_lockDamage)
            return;

        PlayHitSound();

        float y = yPosition - transform.position.y;

        float multiplier = 1.0f;

        if (y >= _headHight)
        {
            multiplier = 2.0f;

            GameObject bloodEffect = GameObject.Instantiate(GameManager.s_instance.BloodSplatterEffect,
                transform.position + new Vector3(0, y, 0), Quaternion.identity);
            bloodEffect.transform.localScale *= 2.0f;
            bloodEffect.transform.LookAt(transform.position + new Vector3(0, y, 0) + transform.up);
        }

        _health -= damage * multiplier;

        if (_hitMaterials != null && _hitMaterials.Length>0) {
            _renderer.materials = _hitMaterials;
        }
        else {
            _renderer.material = _hitMaterial;
        }

        UpdateHealthBar();

        if (_hitSounds.Length > 0) {
            _audioSource.PlayOneShot(_hitSounds[_rnd.Next(0, _hitSounds.Length)]);
        }

        if (_health <= 0.0f)
        {
            //TargetController.nearEnemies.Remove(this);
            Die();
        }
        else
        {
            _animator.SetTrigger("getHit");
            if (_stopWhenhit)
            {
                StopMovement();
            }

            _lockDamage = true;

            StartCoroutine(LockDamage());
        }
    }

    private IEnumerator LockDamage()
    {
        yield return new WaitForSeconds(1.0f);
        _lockDamage = false;
        _renderer.material = _mat;
        _renderer.materials = _mats;

        ResumeMovement();
    }

    private void Walk()
    {
        UpdateMovementTarget();
    }

    protected void UpdateMovementTarget()
    {
        if (_followPlayer && _characterCloseEnough)
        {
            _movementTarget = GameManager.s_instance.GetCharacterController().transform.position;
        }
        else
        {
            if (Vector3.Distance(transform.position, _movementTarget) < _distanceToUpdateTarget ||
                _movementTarget == transform.position || _movementTarget == Vector3.zero)
            {
                _movementTarget = RandomNavSphere(transform.position, 50.0f, -1);

                if (_showTarget)
                {
                    Debug.DrawLine(_movementTarget, _movementTarget + Vector3.up * 100f, Color.red, 2.0f);
                }

                _pathIdx = 0;
            }
        }

        _navMeshAgent.SetDestination(_movementTarget);

        _movementTarget.y = transform.position.y;

        transform.LookAt(_movementTarget);

        if (_navMeshAgent.path.corners.Length > 0)
        {
            if (_pathIdx <= _navMeshAgent.path.corners.Length - 1)
            {
                _rigidbody.AddForce((_navMeshAgent.path.corners[_pathIdx] - transform.position).normalized *
                                    Time.deltaTime * 100f * _rigidbody.mass * _movementSpeed);

                if (Vector3.Distance(transform.position, _navMeshAgent.path.corners[_pathIdx]) < 0.5f)
                {
                    _pathIdx++;

                    if (_pathIdx > _navMeshAgent.path.corners.Length - 1)
                        _pathIdx = _navMeshAgent.path.corners.Length - 1;
                }
            }
            else
                _pathIdx = _navMeshAgent.path.corners.Length - 1;
        }
    }

    protected void SetNewMovementTarget()
    {
        _movementTarget = RandomNavSphere(transform.position, 50.0f, -1);
        _pathIdx = 0;
    }


    public Vector3 RandomNavSphere(Vector3 origin, float dist, int layermask)
    {
        Vector3 randDirection = Random.insideUnitSphere * dist;

        randDirection += origin;

        NavMeshHit navHit;

        NavMesh.SamplePosition(randDirection, out navHit, dist, layermask);

        return navHit.position;
    }

    private void UpdateAnimator()
    {
        _animator.SetFloat("walkAnimSpeed", _rigidbody.velocity.magnitude * _animationSpeed);
    }

    protected bool IsCharacterCloseEnough()
    {
        if (Vector3.Distance(transform.position, GameManager.s_instance.GetCharacterController().transform.position) <
            _attackDistance)
            return true;
        else
            return false;
    }

    private void Die()
    {
        if (_dead)
            return;

        if (_characterCloseEnough && _addToAlertedEnemy)
            GameManager.s_instance.RemoveAlertedEnemy();

        PlayDeathSound();

        _animator.SetTrigger("die");
        _rigidbody.isKinematic = true;

        if (_hookAttached)
            GameManager.s_instance.Hook.ResetContactPoint();

        _dead = true;
        _healthBar.SetActive(false);
        StopMovement();
        _navMeshAgent.enabled = false;

        _itemPos = transform.position;

        ContactPoint[] contactPoints = GetComponentsInChildren<ContactPoint>();
        foreach (var cp in contactPoints)
            Destroy(cp.gameObject);

        StartCoroutine(WaitForDieAnimation());
        UIManager.s_instance.AddKillToCounter();
    }

    private IEnumerator WaitForDieAnimation()
    {
        yield return new WaitForSeconds(_deathClipLength);
        Instantiate(GameManager.s_instance.GetRandomPickUp(), _itemPos, Quaternion.identity);

        if (_layer != -1)
        {
            MapGenerator.s_instance.SpawnLayerTransition(_layer);
        }

        Destroy(gameObject);
    }

    protected void StopMovement()
    {
        if (!IsStationary()) {
            _navMeshAgent.isStopped = true;
        }
        _navMeshAgent.velocity = Vector3.zero;
        _rigidbody.velocity = Vector3.zero;
    }

    protected virtual void ResumeMovement()
    {
        if (_dead)
            return;

        if (!IsStationary()) {
            _navMeshAgent.isStopped = false;
        }
    }



    private void CheckHealthBar()
    {
        if (_characterCloseEnough && !_healthBar.activeSelf && !_dead)
        {
            _healthBar.SetActive(true);
            UpdateHealthBar();
        }
        else if (!_characterCloseEnough && _healthBar.activeSelf && !_dead)
            _healthBar.SetActive(false);
    }

    private void UpdateHealthBar()
    {
        _healthBar.SetActive(true);

        float percentValue = _health / _initialHealth * 100.0f;

        _progressBarComp.BarValue = percentValue;
    }

    public void AddObjectWeight(float weight)
    {
        _attachedWeight += weight;
        _navMeshAgent.speed = _movementSpeed - _attachedWeight * 0.0001f;
    }

    public void RemoveObjectWeight(float weight)
    {
        _attachedWeight -= weight;
        _navMeshAgent.speed = _movementSpeed - _attachedWeight * 0.0001f;
    }

    public void SetFollowPlayer(bool follow)
    {
        _followPlayer = follow;
    }

    public void AttachHook(bool attach)
    {
        _hookAttached = attach;
    }

    public void SetLayer(int layer)
    {
        _layer = layer;
    }

    public void GetStunned(float time)
    {
        StartCoroutine(Freeze(time));
    }

    private IEnumerator Freeze(float time)
    {
        _rigidbody.isKinematic = true;
        _animator.SetFloat("walkAnimSpeed", 0);
        yield return new WaitForSeconds(time);
        _rigidbody.isKinematic = false;
    }

    private void SetLayerMultiplier()
    {
        float[] boundaries = MapGenerator.s_instance.LayerBoundaries;
        for (int i = 0; i < boundaries.Length; i++)
        {
            if (i < boundaries.Length - 1)
            {
                if (transform.position.y >= boundaries[i] && transform.position.y <= boundaries[i + 1])
                {
                    float multiplier = 1f + i * 0.5f;

                    _damageMultiplier = multiplier;
                    _health *= multiplier;

                    break;
                }
            }
            else
            {
                float multiplier = 1f + i * 0.5f;
                _damageMultiplier = multiplier;
                _health *= multiplier;
            }
        }

        _initialHealth = _health;
        _healthBar.SetActive(false);
    }

    private void PlayHitSound()
    {
        switch (_enemyType)
        {
            case EnemyTypes.GOLEM:
                AkSoundEngine.PostEvent(Golem_PainID, gameObject);
                break;
            case EnemyTypes.ARACH:
                AkSoundEngine.PostEvent(Arach_PainID, gameObject);
                break;
            case EnemyTypes.CRAB:
                AkSoundEngine.PostEvent(Crab_PainID, gameObject);
                break;
            case EnemyTypes.SKELETON:
                AkSoundEngine.PostEvent(Skeleton_PainID, gameObject);
                break;
            case EnemyTypes.UFO:
                AkSoundEngine.PostEvent(UFO_PainID, gameObject);
                break;
            case EnemyTypes.SLIDING:
                AkSoundEngine.PostEvent(Sliding_PainID, gameObject);
                break;
            case EnemyTypes.DEFAULT:
                AkSoundEngine.PostEvent(Golem_PainID, gameObject);
                break;
        }
    }

    private void PlayDeathSound()
    {
        switch (_enemyType)
        {
            case EnemyTypes.GOLEM:
                AkSoundEngine.PostEvent(Golem_DeathID, gameObject);
                break;
            case EnemyTypes.ARACH:
                AkSoundEngine.PostEvent(Arach_DeathID, gameObject);
                break;
            case EnemyTypes.CRAB:
                AkSoundEngine.PostEvent(Crab_DeathID, gameObject);
                break;
            case EnemyTypes.SKELETON:
                AkSoundEngine.PostEvent(Skeleton_DeathID, gameObject);
                break;
            case EnemyTypes.UFO:
                AkSoundEngine.PostEvent(UFO_DeathID, gameObject);
                break;
            case EnemyTypes.SLIDING:
                AkSoundEngine.PostEvent(Sliding_DeathID, gameObject);
                break;
            case EnemyTypes.DEFAULT:
                AkSoundEngine.PostEvent(Golem_DeathID, gameObject);
                break;
        }
    }

    private void PlayIdleSound()
    {
        switch (_enemyType)
        {
            case EnemyTypes.GOLEM:
                AkSoundEngine.PostEvent(Golem_IdleID, gameObject);
                break;
            case EnemyTypes.ARACH:
                AkSoundEngine.PostEvent(Arach_IdleID, gameObject);
                break;
            case EnemyTypes.CRAB:
                //AkSoundEngine.PostEvent(Crab_IdleID, gameObject);
                break;
            case EnemyTypes.SKELETON:
                AkSoundEngine.PostEvent(Skeleton_IdleID, gameObject);
                break;
            case EnemyTypes.UFO:
                AkSoundEngine.PostEvent(UFO_IdleID, gameObject);
                break;
            case EnemyTypes.SLIDING:
                AkSoundEngine.PostEvent(Sliding_IdleID, gameObject);
                break;
            case EnemyTypes.DEFAULT:
                AkSoundEngine.PostEvent(Golem_IdleID, gameObject);
                break;
        }
    }

    protected void CheckCharacterCloseEnough()
    {
        if (!_characterCloseEnough)
        {
            if (IsCharacterCloseEnough())
            {
                _characterCloseEnough = true;

                if(_addToAlertedEnemy)
                    GameManager.s_instance.AddAlertedEnemy();
            }
        }
        else if (_characterCloseEnough)
        {
            if (!IsCharacterCloseEnough())
            {
                _characterCloseEnough = false;

                if(_addToAlertedEnemy)
                    GameManager.s_instance.RemoveAlertedEnemy();
            }
        }
    }
}

