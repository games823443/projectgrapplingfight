﻿using System.Collections;
using System.Collections.Generic;
using Rewired;
using UnityEngine;

public class LeftHandController : MonoBehaviour
{
    
    private Player rewiredPlayer;

    [SerializeField] private GrapplingHookController _hookController;
    
    [Header("Spawnables")]
    [SerializeField] private GameObject[] _spawnables = null;
    // Start is called before the first frame update
    
    private void Awake()
    {
        rewiredPlayer = ReInput.players.GetPlayer(0);
    }

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        CheckInputs();
    }

    void CheckInputs()
    {
        if (rewiredPlayer.GetButtonDown("SpawnObject"))
        {
            SpawnObject();
            Debug.Log("Spawn");
        }
    }

    void SpawnObject()
    {

        int randomIdx = Random.Range(0, _spawnables.Length);

       GameObject spawnObject = Instantiate(_spawnables[randomIdx], transform.position, transform.rotation);
       PullableObject pullableObjectComponent = spawnObject.AddComponent<PullableObject>();
       spawnObject.GetComponent<Rigidbody>().mass = 75;
    }
}
