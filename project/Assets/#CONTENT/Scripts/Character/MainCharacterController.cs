﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Rewired;

public class MainCharacterController : MonoBehaviour
{
    [SerializeField] public float _speed = 0.25f;
    [SerializeField] private float _jumpHeight = 100f;
    [SerializeField] private float _wallJumpHeight = 15.0f;
    [SerializeField] public float _maxSlowmotionTime = 5.0f;
    [SerializeField] private Material _damageMat = null;
    [SerializeField] private WallTrigger _wallTrigger = null;
    [SerializeField] private Slider _slowmotionSlider = null;
    [SerializeField] private Image _damageScreen = null;
    [SerializeField] private float _maxHealth = 20.0f;
    
    [Header("Velocity Constraints")]
    [SerializeField] private float _horizontalVelMax = 25.0f;
    [SerializeField] private float _minY = -10.0f;
    [SerializeField] private float _maxY = 10.0f;

    private Rigidbody _rigidbody;

    private int _jumpCounter = 0;

    [Header("Health")]
    [SerializeField] private float _health = 5.0f;
    [SerializeField] private GameObject _healthBar = null;
    [SerializeField] private ProgressBar _progressBarComp = null;
    [SerializeField] private GameObject _hpText = null;
    [SerializeField] private AudioClip[] _hitSounds = null;
    [SerializeField] private AudioClip[] _painSounds = null;
    [SerializeField] private AudioSource _hitAudioSource = null;
    [SerializeField] private AudioSource _painAudioSource = null;
    private float _initialHealth = 5.0f;
    private float _damageScreenAlpha = 0.0f;
    private bool _dead = false;


    [Header("Combo")] 
    [SerializeField] private GameObject _comboBar = null;
    [SerializeField] private ProgressBar _progressBarComboComp = null;

    [Header("PhysicMaterials")]
    [SerializeField] private PhysicMaterial _regularMaterial = null;
    [SerializeField] private PhysicMaterial _noFrictionMat = null;
    
    private MeshRenderer _renderer;
    private Material _mat;

    private Vector3 _movement = Vector3.zero;

    private float _kickCooldown = 0.0f;

    private float slowmotionTime = 0.0f;

    private bool slowmotionCooldown = false;
    private bool _slowmotionPressed = false;
    private bool _slowmotionHook = false;

    private MovementStates _movementState = MovementStates.ONGROUND;

    public MovementStates movementState => _movementState;
    private bool _wallJump = false;
    private bool _blockDamage = false;

    private Player rewiredPlayer;

    private Color _damageColor;
    private bool _damageScreenFadeOut = false;
    
    private bool _comboMoveUp=false;

    private float _comboStrength = 1.0f;
    private float _comboStunTime = 5.0f;
    private float _comboRechargeTime = 20.0f;
    private float _comboCurrentRechargTime = 0.0f;

    private bool isAttachedToConeWall = false;
    
    private System.Random _rnd;

    private CapsuleCollider _collider;

    //---Wwise Events---//
    [Header("Wwise Events")]

    [Header("general sounds")]
    //general sounds
    public AK.Wwise.Event Player_Jump = new AK.Wwise.Event();
    public uint Player_JumpID { get { return (uint)(Player_Jump == null ? AkSoundEngine.AK_INVALID_PLAYING_ID : Player_Jump.Id); } }

    public AK.Wwise.Event Player_Land = new AK.Wwise.Event();
    public uint Player_LandID { get { return (uint)(Player_Land == null ? AkSoundEngine.AK_INVALID_PLAYING_ID : Player_Land.Id); } }

    public AK.Wwise.Event Player_Foot = new AK.Wwise.Event();
    public uint Player_FootID { get { return (uint)(Player_Foot == null ? AkSoundEngine.AK_INVALID_PLAYING_ID : Player_Foot.Id); } }
    [SerializeField] private float _stepInterval = 0.5f;
    private float _currentStep = 0f;

    public AK.Wwise.Event Player_Death = new AK.Wwise.Event();
    public uint Player_DeathID { get { return (uint)(Player_Death == null ? AkSoundEngine.AK_INVALID_PLAYING_ID : Player_Death.Id); } }

    public AK.Wwise.Event Player_Fall = new AK.Wwise.Event();
    public uint Player_FallID { get { return (uint)(Player_Fall == null ? AkSoundEngine.AK_INVALID_PLAYING_ID : Player_Fall.Id); } }

    public AK.Wwise.Event InAir = new AK.Wwise.Event();
    public uint InAirID { get { return (uint)(InAir == null ? AkSoundEngine.AK_INVALID_PLAYING_ID : InAir.Id); } }

    public AK.Wwise.Event OnGround = new AK.Wwise.Event();
    public uint OnGroundID { get { return (uint)(OnGround == null ? AkSoundEngine.AK_INVALID_PLAYING_ID : OnGround.Id); } }

    [Header("enemy damage")]
    //enemy damage
    public AK.Wwise.Event Player_Pain_Arach = new AK.Wwise.Event();
    public uint Player_Pain_ArachID { get { return (uint)(Player_Pain_Arach == null ? AkSoundEngine.AK_INVALID_PLAYING_ID : Player_Pain_Arach.Id); } }

    public AK.Wwise.Event Player_Pain_Golem = new AK.Wwise.Event();
    public uint Player_Pain_GolemID { get { return (uint)(Player_Pain_Golem == null ? AkSoundEngine.AK_INVALID_PLAYING_ID : Player_Pain_Golem.Id); } }

    public AK.Wwise.Event Player_Pain_Crab = new AK.Wwise.Event();
    public uint Player_Pain_CrabID { get { return (uint)(Player_Pain_Crab == null ? AkSoundEngine.AK_INVALID_PLAYING_ID : Player_Pain_Crab.Id); } }

    public AK.Wwise.Event Player_Pain_Skeleton = new AK.Wwise.Event();
    public uint Player_Pain_SkeletonID { get { return (uint)(Player_Pain_Skeleton == null ? AkSoundEngine.AK_INVALID_PLAYING_ID : Player_Pain_Skeleton.Id); } }

    public AK.Wwise.Event Player_Pain_Default = new AK.Wwise.Event();
    public uint Player_Pain_DefaultID { get { return (uint)(Player_Pain_Default == null ? AkSoundEngine.AK_INVALID_PLAYING_ID : Player_Pain_Default.Id); } }

    [Header("RTPCs")]
    //RTPCs
    [SerializeField]
    private AK.Wwise.RTPC _healthRTPC = null;
    //RTPCs
    [SerializeField]
    private AK.Wwise.RTPC _slowMo = null;

    private void Awake()
    {
        rewiredPlayer = ReInput.players.GetPlayer(0);
        _initialHealth = _health;

        AkSoundEngine.SetState("Music_InGame", "Explore");
    }

    private void Start()
    {
        _rigidbody = GetComponent<Rigidbody>();
        _renderer = GetComponent<MeshRenderer>();
        _collider = GetComponent<CapsuleCollider>();

        _mat = _renderer.material;
        _damageColor = _damageScreen.color;
        _damageColor.a = 0.0f;
        _damageScreen.color = _damageColor;

        _rnd = new System.Random();

        UpdateHealthBar(false);
        SetStatePlayerLife(false);
        
        //Move Player Start Position
        
       transform.position += new Vector3(-10, 0, -40);
       
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.s_instance.Paused)
        {
            if (!_rigidbody.isKinematic)
                _rigidbody.isKinematic = true;

            return;
        }
        else if (_rigidbody.isKinematic)
            _rigidbody.isKinematic = false;
        
        CheckInputs();
        CheckHasSlowMo();
        UpdateCooldowns();
        Move();
        ProcessStepCycle();

        UpdatePhysicMaterial();

        if (_damageScreenFadeOut)
            FadeOutDamageScreen();

        if (transform.position.y < GameManager.s_instance.WaterHight)
            Die(true);
        
    }
    
    private void CheckInputs()
    {
        if (_slowmotionSlider != null)
        {
            _slowmotionSlider.value = slowmotionTime / _maxSlowmotionTime;

            if (rewiredPlayer.GetButton("Slowmotion") && slowmotionTime < _maxSlowmotionTime && !slowmotionCooldown)
            {
                slowmotionTime += Time.deltaTime;
                if (slowmotionTime > _maxSlowmotionTime)
                {
                    slowmotionTime = _maxSlowmotionTime;
                }
                Time.timeScale = 0.5f;

                _slowmotionPressed = true;
            }
            else
            {
                _slowmotionPressed = false;

                Time.timeScale = 1.0f;
                if (slowmotionTime > 0.0f)
                {
                    slowmotionCooldown = true;
                    slowmotionTime -= Time.deltaTime;
                    var colors = _slowmotionSlider.colors;
                    colors.disabledColor = Color.red;
                    _slowmotionSlider.colors = colors;
                }
                if (slowmotionTime <= 0.0f)
                {
                    slowmotionTime = 0.0f;
                    slowmotionCooldown = false;
                    var colors = _slowmotionSlider.colors;
                    colors.disabledColor = Color.green;
                    _slowmotionSlider.colors = colors;
                }
            }
        }

        if(_slowmotionHook)
        {
            Time.timeScale = 0.65f;
        }

        _movement = Vector3.zero;

        _movement += rewiredPlayer.GetAxis("VerticalMove") * transform.forward * Time.deltaTime;

        if (_movementState != MovementStates.ONWALL)
        {
            _movement += rewiredPlayer.GetAxis("HorizontalMove") * transform.right * Time.deltaTime;
        }

        if (rewiredPlayer.GetButtonDown("Jump"))
            Jump();
        
    }
    
    private void Move()
    {
        //_rigidbody.MovePosition(transform.position + _movement.normalized * _speed);
        //_rigidbody.velocity = Vector3.ClampMagnitude(_rigidbody.velocity, _maxSpeed);
        if (_comboMoveUp)
        {
            ComboMoveUp();
        }
        if (_movementState == MovementStates.INAIR)
            _rigidbody.AddForce(_movement.normalized * _speed / 7.5f, ForceMode.Impulse);
        else if (_movementState == MovementStates.ONWALL)
        {
            if (!_wallJump)
            {
                _rigidbody.velocity = new Vector3(_rigidbody.velocity.x, -0.1f, _rigidbody.velocity.z);
                _rigidbody.AddForce(_movement.normalized * _speed, ForceMode.Impulse);
            }
        }
        else
            _rigidbody.AddForce(_movement.normalized * _speed, ForceMode.Impulse);

        ApplyVelocityConstraints();
    }

    private void ApplyVelocityConstraints()
    {
        Vector2 constrainedVelocityXZ = new Vector2(_rigidbody.velocity.x, _rigidbody.velocity.z);

        constrainedVelocityXZ = Vector2.ClampMagnitude(constrainedVelocityXZ, _horizontalVelMax);

        float constraindeVelocityY = Mathf.Clamp(_rigidbody.velocity.y, _minY, _maxY);

        Vector3 constrainedVelocity = new Vector3(constrainedVelocityXZ.x, constraindeVelocityY, constrainedVelocityXZ.y);

        _rigidbody.velocity = constrainedVelocity;
    }

    private void Jump()
    {
        ComboJumpStunCheck();
        if (isAttachedToConeWall)
        {
            GameManager.s_instance.Hook.ResetContactPoint();
        }
        if (_movementState == MovementStates.ONWALL)
        {
            PlayJumpSound();
            _rigidbody.velocity = Vector3.zero;

            _rigidbody.AddForce(Vector3.Normalize(Vector3.up + new Vector3(_wallTrigger.wallNormal.x, 0f, _wallTrigger.wallNormal.z).normalized) * _jumpHeight * _wallJumpHeight);
            _jumpCounter = 1;
            _wallJump = true;
            return;
        }

        if (_jumpCounter < 2)
        {
            PlayJumpSound();
            _rigidbody.velocity = new Vector3(_rigidbody.velocity.x, 0.0f, _rigidbody.velocity.z);
            _rigidbody.AddForce(Vector3.up * _jumpHeight * 10.0f);
            _jumpCounter++;
        }
        
        
    }

    private void ComboJumpStunCheck()
    {
        if (GameManager.s_instance.Hook.ContactPoint != null)
        {
            if (GameManager.s_instance.Hook.ContactPoint.transform.parent != null)
            {
                if (GameManager.s_instance.Hook.ContactPoint.transform.parent.CompareTag("enemy"))
                {
                    if (GameManager.s_instance.Hook.CurrentSize < 10.0f)
                    {
                        if(_comboCurrentRechargTime>=_comboRechargeTime/2.0f)
                            StartCoroutine(WaitComboJumpStun());
                    }
                }
            }
        }
    }
    
   
    private IEnumerator WaitComboJumpStun()
    {
        _comboMoveUp = true;
        Transform enemy = GameManager.s_instance.Hook.ContactPoint.transform.parent;
        if (enemy != null)
        {
            if (enemy.CompareTag("enemy"))
            {
                PlayJumpSound();
                GameManager.s_instance.Hook.PlayPlasma();
                enemy.GetComponent<Enemy>().GetStunned(_comboStunTime*_comboCurrentRechargTime/10.0f);
                _comboCurrentRechargTime = 0.0f;
            }
        }
        yield return new WaitForSeconds(_comboStrength);
        _comboMoveUp = false;
    }


    private void ComboMoveUp()
    {
        _rigidbody.velocity = new Vector3(_rigidbody.velocity.x, 0.0f, _rigidbody.velocity.z);
        _rigidbody.AddForce(Vector3.up * _jumpHeight * 400.0f*Time.deltaTime);
    }
    
    
    private void UpdateCooldowns()
    {
        if(_kickCooldown > 0.0f)
        {
            _kickCooldown -= Time.deltaTime;

            if (_kickCooldown <= 0.0f)
                _kickCooldown = 0.0f;
        }
        if (_comboCurrentRechargTime < _comboRechargeTime)
        {
            _comboCurrentRechargTime += Time.deltaTime;
            UpdateComboBar();    
        }
    }

    public void KnockBack()
    {
        _rigidbody.velocity = new Vector3(_rigidbody.velocity.x, 0.0f, _rigidbody.velocity.z);
        
        _rigidbody.AddForce(Vector3.up * _jumpHeight * 10.0f);
        
        _rigidbody.AddForce(transform.forward*-1 * 20000.0f,ForceMode.Impulse);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (_jumpCounter > 0 && _movementState == MovementStates.ONGROUND)
            _jumpCounter = 0;
        if (collision.gameObject.tag.Equals("coneWall"))
        {
            isAttachedToConeWall = true;
        }
    }

    private void OnCollisionExit(Collision other)
    {
        isAttachedToConeWall = false;
    }

    public void TakeDamage(float damage = 1, EnemyTypes enemyType = EnemyTypes.DEFAULT)
    {
        if (!_blockDamage)
        {
            _blockDamage = true;
            StartCoroutine(BlockDamage());

            _health -= damage;
            UpdateHealthBar();

            //_hitAudioSource.PlayOneShot(_hitSounds[_rnd.Next(0, _hitSounds.Length - 1)]);
            //StartCoroutine(WaitForPainSound());
            PlayPainSound(enemyType);

           // UIManager.s_instance.RemoveHeart();

            _renderer.material = _damageMat;
            StartCoroutine(ResetMaterial());

            if (_health <= 0)
            {
                Die(false);
            }
        }
    }

    //private IEnumerator WaitForPainSound()
    //{
    //    yield return new WaitForSeconds(0.5f);
    //    _painAudioSource.PlayOneShot(_painSounds[_rnd.Next(0, _painSounds.Length - 1)]);

    //}

    public void UpdateComboBar()
    {
        _comboBar.SetActive(true);
        float percentValue = _comboCurrentRechargTime / _comboRechargeTime * 100.0f;
        _progressBarComboComp.BarValue = percentValue;
    }

    public void UpdateHealthBar(bool showDamageScreen = true)
    {
        _healthRTPC.SetValue(gameObject, _health);
        _healthBar.SetActive(true);

        float percentValue = _health / _initialHealth * 100.0f;

        _progressBarComp.BarValue = percentValue;
        _hpText.GetComponent<Text>().text = "Health:" + _health;

        if (_health < 3f && _health >= 2f)
            _damageScreenAlpha = 0.1f;
        else if (_health < 2f)
            _damageScreenAlpha = 0.2f;

        if (showDamageScreen && _damageScreen != null)
            ShowDamageScreen();
    }
    
    private IEnumerator ResetMaterial()
    {
        yield return new WaitForSeconds(0.15f);
        _renderer.material = _mat;
    }

    private IEnumerator BlockDamage()
    {
        yield return new WaitForSeconds(0.15f);
        _blockDamage = false;
    }

    private void Die(bool dieFromFalling)
    {
        if (_dead)
            return;

        SetStatePlayerLife(true);

        _dead = true;

        GameManager.s_instance.Camera.Die();

        if (dieFromFalling)
            PlayFallSound();
        else
            PlayDeathSound();

        StartCoroutine(WaitToReturnToMenu());
    }

    private IEnumerator WaitToReturnToMenu()
    {
        yield return new WaitForSeconds(1.5f);

        UIManager.s_instance.ReturnToMainMenu();
    }

    public float GetHealth()
    {
        return _health;
    }

    public void SetHealth(float addHealth)
    {
        _health += addHealth;

        if (_health > _maxHealth)
            _health = _maxHealth;

        if (_health < 3f && _health >= 2f)
            _damageScreenAlpha = 0.1f;
        else if (_health < 2f)
            _damageScreenAlpha = 0.2f;

        _damageColor.a = _damageScreenAlpha;
        _damageScreen.color = _damageColor;
    }

    public void SetMovementState(MovementStates state)
    {
        if (state == _movementState)
            return;

        _movementState = state;

        if (_movementState == MovementStates.ONWALL)
        {
            _wallJump = false;
            //AkSoundEngine.SetState("PlayerLocation", "OnGround");
            //AkSoundEngine.SetSwitch("PlayerLocation", "OnGround", gameObject);
            AkSoundEngine.PostEvent(OnGroundID, gameObject);
        }

        if (_movementState == MovementStates.ONGROUND)
        {
            PlayLandSound();
            //AkSoundEngine.SetState("PlayerLocation", "OnGround");
            //AkSoundEngine.SetSwitch("PlayerLocation", "OnGround", gameObject);
            AkSoundEngine.PostEvent(OnGroundID, gameObject);
        }

        if (_movementState == MovementStates.INAIR)
        {
            //AkSoundEngine.SetState("PlayerLocation", "InAir");
            //AkSoundEngine.SetSwitch("PlayerLocation", "InAir", gameObject);
            AkSoundEngine.PostEvent(InAirID, gameObject);
        }
    }

    private void ShowDamageScreen()
    {
        _damageColor.a = 0.5f;
        _damageScreen.color = _damageColor;
        _damageScreenFadeOut = true;
    }

    private void FadeOutDamageScreen()
    {
        _damageColor.a -= Time.deltaTime * 0.75f;
        _damageScreen.color = _damageColor;

        if(_damageColor.a < _damageScreenAlpha)
        {
            _damageColor.a = _damageScreenAlpha;
            _damageScreen.color = _damageColor;
            _damageScreenFadeOut = false;
        }
    }

    
    public void EnableHookSlowmotion()
    {
        _slowmotionHook = true;
    }

    public void DisableHookSlowmotion()
    {
        _slowmotionHook = false;

        if (!_slowmotionPressed)
            Time.timeScale = 1f;
    }

    private void UpdatePhysicMaterial()
    {
        if (_movementState == MovementStates.ONWALL)
            _collider.material = _noFrictionMat;
        else
            _collider.material = _regularMaterial;
    }

    private void PlayJumpSound()
    {
        AkSoundEngine.PostEvent(Player_JumpID, gameObject);
    }

    private void PlayLandSound()
    {
        AkSoundEngine.PostEvent(Player_LandID, gameObject);
    }

    private void PlayDeathSound()
    {
        AkSoundEngine.PostEvent(Player_DeathID, gameObject);
    }

    private void PlayFallSound()
    {
        AkSoundEngine.PostEvent(Player_FallID, gameObject);
    }

    private void PlayPainSound(EnemyTypes enemyType)
    {
        switch(enemyType)
        {
            case EnemyTypes.ARACH:
                AkSoundEngine.PostEvent(Player_Pain_ArachID, gameObject);
                break;
            case EnemyTypes.GOLEM:
                AkSoundEngine.PostEvent(Player_Pain_GolemID, gameObject);
                break;
            case EnemyTypes.CRAB:
                AkSoundEngine.PostEvent(Player_Pain_CrabID, gameObject);
                break;
            case EnemyTypes.SKELETON:
                AkSoundEngine.PostEvent(Player_Pain_SkeletonID, gameObject);
                break;
            case EnemyTypes.DEFAULT:
                AkSoundEngine.PostEvent(Player_Pain_DefaultID, gameObject);
                break;
        }
    }




    private void PlayStepSound()
    {
        AkSoundEngine.PostEvent(Player_FootID, gameObject);
    }

    private void ProcessStepCycle()
    {
        if(_movementState == MovementStates.ONGROUND)
        {
            if (_movement != Vector3.zero)
            {
                _currentStep += Time.deltaTime;

                if (_currentStep >= _stepInterval)
                {
                    PlayStepSound();
                    _currentStep = 0f;
                }
            }
        }
        else
        {
            _currentStep = 0f;
        }
    }

    private void CheckHasSlowMo()
    {
        if (Time.timeScale == 1f)
            _slowMo.SetValue(gameObject, 0f);//AkSoundEngine.SetState("PlayerHasSlowMo", 0f);
        else if (Time.timeScale == 0.5f)
            _slowMo.SetValue(gameObject, 1f);//AkSoundEngine.SetState("PlayerHasSlowMo", 1f);
    }

    private void SetStatePlayerLife(bool dead)
    {
        if (dead)
            AkSoundEngine.SetState("PlayerLife", "Defeated");
        else
            AkSoundEngine.SetState("PlayerLife", "Alive");
    }
}



public enum MovementStates { ONGROUND, INAIR, ONWALL}
public enum EnemyTypes { ARACH, GOLEM, CRAB, SKELETON, UFO, SLIDING, DEFAULT}