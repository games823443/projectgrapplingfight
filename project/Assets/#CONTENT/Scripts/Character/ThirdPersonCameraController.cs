﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;

public class ThirdPersonCameraController : MonoBehaviour
{
    [SerializeField] private bool _visualizeHookTarget = false;
    [SerializeField] private float _hookRotationWeight = 0.01f;
    [SerializeField] private float _rotationSpeed = 100.0f;
    [SerializeField] private float _lookCorrectionThreshold = 5.0f;

    private Transform _player;
    
    private GameObject _lookTarget;
    private GameObject _hookTarget;
    private Player rewiredPlayer;

    private bool _dead = false;

    private Vector3 _offset;
    private bool _lastRotSet = false;

    private Camera _cam;

    private void Awake()
    {
        rewiredPlayer = ReInput.players.GetPlayer(0);
        _cam = GetComponent<Camera>();
    }

    // Start is called before the first frame update
    void Start()
    {
        _player = transform.parent;

        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;

        if (!_visualizeHookTarget)
        {
            _lookTarget = new GameObject("lookTarget");
            _hookTarget = new GameObject("hookTarget");
        }
        else
        {
            _lookTarget = GameObject.CreatePrimitive(PrimitiveType.Cube);
            _lookTarget.transform.localScale = Vector3.one * 0.1f;
            _lookTarget.GetComponent<BoxCollider>().enabled = false;

            _hookTarget = GameObject.CreatePrimitive(PrimitiveType.Cube);
            _hookTarget.transform.localScale = Vector3.one * 0.1f;
            _hookTarget.GetComponent<BoxCollider>().enabled = false;
        }

        _offset = transform.position - _player.transform.position;

        transform.SetParent(null);
    }

    // Update is called once per frame
    void Update()
    {
        _cam.backgroundColor = new Color(0.5f, 0.5f, 0.5f);

        if (GameManager.s_instance.Paused)
            return;

        if(_dead)
        {
            float angleX = Vector3.SignedAngle(_player.transform.forward, transform.forward, transform.right);

            if (angleX > -90.0f && !_lastRotSet)
            {
                transform.localEulerAngles = transform.localEulerAngles + new Vector3(Time.deltaTime * -75f, 0.0f, 0.0f);
            }
            else
            {
                _lastRotSet = true;
            }

            return;
        }

        Vector3 newPos = _player.transform.position + _player.transform.right * _offset.x + _player.transform.up * _offset.y + _player.transform.forward * _offset.z;

        transform.position = Vector3.Lerp(transform.position, newPos, 0.5f);
        _lookTarget.transform.position = transform.position + transform.forward * 50.0f;

        //CheckHook();

        float mouseSensitivity = UIManager.s_instance.MouseSensitivity;

        float inputXAxis = rewiredPlayer.GetAxis("HorizontalLookJoystick") * _rotationSpeed * Time.deltaTime * mouseSensitivity;
        float inputYAxis = rewiredPlayer.GetAxis("VerticalLookJoystick") * _rotationSpeed * -1.0f * Time.deltaTime * mouseSensitivity;

        if (inputXAxis == 0.0f)
        {
            inputXAxis = rewiredPlayer.GetAxis("HorizontalLook") * _rotationSpeed * Time.deltaTime * mouseSensitivity;
        }
        if (inputYAxis == 0.0f)
        {
            inputYAxis = rewiredPlayer.GetAxis("VerticalLook") * _rotationSpeed * -1.0f * Time.deltaTime * mouseSensitivity;
        }

        _lookTarget.transform.RotateAround(transform.position, transform.right, inputYAxis);
        _lookTarget.transform.RotateAround(transform.position, transform.up, inputXAxis);

        UpdateLookAt();
    }

    private void CheckHook() //move camera with hook movement
    {
        if(GameManager.s_instance.Hook.State == HookStates.PULLTOWARDSOBJECT)
        {
            _hookTarget.transform.position = _lookTarget.transform.position;

            float angle = Vector3.SignedAngle(transform.forward, GameManager.s_instance.Hook.ContactPoint.transform.position - transform.position, transform.up) / 2.0f;
            
            if (angle <= _lookCorrectionThreshold)
                angle = 0.0f;

            _hookTarget.transform.RotateAround(GameManager.s_instance.Hook.ContactPoint.transform.position, transform.up, angle);

            _lookTarget.transform.position = Vector3.Slerp(_lookTarget.transform.position, _hookTarget.transform.position, _hookRotationWeight);

            UpdateLookAt();
        }
    }

    private void UpdateLookAt()
    {
        float angleX = Vector3.SignedAngle(_player.transform.forward, _lookTarget.transform.position - transform.position, transform.right);
        
        float correctionVal = 0.0f;

        if (angleX > 60.0f)
            correctionVal = 60.0f - angleX;
        else if (angleX < -60.0f)
            correctionVal = -60.0f - angleX;

        _lookTarget.transform.RotateAround(transform.position, transform.right, correctionVal);

        _player.transform.LookAt(_lookTarget.transform);
        _player.transform.rotation = Quaternion.Euler(0.0f, _player.transform.eulerAngles.y, 0.0f);

        transform.LookAt(_lookTarget.transform);
        transform.rotation = Quaternion.Euler(transform.eulerAngles.x, transform.eulerAngles.y, 0.0f);
    }

    public void SetCamera(Transform lookAtTarget)
    {
        transform.LookAt(lookAtTarget);
    }

    public void Die()
    {
        _dead = true;
    }
}
