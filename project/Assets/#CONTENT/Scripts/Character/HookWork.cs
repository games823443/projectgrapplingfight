﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;
using UnityEngine.UI;

public class HookWork : MonoBehaviour
{
    [SerializeField] private float _growSpeed = 1.0f;
    [SerializeField] private float _hookPullSpeed = 1.5f;
    [SerializeField] private float _distanceWeight = 0.1f;
    [SerializeField] private float _gravityWeight = 0.1f;
    [SerializeField] private float _hookMaxLength = 50.0f;
    [SerializeField] private int _maxDetachCount = 3;
    [SerializeField] private float _forwardWeight = 0.5f;
    [SerializeField] private float _objectPullMultiplier = 0.75f;
    [SerializeField] private float _detachedHookPullMultiplier = 0.15f;
    [SerializeField] private Material _hookMat = null;

    [Header("Effects")]
    [SerializeField] private AudioClip[] _sounds = null;
    [SerializeField] private AudioSource _audioSource = null;
    [SerializeField] private GameObject _muzzleFlash = null;
    [SerializeField] private Transform _muzzleFlashSpawnPoint = null;
    [SerializeField] private GameObject _plasmaEffect = null;

    private AudioSource _hookAudioSource = null;
    [SerializeField] private AudioClip[] _hookAttachClips = null;
    [SerializeField] private AudioClip[] _pullableAttachClips = null;

    private bool useHold = false;
    
    private Transform _parent;

    private float _size = 0.1f;
    
    public float CurrentSize=>_size;

    private GameObject _contactPoint;

    private bool _hookAttached = false;
    private bool _getContactPoint = true;

    private HookStates _state = HookStates.NONE;

    private Queue<GameObject> _detachedHooks = new Queue<GameObject>();

    public HookStates State => _state;
    
    public GameObject ContactPoint => _contactPoint;

    private Player rewiredPlayer;

    private System.Random _rnd = new System.Random();

    private PullableObject _pullableObject = null;
    private float _hookDamage = 0.0f;
    
    private bool _isMagnetic = false;
    [SerializeField] private float _magneticRadius = 5.0f;

    private float _sphereCastRadius = 3.5f;

    private bool _hookAttachedToEnemy = false;
    private Enemy _attachedEnemy;

    private Outline _lastOutline;

    private float _hookScaleOffset = 3.0f;

    
    
    /*new*/
    private Spring spring;
    [SerializeField] private LineRenderer lr;
    public int quality;
    public float damper;
    public float strength;
    public float velocity;
    public float waveCount;
    public float waveHeight;
    public AnimationCurve affectCurve;
    private Vector3 currentGrapplePosition;
    
    
    
    /*new end */
    
    public float HookPullSpeed
    {
        get => _hookPullSpeed;
        set => _hookPullSpeed = value;
    }

    private void Awake()
    {
        rewiredPlayer = ReInput.players.GetPlayer(0);
        
        /*new*/
        spring = new Spring();
        spring.SetTarget(0);
    }

    private void Start()
    {
        _parent = transform.parent;
        _contactPoint = new GameObject("contactPoint");

        _hookAudioSource = GetComponent<AudioSource>();
        _hookAudioSource.volume = 0.2f;
        _hookAudioSource.spatialBlend = 0.6f;

        _hookPullSpeed *= 100000;
        _forwardWeight *= 100000;
        _objectPullMultiplier /= 1000;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        CheckCrosshair();
        if (GameManager.s_instance.Paused)
            return;

        CheckInputs();

        switch(_state)
        {
            case HookStates.GROW:
                Grow();
                DrawRope();
                break;
            case HookStates.SHRINK:
                Shrink();
                DrawRope();
                break;
            case HookStates.PULLTOWARDSOBJECT:
                PullTowardsObject();
                DrawRope();
                break;
            case HookStates.PULLOBJECTTOPLAYER:
                PullObjectToPlayer();
                DrawRope();
                break;
            case HookStates.HOLDHOOK:
                holdHook();
                DrawRope();
                break;
            case HookStates.NONE:
                _parent.localRotation = Quaternion.Euler(0.0f, 0.0f, 0.0f);
                //_parent.localRotation = Quaternion.Euler(GameManager.s_instance.Camera.transform.rotation.eulerAngles.x, 0.0f, 0.0f);
                /*new*/
                spring.Reset();
                if (lr.positionCount > 0)
                    lr.positionCount = 0;
                break;
        }
    }

    private void Grow()
    {
        _parent.localScale = new Vector3(_parent.localScale.x, _parent.localScale.y, _size);
        _parent.LookAt(_contactPoint.transform.position);

        _size += Time.deltaTime * _growSpeed;
        GameManager.s_instance.GetCharacterController().EnableHookSlowmotion();

        if (_hookAttached && _size >= Vector3.Distance(_parent.transform.position, _contactPoint.transform.position)/_hookScaleOffset)
        {
            if (_contactPoint.transform.parent != null && _contactPoint.transform.parent.CompareTag("pullableObject"))
            {
                if (rewiredPlayer.GetButton("ReleaseHook") && useHold)
                {
                    _state = HookStates.HOLDHOOK;
                }
                else
                {
                    _state = HookStates.PULLOBJECTTOPLAYER;
                    _hookAudioSource.PlayOneShot(_pullableAttachClips[_rnd.Next(0, _pullableAttachClips.Length)]);
                    _pullableObject = _contactPoint.transform.parent.GetComponent<PullableObject>();
                    //_pullableObject.SetIsHooked(true);
                }
            }
            else if (_contactPoint.transform.parent != null && _contactPoint.transform.parent.CompareTag("itemSpawner"))
            {
                if (rewiredPlayer.GetButton("ReleaseHook") && useHold)
                {
                    _state = HookStates.HOLDHOOK;
                }
                else
                {
                    GameObject obj = _contactPoint.transform.parent.GetComponent<ItemSpawner>().GetPullableObject();

                    _contactPoint.transform.position = obj.transform.position;
                    _contactPoint.transform.SetParent(obj.transform);

                    _state = HookStates.PULLOBJECTTOPLAYER;
                    _hookAudioSource.PlayOneShot(_pullableAttachClips[_rnd.Next(0, _pullableAttachClips.Length)]);
                }
            }
            else
            {
                if (rewiredPlayer.GetButton("ReleaseHook") && useHold)
                {
                    _state = HookStates.HOLDHOOK;
                }
                else
                {
                    _state = HookStates.PULLTOWARDSOBJECT;

                    if (_contactPoint.transform.parent.CompareTag("enemy") && _hookDamage > 0.0f)
                    {
                        Enemy e = _contactPoint.transform.parent.GetComponent<Enemy>();

                        if (!e.Dead && e != null)
                        {
                            e.TakeDamage(_hookDamage);
                            e.AttachHook(true);

                            _attachedEnemy = e;
                            _hookAttachedToEnemy = true;
                        }
                    }

                    _hookAudioSource.PlayOneShot(_hookAttachClips[_rnd.Next(0, _hookAttachClips.Length)]);
                }
            }
               
        }
        else if (!_hookAttached && _size >= _hookMaxLength/_hookScaleOffset)
        {
            _state = HookStates.SHRINK;
            GameManager.s_instance.GetCharacterController().DisableHookSlowmotion();
        }
    }

    private void Shrink()
    {
        _parent.localScale = new Vector3(_parent.localScale.x, _parent.localScale.y, _size);
        if (_contactPoint != null)
        {
            _parent.LookAt(_contactPoint.transform.position);
        }

        _size -= Time.deltaTime * _growSpeed * 2.0f;

            if (_size <= 0.1f)
            {
                _state = HookStates.NONE;
                _size = 0.1f;
                _parent.localScale = new Vector3(_parent.localScale.x, _parent.localScale.y, _size);
                _getContactPoint = true;
            } 
      
    }

    private void PullTowardsObject()
    {
        GameManager.s_instance.GetCharacterController().DisableHookSlowmotion();

        if (_contactPoint.transform.parent == null)
        {
            _state = HookStates.SHRINK;
            return;
        }

        if (Vector3.Magnitude(_contactPoint.transform.position - _parent.parent.transform.position) > 2.5f)
        {
            Vector3 distance = _contactPoint.transform.position - _parent.parent.transform.position;
            GameManager.s_instance.characterRigidbody.AddForce(((distance.normalized *_hookPullSpeed + GameManager.s_instance.Camera.transform.forward * _forwardWeight) * Mathf.Clamp(distance.magnitude * _distanceWeight, 0.5f, 1f) + Physics.gravity * _gravityWeight) * Time.deltaTime); ;
            Debug.DrawRay(GameManager.s_instance.characterRigidbody.transform.position, GameManager.s_instance.characterRigidbody.velocity);
        }
        
        _parent.LookAt(_contactPoint.transform.position);

        _size = Vector3.Magnitude(_contactPoint.transform.position - _parent.parent.transform.position)/3f;
        _parent.localScale = new Vector3(_parent.localScale.x, _parent.localScale.y, _size);
    }

    private void PullObjectToPlayer()
    {
        if (_contactPoint.transform.parent == null)
        {
            _state = HookStates.SHRINK;
            return;
        }

        float magnitude = Vector3.Magnitude(_contactPoint.transform.position - _parent.parent.transform.position);

        if (/*magnitude > 4f && */magnitude < 200f)
        {
            Vector3 distance = _parent.parent.transform.position - _contactPoint.transform.position;
            if (_isMagnetic)
            {
                Collider[] hitColliders = Physics.OverlapSphere( _contactPoint.transform.position, _magneticRadius);

                foreach (Collider hit in hitColliders)
                {
                    if (hit.gameObject.layer == 16&&hit.gameObject!=_contactPoint.gameObject)
                    {
                        hit.gameObject.transform.GetComponent<Rigidbody>().AddForce((distance + GameManager.s_instance.Camera.transform.forward * distance.magnitude * -1) * Time.deltaTime * _hookPullSpeed * _objectPullMultiplier*2.0f);

                    }
                }
               
            }
            _contactPoint.transform.parent.GetComponent<Rigidbody>().AddForce((distance + GameManager.s_instance.Camera.transform.forward * distance.magnitude * -1) * Time.deltaTime * _hookPullSpeed * _objectPullMultiplier);
            
            _parent.LookAt(_contactPoint.transform.position);

            _size = Vector3.Magnitude(_contactPoint.transform.position - _parent.parent.transform.position)/_hookScaleOffset;
            _parent.localScale = new Vector3(_parent.localScale.x, _parent.localScale.y, _size);
        }
        else
        {
            _state = HookStates.SHRINK;
            _contactPoint.transform.parent = null;

            GameManager.s_instance.GetCharacterController().DisableHookSlowmotion();
        }
    }

    private void CheckInputs()
    {
        if (rewiredPlayer.GetButtonDown("SwitchHold"))
        {
            useHold = !useHold;
            Debug.Log("useHold switched "+useHold);
        }
        if (_state == HookStates.NONE && rewiredPlayer.GetButtonDown("ShootHook"))
        {
            if(_getContactPoint)
            {
                GetContactPoint();
                _getContactPoint = false;
                _state = HookStates.GROW;

                PlaySound();
            }
        }

        if ((_state == HookStates.GROW || _state == HookStates.PULLTOWARDSOBJECT || _state == HookStates.PULLOBJECTTOPLAYER) && rewiredPlayer.GetButtonUp("ShootHook"))
        {
            _state = HookStates.SHRINK;

            if(_pullableObject != null)
            {
                //_pullableObject.SetIsHooked(false);
                _pullableObject = null;
            }

            if(_hookAttachedToEnemy && _attachedEnemy != null)
            {
                _hookAttachedToEnemy = false;
                _attachedEnemy.AttachHook(false);
                _attachedEnemy = null;
            }

            GameManager.s_instance.GetCharacterController().DisableHookSlowmotion();

            _contactPoint.transform.SetParent(null);
        }

        if((_state == HookStates.PULLOBJECTTOPLAYER || _state == HookStates.PULLTOWARDSOBJECT) && rewiredPlayer.GetButtonDown("ReleaseHook"))
        {
            PlaySound();
            Detach();
            GameManager.s_instance.GetCharacterController().DisableHookSlowmotion();
        }
        
    }
    
    public void checkDetachForTarget()
    {
        if((_state == HookStates.PULLOBJECTTOPLAYER || _state == HookStates.PULLTOWARDSOBJECT))
        {
            PlaySound();
            Detach();
        }
    }

    private void CheckCrosshair()
    {
    //    GameObject camera = GameManager.s_instance.Camera.gameObject;
    //    GameObject crosshair = GameManager.s_instance.Crosshair;
    //    Vector3 crosshairPos = crosshair.transform.position;
    //    RaycastHit hit;
    //    Ray ray = camera.GetComponent<Camera>().ScreenPointToRay(new Vector2(crosshairPos.x, crosshairPos.y));
    //    Physics.Raycast(ray, out hit);
    //    Image[] images=  crosshair.GetComponentsInChildren<Image>();
    //    float dist = Vector3.Distance(gameObject.transform.position, hit.point);
    //    if (dist<=_hookMaxLength)
    //    {
    //        foreach (Image image in images)
    //        {
    //         image.color=new Color32(0,247,0,100);
    //        }
            
    //    }
    //    else
    //    {
    //        foreach (Image image in images)
    //        {
    //            image.color=new Color32(247,0,0,100);
    //        } 
            
    //    }

        HighlightHookTarget();
    }

    private void GetContactPoint()
    {
        GameObject camera = GameManager.s_instance.Camera.gameObject;
        Vector3 crosshairPos = GameManager.s_instance.Crosshair.transform.position;
        Ray ray = camera.GetComponent<Camera>().ScreenPointToRay(new Vector2(crosshairPos.x, crosshairPos.y)); 
        RaycastHit hit;
        RaycastHit[] hits = Physics.SphereCastAll(ray, _sphereCastRadius, _hookMaxLength);//, LayerMask.NameToLayer("NotGrabbable"));

        //changed to spherecast to enable easier hitting
        // if (Physics.Raycast(ray, out hit, _hookMaxLength, LayerMask.NameToLayer("NotGrabbable")))
        //if (Physics.SphereCast(ray, 5f, out hit, _hookMaxLength))//, LayerMask.NameToLayer("NotGrabbable")))
        GameObject possibleHitGO = null;

        if(hits.Length > 0)
        {
            foreach (var possibleHit in hits)
            {
                if (possibleHit.transform.CompareTag("pullableObject") || possibleHit.transform.CompareTag("enemy"))
                {
                    _contactPoint.transform.position = possibleHit.point;
                    _contactPoint.transform.SetParent(possibleHit.transform);

                    if (possibleHit.transform.CompareTag("enemy"))
                    {
                        possibleHit.transform.GetComponent<Enemy>().AttachHook(true);
                    }

                    _hookAttached = true;

                    possibleHitGO = possibleHit.transform.gameObject;

                    break;
                }
            }

            bool isInSight;

            if (possibleHitGO == null)
                isInSight = false;
            else
                isInSight = CheckIfContactPointIsInSight(_contactPoint.transform.position, possibleHitGO);

            if (isInSight)
                return;
            else
            {
                if (Physics.Raycast(ray, out hit, _hookMaxLength))//, LayerMask.NameToLayer("NotGrabbable")))
                {
                    _contactPoint.transform.position = hit.point;
                    _contactPoint.transform.SetParent(hit.transform);

                    if (hit.transform.CompareTag("enemy"))
                        hit.transform.GetComponent<Enemy>().AttachHook(true);

                    _hookAttached = true;
                    return;
                }
            }
        }

        SetDefaultHookTarget();
    }

    public void HighlightHookTarget()
    {
        GameObject camera = GameManager.s_instance.Camera.gameObject;
        Vector3 crosshairPos = GameManager.s_instance.Crosshair.transform.position;
        Ray ray = camera.GetComponent<Camera>().ScreenPointToRay(new Vector2(crosshairPos.x, crosshairPos.y));
        RaycastHit hit;
        RaycastHit[] hits = Physics.SphereCastAll(ray, _sphereCastRadius, _hookMaxLength);
        GameObject possibleHitGO = null;

        if (_lastOutline != null)
            _lastOutline.enabled = false;

        SetCrosshairColor(Color.red);

        if (hits.Length > 0)
        {
            foreach (var possibleHit in hits)
            {
                if (possibleHit.transform.CompareTag("pullableObject") || possibleHit.transform.CompareTag("enemy"))
                {
                    if (possibleHit.transform.CompareTag("enemy"))
                    {
                        possibleHit.transform.GetComponent<Enemy>().AttachHook(true);
                    }

                    possibleHitGO = possibleHit.transform.gameObject;
                    break;
                }
            }

            bool isInSight;

            if (possibleHitGO == null)
                isInSight = false;
            else
                isInSight = CheckIfContactPointIsInSight(_contactPoint.transform.position, possibleHitGO);

            if (isInSight)
            {
                _lastOutline = possibleHitGO.GetComponent<Outline>();
                SetCrosshairColor(Color.green);
                _lastOutline.enabled = true;
                return;
            }
            else
            {
                if (Physics.Raycast(ray, out hit, _hookMaxLength))//, LayerMask.NameToLayer("NotGrabbable")))
                {
                    bool enableOutline = false;

                    if (hit.transform.CompareTag("enemy"))
                    {
                        hit.transform.GetComponent<Enemy>().AttachHook(true);

                        enableOutline = true;
                    }
                    else if(hit.transform.CompareTag("pullableObject"))
                    {
                        enableOutline = true;
                    }

                    if(enableOutline)
                    { 
                        _lastOutline = hit.transform.GetComponent<Outline>();
                        _lastOutline.enabled = true;
                    }

                    SetCrosshairColor(Color.green);
                    return;
                }
            }

        }
    }

    private void SetCrosshairColor(Color c)
    {
        GameObject crosshair = GameManager.s_instance.Crosshair;
        Image[] images = crosshair.GetComponentsInChildren<Image>();

        foreach (Image image in images)
        {
            image.color = c;
        }
    }

    private void SetDefaultHookTarget()
    {
        GameObject camera = GameManager.s_instance.Camera.gameObject;
        Vector3 crosshairPos = GameManager.s_instance.Crosshair.transform.position;
        Vector3 pos = camera.GetComponent<Camera>().ScreenToWorldPoint(new Vector2(crosshairPos.x, crosshairPos.y));
        _contactPoint.transform.position = pos + _parent.transform.forward * _hookMaxLength;

        _hookAttached = false;
    }

    public void Detach()
    {
        RaycastHit possibleRaycastHit = new RaycastHit();

        GameObject contactPoint1;
        GameObject contactPoint2; 
        
        contactPoint1 = new GameObject("ContactPoint1");
        contactPoint1.transform.position = _contactPoint.transform.position;
        contactPoint1.transform.SetParent(_contactPoint.transform.parent);

        if (_contactPoint.transform.parent != null)
        {
            if (_contactPoint.transform.parent.CompareTag("enemy"))
                _contactPoint.transform.parent.GetComponent<Enemy>().AttachHook(false);
            else if (_contactPoint.transform.parent.gameObject.layer == LayerMask.NameToLayer("Boids"))
                _contactPoint.transform.parent.GetComponent<Boid>().AttachHook(false);
        }

        RaycastHit[] hits = Physics.SphereCastAll(_parent.parent.transform.position, _sphereCastRadius, GameManager.s_instance.Camera.transform.forward, _hookMaxLength);

        if (hits.Length > 0)
        {
            bool hitfound = false;

            foreach (var potentialHit in hits)
            {
                if (potentialHit.transform.CompareTag("pullableObject") || potentialHit.transform.CompareTag("enemy"))
                {
                    possibleRaycastHit = potentialHit;
                    hitfound = true;
                    break;
                }
            }

            //bool isInSight = false;

            //if (possibleHitGO == null)
            //    isInSight = false;
            //else
            //    isInSight = CheckIfContactPointIsInSight(_contactPoint.transform.position, possibleHitGO);

            //Debug.Log("isInSight: " + isInSight);

            if (!hitfound)
            {
                RaycastHit hit;

                if (Physics.Raycast(_parent.parent.transform.position, GameManager.s_instance.Camera.transform.forward, out hit, _hookMaxLength))
                {
                    possibleRaycastHit = hit;
                }
                else
                {
                    return;
                }
            }
        }
        else
        {
            return;
        }

        contactPoint2 = new GameObject("ContactPoint2");
        contactPoint2.transform.position = possibleRaycastHit.point;
        contactPoint2.transform.SetParent(possibleRaycastHit.transform);

        GameObject detachedHook = GameObject.CreatePrimitive(PrimitiveType.Cube);
        detachedHook.tag = "detachedHook";
        DetachedHook detachedHookComp = detachedHook.AddComponent<DetachedHook>();
        detachedHookComp.SetContactPoints(contactPoint1, contactPoint2);
        detachedHookComp.SetHookPullSpeed(_hookPullSpeed * _detachedHookPullMultiplier);

        detachedHook.GetComponent<Renderer>().material = _hookMat;

        _state = HookStates.SHRINK;

        _detachedHooks.Enqueue(detachedHook);
        
        if (_detachedHooks.Count > _maxDetachCount)
        {
            GameObject detHook = _detachedHooks.Dequeue();
            if(detHook != null)
                Destroy(detHook);
        }
    }

    private bool CheckIfContactPointIsInSight(Vector3 contactPointPosition, GameObject hitObject)
    {
        RaycastHit hit;

        if (Physics.Raycast(GameManager.s_instance.Camera.transform.position, contactPointPosition - GameManager.s_instance.Camera.transform.position, out hit, _hookMaxLength))//, LayerMask.NameToLayer("NotGrabbable")))
        {
            if (hit.transform.gameObject == hitObject)
                return true;
            else
                return false;
        }

        return false;
    }


    private void holdHook()
    {
        _parent.LookAt(_contactPoint.transform.position);
        if (!rewiredPlayer.GetButton("ReleaseHook"))
        {
            if (_contactPoint.transform.parent.CompareTag("pullableObject"))
            {
                _state = HookStates.PULLOBJECTTOPLAYER;
            }
            else
            {
                _state = HookStates.PULLTOWARDSOBJECT;
            }
        }
        else
        {
            _state = HookStates.HOLDHOOK;
        }
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (_hookAttached || _state != HookStates.GROW)
            return;

        RaycastHit hit;

        if (Physics.Raycast(_parent.transform.position, _parent.transform.forward, out hit))
        {
            if (hit.transform.gameObject.layer != LayerMask.NameToLayer("NotGrabbable"))
            {
                _contactPoint.transform.position = hit.point;
                _contactPoint.transform.SetParent(hit.transform);

                if (hit.transform.gameObject.layer == LayerMask.NameToLayer("Boids"))
                    hit.transform.GetComponent<Boid>().AttachHook(true);

                _hookAttached = true;
            }
        }
    }
    
    private void PlaySound()
    {
        if (_sounds.Length > 0)
        {
            _audioSource.clip = _sounds[Random.Range(0, _sounds.Length - 1)];
            _audioSource.loop = false;
            _audioSource.Play();

            GameObject muzzle = Instantiate(_muzzleFlash);
            muzzle.transform.parent = _muzzleFlashSpawnPoint;
            muzzle.transform.localPosition = Vector3.zero;
            muzzle.transform.localRotation = Quaternion.Euler(Vector3.zero);
        }
    }
    /*new*/
    private void DrawRope() {
        
        if (lr.positionCount == 0)
            {
                spring.SetVelocity(velocity);
                lr.positionCount = quality + 1;
            }

            spring.SetDamper(damper);
            spring.SetStrength(strength);
            spring.Update(Time.deltaTime);
            var up = Quaternion.LookRotation((_contactPoint.transform.position - _muzzleFlashSpawnPoint.position)
                .normalized) * Vector3.up;
            currentGrapplePosition = Vector3.Lerp(currentGrapplePosition, _contactPoint.transform.position,
                Time.deltaTime * 12f);
            for (var i = 0; i < quality + 1; i++)
            {
                var delta = i / (float) quality;
                var offset = up * waveHeight * Mathf.Sin(delta * waveCount * Mathf.PI) * spring.Value *
                             affectCurve.Evaluate(delta);
                if (_state == HookStates.GROW)
                {
                    lr.SetPosition(i,
                        Vector3.Lerp(_muzzleFlashSpawnPoint.position, currentGrapplePosition, delta) + offset);
                }
                else
                {
                    lr.SetPosition(i,
                        Vector3.Lerp(_muzzleFlashSpawnPoint.position, _contactPoint.transform.position, delta));
                }
            }
    }
    
    public void PlayPlasma()
    {
        _plasmaEffect.GetComponent<ParticleSystem>().Play();
        StartCoroutine(WaitForPlasmaAnim());
    }

    private IEnumerator WaitForPlasmaAnim()
    {
        yield return new WaitForSeconds(1.0f);
        _plasmaEffect.GetComponent<ParticleSystem>().Stop();
    }

    public void ResetContactPoint()
    {
        _contactPoint.transform.SetParent(null);
    }

    public void IncreaseRange(float effectStrength)
    {
        _hookMaxLength += effectStrength;
    }

    public void IncreaseHookDamage(float damage)
    {
        _hookDamage += damage;
    }

    public void IncreaseHookSpeed(float speed)
    {
        _growSpeed += speed;
        _hookPullSpeed += speed*1000f;
    }

    public void EnableMagnetic()
    {
        _isMagnetic = true;
    }
}

//public enum HookStates { NONE, GROW, SHRINK, PULLTOWARDSOBJECT, PULLOBJECTTOPLAYER,HoldHook }
