﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;

public class WeaponController : MonoBehaviour
{
    private bool _move = false;
    private bool _moveBack = false;

    [SerializeField] private float _hitSpeed = 20.0f;
    [SerializeField] private float _swordDamage = 5.0f;
    private float _angleX = 0.0f;

    private float _cameraAngleOffset = -90.0f;

    private bool _enemyTriggerEnter = false;
    private Enemy _other;

    private Player rewiredPlayer;

    private void Awake()
    {
        rewiredPlayer = ReInput.players.GetPlayer(0);
    }

    // Update is called once per frame
    void Update()
    {
        if (!_move && !_moveBack && rewiredPlayer.GetButtonDown("WeaponAttack"))
            _move = true;

        if(_move)
        {
            //transform.parent.Rotate(new Vector3(_hitSpeed * Time.deltaTime, 0.0f, 0.0f), Space.Self);
            _angleX += _hitSpeed * Time.deltaTime;

            if (_angleX >= 120.0f)
            {
                _move = false;
                _moveBack = true;
            }
        }
        else if(_moveBack)
        {
            //transform.parent.Rotate(new Vector3(0.0f, 0.0f, _hitSpeed * Time.deltaTime * -1));
            _angleX -= _hitSpeed * Time.deltaTime;

            if (_angleX <= 0.0f)
            {
                _moveBack = false;
                _angleX = 0f;
                //transform.parent.localRotation = Quaternion.Euler(Vector3.zero);
            }
        }

        if ((_move || _moveBack) && _enemyTriggerEnter)
            _other?.TakeDamage(_swordDamage);

        transform.parent.localRotation = Quaternion.Euler(GameManager.s_instance.Camera.transform.rotation.eulerAngles.x + _angleX + _cameraAngleOffset, 0.0f, 0.0f);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("enemy"))
        {
            _enemyTriggerEnter = true;

            _other = other.gameObject.GetComponent<Enemy>();

            if (_move || _moveBack)
            {
                _other.TakeDamage(_swordDamage);
                GameObject bloodEffect = GameObject.Instantiate(GameManager.s_instance.BloodSplatterEffect, other.ClosestPoint(transform.position), Quaternion.identity);
                bloodEffect.transform.LookAt(transform.position);
            }
        }
        else if (other.CompareTag("detachedHook"))
            Destroy(other.gameObject);
        else if (other.gameObject.layer == LayerMask.NameToLayer("Boids"))
        {
            if (_move || _moveBack)
            {
                other.gameObject.GetComponent<Boid>().Kill();
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("enemy"))
        {
            _enemyTriggerEnter = false;
            _other = null;
        }

    }
}
