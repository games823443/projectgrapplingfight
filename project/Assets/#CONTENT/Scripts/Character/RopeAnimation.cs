using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RopeAnimation : MonoBehaviour
{
    // Start is called before the first frame update
    private float _fpsCnt;

    private LineRenderer _lr;
    [SerializeField] private Texture[] _textures;
    private int _animStep;
    [SerializeField] private float _fps = 30f;

    public Texture[] Textures
    {
        get => _textures;
        set => _textures = value;
    }

    private void Awake()
    {
        _lr = GetComponent<LineRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        _fpsCnt += Time.deltaTime;
        if (_fpsCnt >= 1f / _fps)
        {
            _animStep++;
            if (_animStep == _textures.Length)
            {
                _animStep = 0;
            }
            _lr.material.SetTexture("_MainTex",_textures[_animStep]);
            _fpsCnt = 0;

        }
    }
}
