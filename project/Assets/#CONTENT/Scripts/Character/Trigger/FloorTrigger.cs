﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorTrigger : MonoBehaviour
{
    [SerializeField] private WallTrigger _wallTrigger = null;
    private int _floorTriggerCounter = 0;

    private void Update()
    {
        if(_floorTriggerCounter > 0)
            GameManager.s_instance.GetCharacterController().SetMovementState(MovementStates.ONGROUND);

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("enemy") || other.CompareTag("Player") || other.CompareTag("hookGun") || other.transform.gameObject.layer == LayerMask.NameToLayer("NotGrabbable"))
            return;

        _floorTriggerCounter++;

        GameManager.s_instance.GetCharacterController().SetMovementState(MovementStates.ONGROUND);

        AttachMaterial amat = other.transform.GetComponent<AttachMaterial>();

        if (amat != null)
        {
            SetFloorSwitch(amat.AttachMat);
            Debug.Log("Floor mat switched to " + amat.AttachMat);
        }
        else
        {
            SetFloorSwitch(AttachMaterials.STONE);
            Debug.Log("Floor mat switched to Default");
        }

    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("enemy") || other.CompareTag("Player") || other.CompareTag("hookGun") || other.transform.gameObject.layer == LayerMask.NameToLayer("NotGrabbable"))
            return;

        _floorTriggerCounter--;

        if (_wallTrigger.isCloseToWall)
            GameManager.s_instance.GetCharacterController().SetMovementState(MovementStates.ONWALL);
        else
            GameManager.s_instance.GetCharacterController().SetMovementState(MovementStates.INAIR);
    }

    private void SetFloorSwitch(AttachMaterials material)
    {
        switch(material)
        {
            case AttachMaterials.STONE:
                AkSoundEngine.SetSwitch("Materials_Foot", "Stone", gameObject);
                AkSoundEngine.SetSwitch("Materials_Land", "Stone", gameObject);
                break;
            case AttachMaterials.WOOD:
                AkSoundEngine.SetSwitch("Materials_Foot", "Wood", gameObject);
                AkSoundEngine.SetSwitch("Materials_Land", "Wood", gameObject);
                break;
            case AttachMaterials.METAL:
                AkSoundEngine.SetSwitch("Materials_Foot", "Metal", gameObject);
                AkSoundEngine.SetSwitch("Materials_Land", "Metal", gameObject);
                break;
        }
    }
}
