﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallTrigger : MonoBehaviour
{
    private bool _closeToWall = false;

    public bool isCloseToWall => _closeToWall;

    private Vector3 _wallNormal = Vector3.zero;
    private Collider _otherCollider;

    public Vector3 wallNormal => _wallNormal;
    
    private void Update()
    {
        if(_otherCollider == null)
            _closeToWall = false;

        if (_closeToWall)
        {
            Vector3 closestPoint = _otherCollider.ClosestPoint(transform.position);
            
            _wallNormal = Vector3.Normalize(transform.position - closestPoint);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("enemy") || other.CompareTag("Player") || other.CompareTag("hookGun") ||  other.transform.gameObject.layer == LayerMask.NameToLayer("NotGrabbable") || other.transform.gameObject.layer == LayerMask.NameToLayer("Ignore Raycast"))
            return;

        _otherCollider = other;

        if (GameManager.s_instance.GetCharacterController().movementState == MovementStates.INAIR)
        {
            GameManager.s_instance.GetCharacterController().SetMovementState(MovementStates.ONWALL);

        }

        _closeToWall = true;
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("enemy") || other.CompareTag("Player") || other.CompareTag("hookGun") || other.transform.gameObject.layer == LayerMask.NameToLayer("NotGrabbable") || other.transform.gameObject.layer == LayerMask.NameToLayer("Ignore Raycast"))
            return;

        if (GameManager.s_instance.GetCharacterController().movementState == MovementStates.ONWALL)
        {
            GameManager.s_instance.GetCharacterController().SetMovementState(MovementStates.INAIR);
        }

        _wallNormal = Vector3.zero;

        _closeToWall = false;
    }
}
