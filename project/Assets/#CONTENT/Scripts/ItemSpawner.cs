﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ItemSpawner : MonoBehaviour
{
    [SerializeField] private GameObject[] _pullableObjects = null;
    private System.Random _rnd = new System.Random();

    [SerializeField] private float _radius = 1.0f;
    [SerializeField] private float _speed = 1.0f;
    

    private Vector3 _initialPosition;

    private void Start()
    {
        _initialPosition = transform.position;
        transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z + _radius);
    }

    private void Update()
    {
        transform.RotateAround(_initialPosition, transform.up, Time.deltaTime * _speed);
    }

   

    public GameObject GetPullableObject()
    {
        int indx = _rnd.Next(0, _pullableObjects.Length-1);

        GameObject pullableObject = Instantiate(_pullableObjects[indx]);
        pullableObject.transform.position = transform.position;

        return pullableObject;
    }
}
