﻿using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;

public class DetachedHook : MonoBehaviour
{
    private ConnectionType _connectionType;
    private GameObject _contactPoint1;

    private Rigidbody _contactPoint1Rigidbody;
    private GameObject _contactPoint2;
    private Rigidbody _contactPoint2Rigidbody;

    private float _hookPullSpeed;

    private Rigidbody _moveableObject;
    private Rigidbody _otherRB;
    private GameObject _other;

    private LineRenderer _lr;
    private void Start()
    {
        gameObject.GetComponent<BoxCollider>().isTrigger = true;
        gameObject.GetComponent<MeshRenderer>().enabled = false;
        _lr = gameObject.AddComponent<LineRenderer>();
        _lr.material = Resources.Load("Material/RopeLight", typeof(Material)) as Material;
        _lr.startWidth = 0.5f;
        _lr.endWidth = 0.5f;
        _lr.positionCount = 2;
        _lr.textureMode = LineTextureMode.Tile;
        Texture[] textures= new Texture[4];
        RopeAnimation ropeAnim=gameObject.AddComponent<RopeAnimation>();
        textures[0]=Resources.Load("Material/electricLine0001", typeof(Texture)) as Texture;
        textures[1]=Resources.Load("Material/electricLine0002", typeof(Texture)) as Texture;
        textures[2]=Resources.Load("Material/electricLine0003", typeof(Texture)) as Texture;
        textures[3]=Resources.Load("Material/electricLine0004", typeof(Texture)) as Texture;
        ropeAnim.Textures = textures;
    }

    // Update is called once per frame
    void Update()
    {
        if (_contactPoint1 == null || _contactPoint2 == null)
        {
            Destroy(gameObject);
        }
        
        switch (_connectionType)
        {
            case ConnectionType.ENEMY2ENEMY:
                Moveable2Moveable();
                DrawRope();
                break;
            case ConnectionType.ENEMY2STATIC:
                Moveable2Static();
                DrawRope();
                break;
            case ConnectionType.PULLABLE2PULLABLE:
                Moveable2Moveable();
                DrawRope();
                break;
            case ConnectionType.PULLABLE2STATIC:
                Moveable2Static();
                DrawRope();
                break;
            case ConnectionType.PULLABLE2ENEMY:
                Moveable2Enemy();
                DrawRope();
                break;
            case ConnectionType.STATIC2STATIC:
                DrawRope();
                break;
        }

        if (_contactPoint1 != null && _contactPoint2 != null)
        {
            transform.position = (_contactPoint1.transform.position + _contactPoint2.transform.position) / 2.0f;
            transform.LookAt(_contactPoint1.transform.position);
            transform.localScale = new Vector3(0.1f, 0.1f,
                Vector3.Distance(_contactPoint1.transform.position, _contactPoint2.transform.position));
        }
        else
            Destroy(gameObject);
    }

    private void OnDestroy()
    {
        //if (_contactPoint1 != null && _contactPoint2 == null)
        //{
        //    if (_contactPoint1?.transform != null && _contactPoint2?.transform != null)
        //    {
        //        if (_contactPoint1.transform.parent != null && _contactPoint2.transform.parent != null)
        //        {
        //            string tag1 = _contactPoint1.transform.parent.tag;
        //            string tag2 = _contactPoint2.transform.parent.tag;

        //            if (_connectionType == ConnectionType.PULLABLE2STATIC)
        //            {
        //                if (tag1 == "enemy")
        //                    _contactPoint1.transform.parent.GetComponent<Enemy>()
        //                        .RemoveObjectWeight(_contactPoint2Rigidbody.mass);
        //                else if (tag2 == "enemy")
        //                    _contactPoint2.transform.parent.GetComponent<Enemy>()
        //                        .RemoveObjectWeight(_contactPoint1Rigidbody.mass);
        //            }
        //        }
        //    }

        //    Destroy(_contactPoint1);
        //    Destroy(_contactPoint2);
        //}

        if(_contactPoint1 != null)
        {
            string tag1 = _contactPoint1.transform.parent.tag;

            if (_connectionType == ConnectionType.PULLABLE2STATIC)
            {
                if (tag1 == "enemy")
                {
                    if (_contactPoint1.transform.parent != null && _contactPoint2Rigidbody!=null)
                    {
                        _contactPoint1.transform.parent.GetComponent<Enemy>()
                            .RemoveObjectWeight(_contactPoint2Rigidbody.mass);
                    }
                }                    
            }
            else if(_connectionType == ConnectionType.PULLABLE2ENEMY)
            {
                if (tag1 == "pullableObject")
                {
                    if (_contactPoint1.transform.parent != null)
                    {
                        _contactPoint1.transform.parent.GetComponent<PullableObject>()
                            .SetIsHooked(false);
                    }
                }
            }

            Destroy(_contactPoint1);
        }

        if (_contactPoint2 != null)
        {
            string tag2 = _contactPoint2.transform.parent.tag;

            if (_connectionType == ConnectionType.PULLABLE2STATIC)
            {
                if (tag2 == "enemy")
                {
                    if (_contactPoint2.transform.parent != null&&_contactPoint1Rigidbody!=null)
                    {
                        _contactPoint2.transform.parent.GetComponent<Enemy>()
                            .RemoveObjectWeight(_contactPoint1Rigidbody.mass);
                    }
                }
            }
            else if (_connectionType == ConnectionType.PULLABLE2ENEMY)
            {
                if (tag2 == "pullableObject")
                {
                    if (_contactPoint2.transform.parent != null)
                    {
                        _contactPoint2.transform.parent.GetComponent<PullableObject>()
                            .SetIsHooked(false);
                    }
                }
            }

            Destroy(_contactPoint2);
        }
    }

    private void Moveable2Static()
    {
        if (_moveableObject != null && _other != null)
            _moveableObject.AddForce((_other.transform.position - _moveableObject.transform.position) * Time.deltaTime *
                                     _hookPullSpeed);
    }

    private void Moveable2Enemy()
    {
        if (_moveableObject != null && _other != null)
        {
            _moveableObject.AddForce((_other.transform.position - _moveableObject.transform.position) * Time.deltaTime *
                                     _hookPullSpeed);

            _moveableObject.velocity = (_other.transform.position - _moveableObject.transform.position).normalized * _moveableObject.velocity.magnitude;
        }
    }

    private void Moveable2Moveable()
    {
        _contactPoint1Rigidbody?.AddForce((_contactPoint2.transform.position - _contactPoint1.transform.position) *
                                          Time.deltaTime * _hookPullSpeed);
        _contactPoint2Rigidbody?.AddForce((_contactPoint1.transform.position - _contactPoint2.transform.position) *
                                          Time.deltaTime * _hookPullSpeed);
    }

    public void SetContactPoints(GameObject cp1, GameObject cp2)
    {
        _contactPoint1 = cp1;
        _contactPoint2 = cp2;

        ContactPoint cpComp1 = _contactPoint1.AddComponent<ContactPoint>();
        cpComp1.SetDetachedHook(gameObject);
        ContactPoint cpComp2 = _contactPoint2.AddComponent<ContactPoint>();
        cpComp2.SetDetachedHook(gameObject);

        string tag1 = _contactPoint1.transform.parent.tag;
        string tag2 = _contactPoint2.transform.parent.tag;

        _contactPoint1Rigidbody = _contactPoint1.transform.parent.GetComponent<Rigidbody>();
        _contactPoint2Rigidbody = _contactPoint2.transform.parent.GetComponent<Rigidbody>();

        if (tag1 == "pullableObject" && tag2 == "pullableObject")
            _connectionType = ConnectionType.PULLABLE2PULLABLE;
        else if ((tag1 == "pullableObject" && tag2 != "pullableObject") ||
                 (tag2 == "pullableObject" && tag1 != "pullableObject"))
        {
            _connectionType = ConnectionType.PULLABLE2STATIC;
            if (tag1 == "pullableObject")
            {
                _moveableObject = _contactPoint1Rigidbody;
                _moveableObject.velocity = Vector3.zero;
                _other = _contactPoint2;

                PullableObject po = _contactPoint1.transform.parent.GetComponent<PullableObject>();

                po.SetDetachedHook(gameObject);

                if (tag2 == "enemy")
                {
                    _other.transform.parent.GetComponent<Enemy>().AddObjectWeight(_moveableObject.mass);
                    _otherRB = _contactPoint2Rigidbody;
                    _connectionType = ConnectionType.PULLABLE2ENEMY;

                    po.SetIsHooked(true);
                }
            }
            else
            {
                _moveableObject = _contactPoint2Rigidbody;
                _moveableObject.velocity = Vector3.zero;
                _other = _contactPoint1;

                PullableObject po = _contactPoint2.transform.parent.GetComponent<PullableObject>();

                po.SetDetachedHook(gameObject);

                if (tag1 == "enemy")
                {
                    _other.transform.parent.GetComponent<Enemy>().AddObjectWeight(_moveableObject.mass);
                    _otherRB = _contactPoint1Rigidbody;
                    _connectionType = ConnectionType.PULLABLE2ENEMY;

                    po.SetIsHooked(true);
                }
            }
        }
        else if (tag1 == "enemy" && tag2 == "enemy")
            _connectionType = ConnectionType.ENEMY2ENEMY;
        else if ((tag1 == "enemy" && tag2 != "enemy") || (tag1 != "enemy" && tag2 == "enemy"))
        {
            _connectionType = ConnectionType.ENEMY2STATIC;
            if (tag1 == "enemy")
            {
                _moveableObject = _contactPoint1Rigidbody;
                _other = _contactPoint2;
            }
            else
            {
                _moveableObject = _contactPoint2Rigidbody;
                _other = _contactPoint1;
            }
        }
        else
            _connectionType = ConnectionType.STATIC2STATIC;
    }

    public void SetHookPullSpeed(float speed)
    {
        _hookPullSpeed = speed;
    }

    private void DrawRope()
    { 
        _lr.SetPosition(0, _contactPoint1.transform.position);
        _lr.SetPosition(1,_contactPoint2.transform.position);
        
    }


    private enum ConnectionType
    {
        STATIC2STATIC,
        PULLABLE2STATIC,
        PULLABLE2PULLABLE,
        ENEMY2STATIC,
        ENEMY2ENEMY,
        PULLABLE2ENEMY
    }
    
}