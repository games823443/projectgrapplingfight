﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField] private MainCharacterController _character = null;
    [SerializeField] private GrapplingHookController _hook = null;
    [SerializeField] private ThirdPersonCameraController _camera = null;
    [SerializeField] private GameObject crosshair;
    
    [Header("Effects")]
    [SerializeField] private GameObject _bloodSplatter = null;
    [SerializeField] private Material _redMaterial = null;
    [SerializeField] private GameObject _fireEffect = null;

    [Header("Enemies")]
    [SerializeField] private GameObject[] _enemyPrefabs = null;
    [SerializeField] private GameObject[] _powerUpPrefabs = null;

    [Header("Outline")]
    [SerializeField] private float _enemyOutlineWidth = 1.0f;
    [SerializeField] private Color _enemyOutlineColor = Color.red;
    [SerializeField] private float _objectOutlineWidth = 1.0f;
    [SerializeField] private Color _objectOutlineColor = Color.green;

    private Rigidbody _characterRigidbody;
    private bool _paused = false;
    
    public static GameManager s_instance;
    public Rigidbody characterRigidbody => _characterRigidbody;

    public GameObject Crosshair => crosshair;
    public GrapplingHookController Hook => _hook;
    public ThirdPersonCameraController Camera => _camera;
    public MainCharacterController CharacterController => _character;
    public bool Paused => _paused;
    public GameObject BloodSplatterEffect => _bloodSplatter;
    private System.Random _rnd = new System.Random();

    public Material RedMaterial => _redMaterial;
    public GameObject FireEffect => _fireEffect;

    public float EnemyOutlineWidth => _enemyOutlineWidth;
    public Color EnemyOutlineColor => _enemyOutlineColor;
    public float ObjectOutlineWidth => _objectOutlineWidth;
    public Color ObjectOutlineColor => _objectOutlineColor;

    public float WaterHight = -145f;

    //---Wwise Events---//

    //music and atmo
    public AK.Wwise.Event Ambience_Wind = new AK.Wwise.Event();
    public uint Ambience_WindID { get { return (uint)(Ambience_Wind == null ? AkSoundEngine.AK_INVALID_PLAYING_ID : Ambience_Wind.Id); } }

    public AK.Wwise.Event Ambience_Rain = new AK.Wwise.Event();
    public uint Ambience_RainID { get { return (uint)(Ambience_Rain == null ? AkSoundEngine.AK_INVALID_PLAYING_ID : Ambience_Rain.Id); } }

    public AK.Wwise.Event Music = new AK.Wwise.Event();
    public uint MusicID { get { return (uint)(Music == null ? AkSoundEngine.AK_INVALID_PLAYING_ID : Music.Id); } }

    public AK.Wwise.Event Boss_Start = new AK.Wwise.Event();
    public uint Boss_StartID { get { return (uint)(Boss_Start == null ? AkSoundEngine.AK_INVALID_PLAYING_ID : Boss_Start.Id); } }

    public AK.Wwise.Event Map_Completed = new AK.Wwise.Event();
    public uint Map_CompletedID { get { return (uint)(Map_Completed == null ? AkSoundEngine.AK_INVALID_PLAYING_ID : Map_Completed.Id); } }

    public AK.Wwise.Event Map_Start = new AK.Wwise.Event();
    public uint Map_StartID { get { return (uint)(Map_Start == null ? AkSoundEngine.AK_INVALID_PLAYING_ID : Map_Start.Id); } }

    public AK.Wwise.Event Monsters_Aware = new AK.Wwise.Event();
    public uint Monsters_AwareID { get { return (uint)(Monsters_Aware == null ? AkSoundEngine.AK_INVALID_PLAYING_ID : Monsters_Aware.Id); } }

    public AK.Wwise.Event Monsters_Unaware = new AK.Wwise.Event();
    public uint Monsters_UnawareID { get { return (uint)(Monsters_Unaware == null ? AkSoundEngine.AK_INVALID_PLAYING_ID : Monsters_Unaware.Id); } }

    public AK.Wwise.Event Pause_Game = new AK.Wwise.Event();
    public uint Pause_GameID { get { return (uint)(Pause_Game == null ? AkSoundEngine.AK_INVALID_PLAYING_ID : Pause_Game.Id); } }

    public AK.Wwise.Event Return_MainMenu = new AK.Wwise.Event();
    public uint Return_MainMenuID { get { return (uint)(Return_MainMenu == null ? AkSoundEngine.AK_INVALID_PLAYING_ID : Return_MainMenu.Id); } }

    public AK.Wwise.Event Map_Loaded = new AK.Wwise.Event();
    public uint Map_LoadedID { get { return (uint)(Map_Loaded == null ? AkSoundEngine.AK_INVALID_PLAYING_ID : Map_Loaded.Id); } }

    private int _alertedEnemies = 0;
    public int AlertedEnemies => _alertedEnemies;

    // Start is called before the first frame update
    void Start()
    {
        if (s_instance == null)
            s_instance = this;
        else
            Debug.LogWarning("GameManager already exists!");

        _characterRigidbody = _character.GetComponent<Rigidbody>();
        RenderSettings.fogDensity = 0.008f;
        PlayAmbienceRain();
        PlayAmbienceWind();
        PlayMusic();

        AkSoundEngine.SetState("Music_Global", "InGame");

        PlayMapLoaded();
        PlayMapStart();
        
    }

    private void Update()
    {
        if (!RenderSettings.fog)
            RenderSettings.fog = true;

        if(RenderSettings.fogDensity != 0.008f)
            RenderSettings.fogDensity = 0.008f;

    }

    public MainCharacterController GetCharacterController()
    {
        return _character;
    }

    public void PauseGame()
    {
        if (UIManager.s_instance.MouseFocus)
        {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }

        _paused = true;
        Time.timeScale = 0.0f;
    }

    public void ResumeGame()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;

        _paused = false;
        Time.timeScale = 1.0f;
    }

    public GameObject GetRandomEnemy()
    {
        return _enemyPrefabs[_rnd.Next(0, _enemyPrefabs.Length)];
    }

    public GameObject GetRandomPickUp()
    {
        return _powerUpPrefabs[_rnd.Next(0, _powerUpPrefabs.Length)];
    }

    public bool Sleep(Vector3 position)
    {
        if (Vector3.Distance(position, _character.gameObject.transform.position) > 300)
            return true;
        else
            return false;
    }

    private void PlayAmbienceWind()
    {
        AkSoundEngine.PostEvent(Ambience_WindID, gameObject);
    }

    private void PlayAmbienceRain()
    {
        AkSoundEngine.PostEvent(Ambience_RainID, gameObject);
    }

    private void PlayMusic()
    {
        AkSoundEngine.PostEvent(MusicID, gameObject);
    }

    public void AddAlertedEnemy()
    {
        if (_alertedEnemies == 0)
        {
            //AkSoundEngine.SetState("Music_InGame", "Combat");
            AkSoundEngine.SetSwitch("Gameplay_Switch", "Combat", gameObject);
            PlayMonstersAware();
        }

        _alertedEnemies++;
    }

    public void RemoveAlertedEnemy()
    {
        _alertedEnemies--;

        if (_alertedEnemies == 0)
        {
            //AkSoundEngine.SetState("Music_InGame", "Explore");
            AkSoundEngine.SetSwitch("Gameplay_Switch", "Explore", gameObject);
            PlayMonstersUnaware();
        }
    }

    public void PlayBossStart()
    {
        AkSoundEngine.PostEvent(Boss_StartID, gameObject);
    }

    public void PlayMapCompleted()
    {
        AkSoundEngine.PostEvent(Map_CompletedID, gameObject);
    }

    public void PlayMapStart()
    {
        AkSoundEngine.PostEvent(Map_StartID, gameObject);
    }

    public void PlayMonstersAware()
    {
        AkSoundEngine.PostEvent(Monsters_AwareID, gameObject);
    }

    public void PlayMonstersUnaware()
    {
        AkSoundEngine.PostEvent(Monsters_UnawareID, gameObject);
    }

    public void PlayPauseGame()
    {
        AkSoundEngine.PostEvent(Pause_GameID, gameObject);
    }

    public void PlayReturnMainMenu()
    {
        AkSoundEngine.PostEvent(Return_MainMenuID, gameObject);
    }

    public void PlayMapLoaded()
    {
        AkSoundEngine.PostEvent(Map_LoadedID, gameObject);
    }
}
