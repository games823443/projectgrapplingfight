using System;
using System.IO;
using Rewired;
using UnityEngine;

public class ScreenShots : MonoBehaviour
{
    private Player rewiredPlayer;
    private int cnt;

    private string path;
    private void Awake()
    {
        rewiredPlayer = ReInput.players.GetPlayer(0);
        path=Environment.GetFolderPath(Environment.SpecialFolder.MyPictures);
        path += "\\GrapplingFight";
        if (!Directory.Exists(path))
        {
            Directory.CreateDirectory(path);
        }
        cnt = 0;

    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (rewiredPlayer.GetButtonDown("ScreenShot"))
        {
            cnt = cnt + 1;
            ScreenCapture.CaptureScreenshot(path+"\\Screenshot"+cnt+".png",2);
            Debug.Log("ScreenShot saved");
        }
    }
}
