using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttachMaterial : MonoBehaviour
{
    [SerializeField] private AttachMaterials _material = AttachMaterials.NONE;
    public AttachMaterials AttachMat => _material;
}
