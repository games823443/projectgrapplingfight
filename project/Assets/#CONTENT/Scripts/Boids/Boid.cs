﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]

public class Boid : MonoBehaviour
{
    [SerializeField] private bool _isLeader = false;

    [Header("Sounds")]
    [SerializeField] private AudioClip[] _hitSounds = null;

    private List<GameObject> _visibleBoids;

    private Vector3 _velocity;
    private Vector3 _collisionDirection;

    private SwarmManager _swarmManager;

    private int _collisionCounter = 0;

    private GameObject _attackTarget;
    private bool _inPlaceForAttack = false;

    private bool _attack = false;

    private float _minSpeed;
    private float _maxSpeed;

    private Animator _animator;
    private SwarmCoordinator _swarmCoordinator;

    private bool _hookAttached = false;
    private System.Random _rnd = new System.Random();
    private AudioSource _audioSource;

    private Outline _outline;
    private bool _initOutline = true;

    // Start is called before the first frame update
    void Start()
    {
        _visibleBoids = new List<GameObject>();
        _collisionDirection = Vector3.zero;

        float startSpeed = (BoidsManager.Instance.MinSpeed + BoidsManager.Instance.MaxSpeed) / 2.0f;
        _velocity = transform.forward * startSpeed;

        _swarmManager = transform.parent.GetComponent<SwarmManager>();
        _swarmManager.RegisterBoid(this);

        SkinnedMeshRenderer renderer = GetComponentInChildren<SkinnedMeshRenderer>();

        if (_isLeader)
            renderer.material = BoidsManager.Instance.LeaderMaterial;
        else
            renderer.material = BoidsManager.Instance.RegularMaterial;

        _minSpeed = BoidsManager.Instance.MinSpeed;
        _maxSpeed = BoidsManager.Instance.MaxSpeed;

        _animator = GetComponent<Animator>();
        _swarmCoordinator = _swarmManager.GetSwarmCoordinator();

        _audioSource = GetComponent<AudioSource>();
        _audioSource.spatialBlend = 0.5f;
        _audioSource.volume = 0.5f;

        if (_isLeader)
            transform.localScale *= 2f;

        _outline = gameObject.AddComponent<Outline>();
        _outline.enabled = false;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (GameManager.s_instance.Sleep(transform.position))
            return;

        if (_initOutline)
        {
            _outline.OutlineWidth = GameManager.s_instance.EnemyOutlineWidth;
            _outline.OutlineMode = Outline.Mode.OutlineAll;
            _outline.OutlineColor = GameManager.s_instance.EnemyOutlineColor;

            _initOutline = false;
        }

        if (_swarmCoordinator.AllLeadersDead)
            Destroy(gameObject);
        
        Vector3 acceleration = SteerToTarget();

        if (_visibleBoids.Count != 0)
        {
            acceleration += Seperation() * BoidsManager.Instance.SeperationWeight;
            if (!_isLeader)
            {
                acceleration += Cohesion() * BoidsManager.Instance.CohesionWeight;
                acceleration += Alignment() * BoidsManager.Instance.AlignmentWeight;
            }
        }

        float collisionWeight = BoidsManager.Instance.CollisionAvoidanceWeight;

        if (_isLeader)
        {
            //CheckCollision();

            if (_attackTarget != null && !_attack)
                collisionWeight = BoidsManager.Instance.AttackCollisionAvoidanceWeight;
        }

        acceleration += SteerTowards(_collisionDirection) * collisionWeight;

        _velocity += acceleration * Time.fixedDeltaTime;
        float speed = _velocity.magnitude;
        Vector3 dir = _velocity / speed;

        speed = Mathf.Clamp(speed, _minSpeed, _maxSpeed);
        _velocity = dir * speed;

        transform.position += _velocity * Time.fixedDeltaTime;
        transform.forward = dir;

        if (Vector3.Distance(transform.position, transform.parent.position) > 200.0f)
            transform.position = transform.parent.position;
    }

    private void CheckCollision()
    {
        if (_collisionCounter >= 5)
        {
            RaycastHit hit;

            if (Physics.Raycast(transform.position, transform.forward, out hit, 50.0f, ~BoidsManager.Instance.IgnoreLayerMask))
            {
                _collisionDirection = transform.position - hit.point;
            }
            _collisionCounter = 0;
        }
        else
        {
            _collisionCounter++;
        }

        if(_isLeader)
            Debug.DrawRay(transform.position, _collisionDirection, Color.red);

        _collisionDirection = Vector3.ClampMagnitude(_collisionDirection, _collisionDirection.magnitude * 0.95f);
    }
    
    private Vector3 SteerTowards(Vector3 direction)
    {
        return Vector3.ClampMagnitude(direction.normalized * _maxSpeed - _velocity, BoidsManager.Instance.MaxSteerForce);
    }

    private Vector3 SteerToTarget()
    {
        Vector3 distance;

        float weight = BoidsManager.Instance.TargetWeight;

        if (!_isLeader && _swarmManager.SwarmTarget != null)
        {
            distance = _swarmManager.SwarmTarget.transform.position - transform.position;
        }
        else if(_swarmManager.SwarmTarget == null)
        {
            distance = transform.forward;
            _swarmManager.SetLastLeaderPos(transform.position);
            _swarmManager.GetNewSwarmTarget();
        }
        else if(_isLeader && _attackTarget != null)
        {
            distance = _attackTarget.transform.position - transform.position;
            weight = BoidsManager.Instance.AttackTargetWeight;
            Debug.DrawRay(transform.position, distance);

            if (distance.magnitude < 2.0f && !_inPlaceForAttack && !_attack)
            {
                _inPlaceForAttack = true;
                StartCoroutine(LeaderInPlace());
            }

            if(_attack && distance.magnitude < 2.0f)
            {
                StopAttack();
            }
        }
        else
            distance = transform.forward;


        return SteerTowards(distance) * weight;
    }

    private Vector3 Seperation()
    {
        if (this == null)
            return Vector3.zero;

        Vector3 distancesSum = Vector3.zero;

        foreach (GameObject boid in _visibleBoids)
        {
            if(boid != null)
                distancesSum += transform.position - boid.transform.position;
        }

        distancesSum /= _visibleBoids.Count;

        return SteerTowards(distancesSum);
    }

    private Vector3 Cohesion()
    {
        Vector3 centerOfFlock = Vector3.zero;

        foreach(var boid in _visibleBoids)
        {
            if(boid != null)
                centerOfFlock += boid.transform.position;
        }

        centerOfFlock /= _visibleBoids.Count;

        Vector3 direction = centerOfFlock - transform.position;

        return SteerTowards(direction);
    }

    private Vector3 Alignment()
    {
        Vector3 heading = Vector3.zero;

        foreach (var boid in _visibleBoids)
            if( boid != null)
                heading += boid.transform.forward;

        heading /= _visibleBoids.Count;

        return SteerTowards(heading);
    }

    public void SetAttackTarget(GameObject target)
    {
        if (!_isLeader)
            return;

        _attackTarget = target;
    }

    public void RemoveAttackTarget()
    {
        if (!_isLeader)
            return;

        _attackTarget = null;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if(_attack)
            {
                GameManager.s_instance.GetCharacterController().TakeDamage(1);
            }
            return;
        }

        if(other.gameObject.layer == LayerMask.NameToLayer("Boids") || other.CompareTag("boid"))
        {
            _visibleBoids.Add(other.gameObject);
        }
        else if(other.CompareTag("pullableObject"))
        {
            if (other.GetComponent<Rigidbody>().velocity.magnitude > 2.0f)
            {
                Kill();
            }
        }
        else
        {
            RaycastHit hit;

            if(Physics.Raycast(transform.position, transform.forward, out hit, 50.0f, ~BoidsManager.Instance.IgnoreLayerMask))
            {
                _collisionDirection = transform.position - hit.point;
            }

        }
    }

    private IEnumerator LeaderInPlace()
    {
        yield return new WaitForSeconds(7.5f);

        _swarmCoordinator.LeaderIsReady();
    }

    public void Attack()
    {
        if (!_isLeader)
            return;

        _inPlaceForAttack = false;
        _attack = true;

        _swarmManager.NotifyAttack(_attack);

        _swarmManager.SetSpeedForBoids(BoidsManager.Instance.AttackMinSpeed, BoidsManager.Instance.AttackMaxSpeed);
    }

    private void StopAttack()
    {
        if (!_isLeader)
            return;

        _attack = false;

        _swarmManager.NotifyAttack(_attack);

        RemoveAttackTarget();
        _swarmManager.SetSpeedForBoids(BoidsManager.Instance.MinSpeed, BoidsManager.Instance.MaxSpeed);

        _swarmCoordinator.AllowAttack();
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Boids"))
        {
            if (_visibleBoids.Contains(other.gameObject))
                _visibleBoids.Remove(other.gameObject);
        }
        else
            _collisionDirection = Vector3.zero;
    }

    public void SetMinMaxSpeed(float minSpeed, float maxSpeed)
    {
        _minSpeed = minSpeed;
        _maxSpeed = maxSpeed;
    }

    public void SetAttackAnimation(bool attack)
    {
        if(_animator != null)
            _animator.SetBool("attack", attack);
    }

    public void AttachHook(bool attach)
    {
        _hookAttached = attach;
    }

    public void Kill()
    {
        GameObject effect = Instantiate(BoidsManager.Instance.SmokeEffect);
        effect.transform.position = transform.position;

        _audioSource.PlayOneShot(_hitSounds[_rnd.Next(0, _hitSounds.Length)]);

        if (_isLeader)
        {
            _swarmCoordinator.RemoveSwarmTarget(this);
            _swarmManager.SetLastLeaderPos(transform.position);
            _swarmManager.GetNewSwarmTarget();
        }

        if (_hookAttached)
            GameManager.s_instance.Hook.ResetContactPoint();

        Destroy(gameObject);
    }
}
