﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoidsManager : MonoBehaviour
{
    private static BoidsManager s_instance;
    public static BoidsManager Instance { get { return s_instance; } }

    [Header("Boids Settings")]
    [SerializeField] private float _minSpeed = 0.1f;
    [SerializeField] private float _maxSpeed = 5f;
    [SerializeField] private float _maxSteerForce = 8.0f;
    [SerializeField] private LayerMask _ignoreLayer = 0;
    [SerializeField] private GameObject _smokeEffect = null;

    [Header("Weights")]
    [SerializeField] private float _targetWeight = 1.0f;
    [SerializeField] private float _seperationWeight = 1.0f;
    [SerializeField] private float _cohesionWeight = 1.0f;
    [SerializeField] private float _alignmentWeight = 1.0f;
    [SerializeField] private float _collisionAvoidanceWeight = 1.0f;

    [Header("Attack")]
    [SerializeField] private float _attackTargetWeight = 1.0f;
    [SerializeField] private float _attackCollisionWeight = 1.0f;
    [SerializeField] private float _attackMinSpeed = 1.0f;
    [SerializeField] private float _attackMaxSpeed = 1.0f;
    [SerializeField] private float _attackRate = 100.0f;

    [Header("Materials")]
    [SerializeField] private Material _leaderMaterial = null;
    [SerializeField] private Material _regularMaterial = null;

    public float MinSpeed => _minSpeed;
    public float MaxSpeed => _maxSpeed;
    public float MaxSteerForce => _maxSteerForce;
    public float TargetWeight => _targetWeight;
    public float SeperationWeight => _seperationWeight;
    public float CohesionWeight => _cohesionWeight;
    public float AlignmentWeight => _alignmentWeight;
    public float CollisionAvoidanceWeight => _collisionAvoidanceWeight;
    public float AttackCollisionAvoidanceWeight => _attackCollisionWeight;
    public float AttackTargetWeight => _attackTargetWeight;
    public float AttackMinSpeed => _attackMinSpeed;
    public float AttackMaxSpeed => _attackMaxSpeed;
    public float AttackRate => _attackRate;

    public Material LeaderMaterial => _leaderMaterial;
    public Material RegularMaterial => _regularMaterial;

    public LayerMask IgnoreLayerMask => _ignoreLayer;
    public GameObject SmokeEffect => _smokeEffect;

    void Awake()
    {
        if (s_instance != null && s_instance != this)
            Destroy(this);
        else
            s_instance = this;
    }
}
