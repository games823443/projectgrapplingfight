﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SwarmCoordinator : MonoBehaviour
{
    [SerializeField] private List<Boid> _swarmTargets = null;
    public List<Boid> SwarmTargets => _swarmTargets;

    private float _attackCountdown = 0.0f;
    private List<GameObject> _attackTargets;

    private bool _blockAttack = false;

    private int _leadersReadyToAttack = 0;
    private GameObject _playerToAttack;
    private bool _allLeadersDead = false;
    public bool AllLeadersDead => _allLeadersDead;

    private int _layer = -1;

    public int Layer
    {
        get => _layer;
        set => _layer=value;
    }

    // Start is called before the first frame update
    void Start()
    {
        _attackTargets = new List<GameObject>();
        _playerToAttack = new GameObject();
    }

    // Update is called once per frame
    void Update()
    {
        if (IsCharacterCloseEnough())
        {
            if (!_blockAttack)
                _attackCountdown += Random.Range(5.0f, 10.0f) * Time.deltaTime;

            if (_attackCountdown >= BoidsManager.Instance.AttackRate)
            {
                PrepareAttack();
                _attackCountdown = 0.0f;
            }
        }
    }

    private void PrepareAttack()
    {
        for (int i = 0; i < _swarmTargets.Count; i++)
        {
            GameObject target = new GameObject("attackTarget");

            target.transform.position = GameManager.s_instance.GetCharacterController().transform.position;
            target.transform.position += GameManager.s_instance.GetCharacterController().transform.forward * 30.0f + new Vector3(0.0f, 30.0f, 0.0f);

            target.transform.RotateAround(GameManager.s_instance.GetCharacterController().transform.position, Vector3.up, 360.0f / _swarmTargets.Count * i);

            _attackTargets.Add(target);

            _swarmTargets[i].SetAttackTarget(target);
        }

        _blockAttack = true;
    }

    public void LeaderIsReady()
    {
        _leadersReadyToAttack++;

        if (_leadersReadyToAttack == _swarmTargets.Count)
        {
            Attack();
            _leadersReadyToAttack = 0;
        }
    }

    private void Attack()
    {
        _playerToAttack.transform.position = GameManager.s_instance.GetCharacterController().transform.position + new Vector3(0.0f, 7.5f, 0.0f);

        foreach (var swarmTarget in _swarmTargets)
        {
            swarmTarget.SetAttackTarget(_playerToAttack);
            swarmTarget.Attack();
        }

        foreach (var attackTarget in _attackTargets)
            Destroy(attackTarget);
    }

    public void AllowAttack()
    {
        _blockAttack = false;
    }

    public void RemoveSwarmTarget(Boid target)
    {
        _swarmTargets.Remove(target);
        if (_swarmTargets.Count <= 0)
        {
            _allLeadersDead = true;
            if (_layer != -1)
            {
                MapGenerator.s_instance.SpawnLayerTransition(_layer);
            }
        }
    }

    private bool IsCharacterCloseEnough()
    {

        if (Vector3.Distance(transform.position, GameManager.s_instance.GetCharacterController().transform.position) < 100.0f)
            return true;
        else
            return false;
    }
}
