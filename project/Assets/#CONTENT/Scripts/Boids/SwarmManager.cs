﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwarmManager : MonoBehaviour
{
    [SerializeField] private int _swarmID = 0;
    [SerializeField] private Boid _swarmTarget = null;
    [SerializeField] private SwarmCoordinator _swarmCoordinator = null;

    public SwarmCoordinator SwarmCoordinator => _swarmCoordinator;

    private Vector3 _lastLeaderPos;

    public Boid SwarmTarget => _swarmTarget;

    private List<Boid> _boids;

    public int SwarmID => _swarmID;

    // Start is called before the first frame update
    void Awake()
    {
        _boids = new List<Boid>();
    }

    public void GetNewSwarmTarget()
    {
        if (_swarmCoordinator.SwarmTargets.Count == 0)
        {
            _swarmTarget = null;
            return;
        }

        if (_lastLeaderPos == null)
            Debug.LogError("set leader pos");

        Boid newTarget = _swarmCoordinator.SwarmTargets[0];

        foreach (Boid target in _swarmCoordinator.SwarmTargets)
        {
            if (Vector3.Distance(target.transform.position, _lastLeaderPos) < Vector3.Distance(newTarget.transform.position, _lastLeaderPos))
                newTarget = target;
        }

        _swarmTarget = newTarget;
    }

    public void RegisterBoid(Boid boid)
    {
        _boids.Add(boid);
    }

    public Vector3 GetCenterOfWholeFlock()
    {
        Vector3 _center = Vector3.zero;

        foreach (var boid in _boids)
            _center += boid.transform.position;

        _center /= _boids.Count;

        _center += _swarmTarget.transform.position;
        _center /= 2;

        return _center;
    }

    public void SetLastLeaderPos(Vector3 lastPost)
    {
        _lastLeaderPos = lastPost;
    }

    public void SetSpeedForBoids(float minSpeed, float maxSpeed)
    {
        foreach (var boid in _boids)
            boid.SetMinMaxSpeed(minSpeed, maxSpeed);
    }

    public void NotifyAttack(bool attack)
    {
        foreach (var b in _boids)
            b.SetAttackAnimation(attack);  
    }

    public SwarmCoordinator GetSwarmCoordinator()
    {
        return _swarmCoordinator;
    }
}
