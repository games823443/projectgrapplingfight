/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID AMBIENCE_INAIR = 3691931759U;
        static const AkUniqueID AMBIENCE_ONGROUND = 1863042906U;
        static const AkUniqueID AMBIENCE_RAIN = 710389270U;
        static const AkUniqueID AMBIENCE_WIND = 156328838U;
        static const AkUniqueID ARACH_ATTACK = 1019256087U;
        static const AkUniqueID ARACH_DEATH = 54500247U;
        static const AkUniqueID ARACH_FOOT = 2862477563U;
        static const AkUniqueID ARACH_IDLE = 2904903353U;
        static const AkUniqueID ARACH_PAIN = 1364441295U;
        static const AkUniqueID BOSS_START = 2985974755U;
        static const AkUniqueID CRAB_ATTACK = 1604427714U;
        static const AkUniqueID CRAB_DEATH = 116215848U;
        static const AkUniqueID CRAB_FOOT = 2127982606U;
        static const AkUniqueID CRAB_PAIN = 1969517458U;
        static const AkUniqueID CRAB_SCREAM = 376073053U;
        static const AkUniqueID GOLEM_ATTACK = 292088046U;
        static const AkUniqueID GOLEM_DEATH = 4226340564U;
        static const AkUniqueID GOLEM_FOOT = 2053776890U;
        static const AkUniqueID GOLEM_IDLE = 276488104U;
        static const AkUniqueID GOLEM_PAIN = 2946755542U;
        static const AkUniqueID GOLEM_SCREAM = 2805128073U;
        static const AkUniqueID GUN_ATTACH = 4052621899U;
        static const AkUniqueID GUN_START = 3493045776U;
        static const AkUniqueID GUN_STOP = 3664266748U;
        static const AkUniqueID ITEM_PICKUP_HEALTH = 395970340U;
        static const AkUniqueID ITEM_PICKUP_HOOKDAMAGE = 3825086380U;
        static const AkUniqueID ITEM_PICKUP_HOOKLENGTH = 2576737963U;
        static const AkUniqueID ITEM_PICKUP_HOOKSPEED = 3599813628U;
        static const AkUniqueID ITEM_PICKUP_MAGNET = 77758800U;
        static const AkUniqueID ITEM_PICKUP_SLOW_MO = 1281326912U;
        static const AkUniqueID ITEM_PICKUP_SPEED = 897096747U;
        static const AkUniqueID ITEM_SPAWN_HEALTH = 4142252629U;
        static const AkUniqueID ITEM_SPAWN_HOOKDAMAGE = 3356348797U;
        static const AkUniqueID ITEM_SPAWN_HOOKLENGTH = 2231464570U;
        static const AkUniqueID ITEM_SPAWN_HOOKSPEED = 1898248767U;
        static const AkUniqueID ITEM_SPAWN_MAGNET = 2913347685U;
        static const AkUniqueID ITEM_SPAWN_SLOW_MO = 193573495U;
        static const AkUniqueID ITEM_SPAWN_SPEED = 1916100416U;
        static const AkUniqueID MAP_COMPLETED = 1656055475U;
        static const AkUniqueID MENU_CLICK = 760777789U;
        static const AkUniqueID MENU_CONTINUE = 2666848820U;
        static const AkUniqueID MENU_PAUSE = 2170009975U;
        static const AkUniqueID MENU_RETURNMAINMENU = 2118713027U;
        static const AkUniqueID MENU_SLIDERRELEASE = 3941864403U;
        static const AkUniqueID MENU_START = 3908297853U;
        static const AkUniqueID MONSTERS_AWARE = 3430883159U;
        static const AkUniqueID MONSTERS_UNAWARE = 2415014760U;
        static const AkUniqueID MUSIC = 3991942870U;
        static const AkUniqueID OBJECT_COLLIDE_BARREL = 235968710U;
        static const AkUniqueID OBJECT_COLLIDE_GASTANKLARGE = 2516745734U;
        static const AkUniqueID OBJECT_COLLIDE_GASTANKSMALL = 792668066U;
        static const AkUniqueID OBJECT_COLLIDE_PLASTICCONTAINER = 2373789927U;
        static const AkUniqueID OBJECT_COLLIDE_ROCK = 118122761U;
        static const AkUniqueID OBJECT_COLLIDE_SMALLTRASH = 1305213107U;
        static const AkUniqueID OBJECT_COLLIDE_TINCAN = 1216663063U;
        static const AkUniqueID OBJECT_COLLIDE_TRASHBAG = 549490946U;
        static const AkUniqueID OBJECT_COLLIDE_TRASHBIN = 683711891U;
        static const AkUniqueID OBJECT_COLLIDE_WOODBOXLARGE = 3697176305U;
        static const AkUniqueID OBJECT_COLLIDE_WOODBOXSMALL = 3336019769U;
        static const AkUniqueID OBJECT_EXTRADAMAGE_BURNING_START = 938804485U;
        static const AkUniqueID OBJECT_EXTRADAMAGE_BURNING_STOP = 3064487591U;
        static const AkUniqueID PLAYER_DEATH = 3083087645U;
        static const AkUniqueID PLAYER_FALL = 2551268862U;
        static const AkUniqueID PLAYER_FOOT = 2532225481U;
        static const AkUniqueID PLAYER_JUMP = 1305133589U;
        static const AkUniqueID PLAYER_LAND = 3629196698U;
        static const AkUniqueID PLAYER_PAIN_ARACH = 1174497823U;
        static const AkUniqueID PLAYER_PAIN_CRAB = 462453546U;
        static const AkUniqueID PLAYER_PAIN_GOLEM = 1378372188U;
        static const AkUniqueID PLAYER_PAIN_SKELETON = 2953291717U;
        static const AkUniqueID SKELETON_ATTACK = 2888548111U;
        static const AkUniqueID SKELETON_DEATH = 2670662143U;
        static const AkUniqueID SKELETON_FOOT = 3226425987U;
        static const AkUniqueID SKELETON_IDLE = 2418878945U;
        static const AkUniqueID SKELETON_PAIN = 1730949655U;
        static const AkUniqueID UFO_DEATH = 2895641108U;
        static const AkUniqueID UFO_IDLE = 419829992U;
        static const AkUniqueID UFO_PAIN = 3089994006U;
    } // namespace EVENTS

    namespace STATES
    {
        namespace MUSIC_GLOBAL
        {
            static const AkUniqueID GROUP = 2880921812U;

            namespace STATE
            {
                static const AkUniqueID INGAME = 984691642U;
                static const AkUniqueID MENU = 2607556080U;
                static const AkUniqueID NONE = 748895195U;
            } // namespace STATE
        } // namespace MUSIC_GLOBAL

        namespace MUSIC_INGAME
        {
            static const AkUniqueID GROUP = 2186578306U;

            namespace STATE
            {
                static const AkUniqueID BOSS = 1560169506U;
                static const AkUniqueID INGAME_MENU = 72369404U;
                static const AkUniqueID NONE = 748895195U;
                static const AkUniqueID VICTORY = 2716678721U;
            } // namespace STATE
        } // namespace MUSIC_INGAME

        namespace PLAYER_LOCATION
        {
            static const AkUniqueID GROUP = 3238956108U;

            namespace STATE
            {
                static const AkUniqueID INAIR = 1710892944U;
                static const AkUniqueID NONE = 748895195U;
                static const AkUniqueID ONGROUND = 1200033591U;
            } // namespace STATE
        } // namespace PLAYER_LOCATION

        namespace PLAYERLIFE
        {
            static const AkUniqueID GROUP = 444815956U;

            namespace STATE
            {
                static const AkUniqueID ALIVE = 655265632U;
                static const AkUniqueID DEFEATED = 2791675679U;
                static const AkUniqueID NONE = 748895195U;
            } // namespace STATE
        } // namespace PLAYERLIFE

    } // namespace STATES

    namespace SWITCHES
    {
        namespace COLLISIONVELOCITY
        {
            static const AkUniqueID GROUP = 1840797538U;

            namespace SWITCH
            {
                static const AkUniqueID HIGH = 3550808449U;
                static const AkUniqueID LOW = 545371365U;
                static const AkUniqueID MEDIUM = 2849147824U;
            } // namespace SWITCH
        } // namespace COLLISIONVELOCITY

        namespace GAMEPLAY_SWITCH
        {
            static const AkUniqueID GROUP = 2702523344U;

            namespace SWITCH
            {
                static const AkUniqueID COMBAT = 2764240573U;
                static const AkUniqueID EXPLORE = 579523862U;
            } // namespace SWITCH
        } // namespace GAMEPLAY_SWITCH

        namespace MATERIALS_ATTACH
        {
            static const AkUniqueID GROUP = 4149199559U;

            namespace SWITCH
            {
                static const AkUniqueID METAL = 2473969246U;
                static const AkUniqueID PLASTIC = 660835419U;
                static const AkUniqueID STONE = 1216965916U;
                static const AkUniqueID WOOD = 2058049674U;
            } // namespace SWITCH
        } // namespace MATERIALS_ATTACH

        namespace MATERIALS_FOOT
        {
            static const AkUniqueID GROUP = 3051607420U;

            namespace SWITCH
            {
                static const AkUniqueID METAL = 2473969246U;
                static const AkUniqueID STONE = 1216965916U;
                static const AkUniqueID WOOD = 2058049674U;
            } // namespace SWITCH
        } // namespace MATERIALS_FOOT

        namespace MATERIALS_LAND
        {
            static const AkUniqueID GROUP = 617726775U;

            namespace SWITCH
            {
                static const AkUniqueID METAL = 2473969246U;
                static const AkUniqueID STONE = 1216965916U;
                static const AkUniqueID WOOD = 2058049674U;
            } // namespace SWITCH
        } // namespace MATERIALS_LAND

    } // namespace SWITCHES

    namespace GAME_PARAMETERS
    {
        static const AkUniqueID COLLISIONVELOCITY = 1840797538U;
        static const AkUniqueID MUSICVOLUME = 2346531308U;
        static const AkUniqueID PLAYERHEALTH = 151362964U;
        static const AkUniqueID SFXVOLUME = 988953028U;
        static const AkUniqueID SLOWMO = 3236148094U;
    } // namespace GAME_PARAMETERS

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID MUSIC = 3991942870U;
        static const AkUniqueID SFX = 393239870U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID AMBIENCE = 85412153U;
        static const AkUniqueID BOSS = 1560169506U;
        static const AkUniqueID CHARACTER = 436743010U;
        static const AkUniqueID COMBAT = 2764240573U;
        static const AkUniqueID ENEMIES = 2242381963U;
        static const AkUniqueID ENVIRONMENT = 1229948536U;
        static const AkUniqueID EXPLORE = 579523862U;
        static const AkUniqueID ITEMSPICKUPS = 4175975082U;
        static const AkUniqueID MAIN = 3161908922U;
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
        static const AkUniqueID MENUINGAME = 1407284327U;
        static const AkUniqueID MENUMAIN = 1301371087U;
        static const AkUniqueID MUSIC = 3991942870U;
        static const AkUniqueID OBJECTS = 1695690031U;
        static const AkUniqueID SFX = 393239870U;
        static const AkUniqueID SFXMAIN = 1754107905U;
        static const AkUniqueID UI = 1551306167U;
    } // namespace BUSSES

    namespace AUX_BUSSES
    {
        static const AkUniqueID OUTSIDE = 438105790U;
    } // namespace AUX_BUSSES

    namespace AUDIO_DEVICES
    {
        static const AkUniqueID NO_OUTPUT = 2317455096U;
        static const AkUniqueID SYSTEM = 3859886410U;
    } // namespace AUDIO_DEVICES

}// namespace AK

#endif // __WWISE_IDS_H__
