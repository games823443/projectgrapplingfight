# Project Grappling Fight

Project Grappling Fight is a 3D single player Rogue-Like/Jump’n’Run, with randomly generated levels. The goal is to explore different layers of the level and defeat enemies on separated platforms. The player starts at the bottom layer and fights their way up to higher layers. At the end of each layer, the player must beat a boss, in order to enter the next layer. By the time, the player can find items to improve their skills. 

The main feature of the player is the movement. The goal is to move fast and precise. The player has a grappling hook for the jump’n’run passages between the platforms, as well as for fighting the enemies. The hook allows the player to pull towards an enemy and can be detached, in order to attach objects to enemies. These objects get pulled towards the enemy and then damage it.

Additionally, the player can perform double jumps and wall runs.

The enemies and bosses are randomly generated on the map (bosses always at the end of a layer, but random which boss appears). Some enemies are rather big and slow, but are constantly shooting at the player and some are rather small and fast and either follow the player or jump at the player. Speed, size, health and firing rate can differ between the different enemy types.

_Portfolio: https://portfolio.fh-salzburg.ac.at/projects/2022-project-grappling-fight_

_itch.io: https://grappling-fight.itch.io/project-grappling-fight_

_Trailer: https://www.youtube.com/watch?v=a-eyY4bLEDM_

_Developer Commentary (German): https://www.youtube.com/watch?v=AfoGeAr4CT8_
